//
//  Button.swift
//  Jeavm Business
//
//  Created by Refundme on 8/7/19.
//  Copyright © 2019 Quantumsoftech. All rights reserved.
//

import UIKit

class Button: UIButton {
    

    @IBInspectable open var gradientFrom: UIColor = UIColor.themeGradientStart  {
    didSet{
       self.setNeedsLayout()
    }
    }
    @IBInspectable open var gradientTo: UIColor =  UIColor.themeGradientEnd {
    didSet{
       self.setNeedsLayout()
    }
    }
    
    @IBInspectable open var titleColor: UIColor =  UIColor.white {
      didSet{
         self.setNeedsLayout()
      }
      }
    
    @IBInspectable open var isBorderEnabled:Bool = false {
    didSet{
       self.setNeedsLayout()
    }
    }
    @IBInspectable  var borderColor: UIColor =  UIColor.themeGradientEnd
    
    //Shadow
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.2
    @IBInspectable var cornerRadius: CGFloat = 0.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
       // addColors()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //addColors()
    }
  
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    func addColors()
    {
        self.layer.sublayers?.forEach { if $0.name == "dhiru_grdient_layer" {$0.removeFromSuperlayer()} }
        
        let colors: [CGColor] = [gradientFrom.cgColor,gradientTo.cgColor]
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.name = "dhiru_grdient_layer"
        gradient.frame = self.bounds
        gradient.cornerRadius = self.frame.height/2
        gradient.masksToBounds = true
        gradient.colors = colors //.map { $0.cgColor }
        gradient.locations = [0.0, 1.0]
//        gradient.startPoint = CGPoint(x : 0.5, y : 0.3)
//        gradient.endPoint = CGPoint(x :0.3, y: 0.5)
        self.layer.insertSublayer(gradient, at: 0)
        self.clipsToBounds = true
    }
    
    func addShadow()
    {
        
            layer.cornerRadius = self.frame.height/2
            let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius:  self.frame.height/2)
            layer.masksToBounds = false
            layer.shadowColor = shadowColor?.cgColor
             self.layer.shadowOpacity = 1.0;
            self.layer.cornerRadius = cornerRadius;
            layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
            layer.shadowOpacity = shadowOpacity
            layer.shadowPath = shadowPath.cgPath
    }
    
    override func draw(_ rect: CGRect) {
        
        
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
       
        if isBorderEnabled
        {
            self.layer.sublayers?.forEach { if $0.name == "dhiru_grdient_layer" {$0.removeFromSuperlayer()} }
            
            self.layer.masksToBounds = true
            self.layer.borderColor = self.borderColor.cgColor
            self.layer.borderWidth = 1
            self.layer.shadowOpacity = 0.0;
            self.backgroundColor = .clear
            self.layer.cornerRadius = self.frame.height / 2
            self.setTitleColor(self.borderColor, for: .normal)
        }else
        {
            setTitleColor(titleColor, for: .normal)
            self.backgroundColor = .clear
            self.layer.borderWidth = 0
            addColors()
            addShadow()
            
        }
    }
    
    
}

class CardView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 10
    
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.2
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
    }
    
}
class ImageCardView: UIImageView {
    
    @IBInspectable var cornerRadius: CGFloat = 10
    @IBInspectable var borderWidth: CGFloat = 0
    @IBInspectable var borderColor: UIColor? = UIColor.clear
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.2
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor?.cgColor
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
    }
    
}

class halfView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 0

    
    override func layoutSubviews() {
        roundCorners(corners: [.bottomLeft, .bottomRight], radius: cornerRadius)
    }
    
}

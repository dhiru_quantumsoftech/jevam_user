
//
//  TableView.swift
//  Jeavm Business
//
//  Created by Refundme on 8/7/19.
//  Copyright © 2019 Quantumsoftech. All rights reserved.
//

import UIKit

class TableView: UITableView {
 
    override var contentSize:CGSize {
            didSet {
                invalidateIntrinsicContentSize()
            }
        }
        
        override var intrinsicContentSize: CGSize {
            layoutIfNeeded()
            return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
        }
 }

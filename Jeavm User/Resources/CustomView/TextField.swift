
//
//  TextField.swift
//  Jeavm Business
//
//  Created by Refundme on 8/7/19.
//  Copyright © 2019 Quantumsoftech. All rights reserved.
//

import UIKit

    //@IBDesignable
    @objc open class TextField: UITextField {
        
        fileprivate var bottomLineView : UIView?
        fileprivate var labelPlaceholder : UILabel?
        fileprivate var labelErrorPlaceholder : UILabel?
        fileprivate var showingError : Bool = false
        
        fileprivate var bottomLineViewHeight : NSLayoutConstraint?
        fileprivate var placeholderLabelHeight : NSLayoutConstraint?
        fileprivate var errorLabelHieght : NSLayoutConstraint?
        
        /// Disable Floating Label when true.
        @IBInspectable open var disableFloatingLabel : Bool = false
        
        /// Shake Bottom line when Showing Error ?
        @IBInspectable open var shakeLineWithError : Bool = true
        
        @IBInspectable open var isIconsLeftAlign:Bool = false
        
        /// Change Bottom Line Color.
        @IBInspectable open var lineColor : UIColor = UIColor.lightGray {
            didSet{
                self.floatTheLabel()
            }
        }
        
        /// Change line color when Editing in textfield
        @IBInspectable open var selectedLineColor : UIColor = UIColor.primaryDarkColor{
            didSet{
                self.floatTheLabel()
            }
        }
        
        /// Change placeholder color.
        @IBInspectable open var placeHolderColor : UIColor = UIColor.lightGray {
            didSet{
                self.floatTheLabel()
            }
        }
        
        /// Change placeholder color while editing.
        @IBInspectable open var selectedPlaceHolderColor : UIColor = UIColor.primaryDarkColor {
            didSet{
                self.floatTheLabel()
            }
        }
        
        /// Change Error Text color.
        @IBInspectable open var errorTextColor : UIColor = UIColor.red{
            didSet{
                self.labelErrorPlaceholder?.textColor = errorTextColor
                self.floatTheLabel()
            }
        }
        
        /// Change Error Line color.
        @IBInspectable open var errorLineColor : UIColor = UIColor.red{
            didSet{
                self.floatTheLabel()
            }
        }
        
        //MARK:- Set Text
        override open var text:String?  {
            didSet {
                if showingError {
                    self.hideErrorPlaceHolder()
                }
                floatTheLabel()
            }
        }
        
        override open var placeholder: String? {
            willSet {
                if newValue != "" {
                    self.labelPlaceholder?.text = newValue
                }
            }
        }
        
        open var errorText : String? {
            willSet {
                self.labelErrorPlaceholder?.text = newValue
            }
        }
        
        
        @IBInspectable var rightSideIcon:UIImage?
        @IBInspectable var rightSideIconSelected:UIImage?
        private let rightIconWidth:CGFloat = 24.0;
        var imgRightSideIconView:UIImageView?
        var rightSideFloatingLablePaddding:CGFloat = 20.0
        
        
        // Icons Tapped handle
       
        // Icon state
        var textFiledIconSelectedState = false
        public var didTappedOnIcon:((Bool)->())?
        //Leading Contrainet
//        var leadingConstraint:NSLayoutConstraint = NSLayoutConstraint
        
        //MARK:- UITtextfield Draw Method Override
        override open func draw(_ rect: CGRect) {
            super.draw(rect)
            self.upadteTextField(frame: CGRect(x:self.frame.minX, y:self.frame.minY, width:rect.width, height:rect.height));
        }
        
        // MARK:- Loading From NIB
        override open func awakeFromNib() {
            super.awakeFromNib()
            self.initialize()
             addPlaceholderIcons()
            
            
        }
        
      
        
        // MARK:- Intialization
        override public init(frame: CGRect) {
            super.init(frame: frame)
            self.initialize()
            addPlaceholderIcons()
        }
        
        required public init(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)!
            self.initialize()
            addPlaceholderIcons()
        }
        
        // MARK:- Text Rect Management
        override open func textRect(forBounds bounds: CGRect) -> CGRect {
            let paddingx:CGFloat = isIconsLeftAlign ? 40:5
            return CGRect(x:paddingx,y:4, width:bounds.size.width, height:bounds.size.height);
        }
        
        override open func editingRect(forBounds bounds: CGRect) -> CGRect {
            let paddingx:CGFloat = isIconsLeftAlign ? 40:5
            return CGRect(x:paddingx, y:4, width:bounds.size.width, height:bounds.size.height);
        }
        
        //MARK:- UITextfield Becomes First Responder
        override open func becomeFirstResponder() -> Bool {
            let result = super.becomeFirstResponder()
            self.textFieldDidBeginEditing()
            return result
        }
        
        //MARK:- UITextfield Resigns Responder
        override open func resignFirstResponder() -> Bool {
            let result =  super.resignFirstResponder()
            self.textFieldDidEndEditing()
            return result
        }
        
        //MARK:- Show Error Label
        public func showError() {
            showingError = true;
            self.showErrorPlaceHolder();
        }
        public func hideError() {
            showingError = false;
            self.hideErrorPlaceHolder();
            floatTheLabel()
        }
        
        public func showErrorWithText(errorText : String) {
            self.errorText = errorText;
            self.labelErrorPlaceholder?.text = self.errorText
            showingError = true;
            self.showErrorPlaceHolder();
        }
        
        
        
        func addPlaceholderIcons()
        {
            if let oldView = self.viewWithTag(9901){oldView.removeFromSuperview() }
            
            var image = rightSideIcon
            
            if rightSideIconSelected != nil
            {
                image = textFiledIconSelectedState ? rightSideIconSelected : rightSideIcon
            }
            
            if let tintedImage = image?.withRenderingMode(.alwaysTemplate)
            {
                
                
                //leftSideFloatingLablePaddding = leftSideFloatingLablePaddding + leftIconWidth
                imgRightSideIconView = UIImageView(frame: CGRect(x: 8.0, y: 8.0, width: rightIconWidth, height: rightIconWidth))
                imgRightSideIconView?.image = tintedImage
                imgRightSideIconView?.contentMode = .scaleAspectFit
                imgRightSideIconView?.backgroundColor = UIColor.clear
                
                imgRightSideIconView?.tintColor = lineColor
                
                
                if !isIconsLeftAlign
                {
                let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
                view.addSubview(imgRightSideIconView!)
                view.backgroundColor = .white
                view.tag = 9901
                    
                    addTapOnIcon(view: view)
                    
                self.rightViewMode = .always
                self.rightView = view
                }else
                {
                    let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 30))
                    view.addSubview(imgRightSideIconView!)
                    view.backgroundColor = .white
                    view.tag = 9901
                    
                    addTapOnIcon(view: view)
                    
                    self.leftViewMode = .always
                    self.leftView = view
                }
            }else
            {
                if !isIconsLeftAlign
                {
                let view = UIView(frame: CGRect(x: 0, y: 0, width: rightSideFloatingLablePaddding, height: 40))
                view.backgroundColor = .white
                view.tag = 9901
                   
                    addTapOnIcon(view: view)
                
                    self.rightViewMode = .always
                self.rightView = view
                }else
                {
                    let view = UIView(frame: CGRect(x: 0, y: 0, width: rightSideFloatingLablePaddding, height: 30))
                    view.backgroundColor = .white
                    view.tag = 9901
                    self.leftViewMode = .always
                    
                    addTapOnIcon(view: view)
                    
                    self.leftView = view
                    
                    
                }
            }
        }
        
        
        func addTapOnIcon(view:UIView)
              {
                  if rightSideIconSelected != nil
                  {
                      view.isUserInteractionEnabled = true
                      let tap = UITapGestureRecognizer(target: self, action: #selector(self.btnTappedOnIcon(_:)))
                        view.addGestureRecognizer(tap)
                  }else
                  {
                     view.isUserInteractionEnabled = false
                }
              }
              
              @objc func btnTappedOnIcon(_ sender: Any) {
                      textFiledIconSelectedState = !textFiledIconSelectedState
                      print("TextFiled Icon tapped : \(textFiledIconSelectedState)")
                    floatTheLabel()
                    didTappedOnIcon?(textFiledIconSelectedState)
                     //addPlaceholderIcons()
                    //  self.layoutSubviews()
                }
        
    }
    
    fileprivate extension TextField {
        
        func initialize() -> Void {
            
            self.clipsToBounds = true
            /// Adding Bottom Line
            addBottomLine()
            
            /// Placeholder Label Configuration.
            addFloatingLabel()
            
            /// Error Placeholder Label Configuration.
            addErrorPlaceholderLabel()
            /// Checking Floatibility
            if self.text != nil && self.text != "" {
                self.floatTheLabel()
            }
            
        }
        
        //MARK:- ADD Bottom Line
        func addBottomLine(){
            
            if bottomLineView?.superview != nil {
                return
            }
            //Bottom Line UIView Configuration.
            bottomLineView = UIView()
            bottomLineView?.backgroundColor = lineColor
            bottomLineView?.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(bottomLineView!)
            
            let leadingConstraint = NSLayoutConstraint.init(item: bottomLineView!, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0)
            let trailingConstraint = NSLayoutConstraint.init(item: bottomLineView!, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
            let bottomConstraint = NSLayoutConstraint.init(item: bottomLineView!, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
            bottomLineViewHeight = NSLayoutConstraint.init(item: bottomLineView!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 1)
            
            self.addConstraints([leadingConstraint,trailingConstraint,bottomConstraint])
            bottomLineView?.addConstraint(bottomLineViewHeight!)
            
            self.addTarget(self, action: #selector(self.textfieldEditingChanged), for: .editingChanged)
        }
        
        @objc func textfieldEditingChanged(){
            if showingError {
                hideError()
            }
        }
        
        //MARK:- ADD Floating Label
        func addFloatingLabel(){
            
            if labelPlaceholder?.superview != nil {
               labelPlaceholder?.removeFromSuperview()
            }
            
            var placeholderText : String? = labelPlaceholder?.text
            if self.placeholder != nil && self.placeholder != "" {
                placeholderText = self.placeholder!
            }
            labelPlaceholder = UILabel()
            labelPlaceholder?.text = placeholderText
            labelPlaceholder?.textAlignment = self.textAlignment
            labelPlaceholder?.textColor = placeHolderColor
            labelPlaceholder?.font = UIFont.init(name: (self.font?.fontName ?? "helvetica")!, size: 12)
            labelPlaceholder?.isHidden = true
            labelPlaceholder?.sizeToFit()
            labelPlaceholder?.translatesAutoresizingMaskIntoConstraints = false
            if #available(iOS 13.0, *) {
                self.placeholderColor(color: placeHolderColor)
            }else
            {
                 self.setValue(placeHolderColor, forKeyPath: "_placeholderLabel.textColor")
            }
            if labelPlaceholder != nil {
                self.addSubview(labelPlaceholder!)
            }
            let leadingConstraint = NSLayoutConstraint.init(item: labelPlaceholder!, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: isIconsLeftAlign ? 40: 5)
            let trailingConstraint = NSLayoutConstraint.init(item: labelPlaceholder!, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
            let topConstraint = NSLayoutConstraint.init(item: labelPlaceholder!, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
            placeholderLabelHeight = NSLayoutConstraint.init(item: labelPlaceholder!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15)
            
            self.addConstraints([leadingConstraint,trailingConstraint,topConstraint])
            labelPlaceholder?.addConstraint(placeholderLabelHeight!)
            
        }
        
        
        
        override open func layoutSubviews() {
            super.layoutSubviews()
            leadingConstaint?.constant =  isIconsLeftAlign ? 40: 5
            self.textColor = .black
            
//        if rightSideIconSelected != nil
//        {
//            self.imgRightSideIconView?.image = self.textFiledIconSelectedState ? rightSideIconSelected : rightSideIcon
//        }
            
            
            self.bringSubviewToFront(self.bottomLineView!)
            
        }
        
        
        func addErrorPlaceholderLabel() -> Void {
            
            if self.labelErrorPlaceholder?.superview != nil{
                return
            }
            labelErrorPlaceholder = UILabel()
            labelErrorPlaceholder?.text = self.errorText
            labelErrorPlaceholder?.textAlignment = self.textAlignment
            labelErrorPlaceholder?.textColor = errorTextColor
            labelErrorPlaceholder?.font = UIFont(name: (self.font?.fontName ?? "helvetica")!, size: 12)
            labelErrorPlaceholder?.sizeToFit()
            labelErrorPlaceholder?.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(labelErrorPlaceholder!)
            
              let leadingConstraint = NSLayoutConstraint.init(item: labelPlaceholder!, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: isIconsLeftAlign ? 40: 5)
            
            let trailingConstraint = NSLayoutConstraint.init(item: labelErrorPlaceholder!, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: isIconsLeftAlign ? 40:5)
            let topConstraint = NSLayoutConstraint.init(item: labelErrorPlaceholder!, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
            errorLabelHieght = NSLayoutConstraint.init(item: labelErrorPlaceholder!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
            
            self.addConstraints([leadingConstraint,trailingConstraint,topConstraint])
            labelErrorPlaceholder?.addConstraint(errorLabelHieght!)
            
        }
        
        func showErrorPlaceHolder() {
            
            bottomLineViewHeight?.constant = 2;
            
            if self.errorText != nil && self.errorText != "" {
                errorLabelHieght?.constant = 15;
                UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
                    self.bottomLineView?.backgroundColor = self.errorLineColor;
                    self.layoutIfNeeded()
                }, completion: nil)
            }else{
                errorLabelHieght?.constant = 0;
                UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
                    self.bottomLineView?.backgroundColor = self.errorLineColor;
                    self.layoutIfNeeded()
                }, completion: nil)
            }
            
            if shakeLineWithError {
                bottomLineView?.shake()
            }
            
        }
        
        func hideErrorPlaceHolder(){
            showingError = false;
            
            if errorText == nil || errorText == "" {
                return
            }
            
            errorLabelHieght?.constant = 0;
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
                self.layoutIfNeeded()
            }, completion: nil)
            
        }
        
        //MARK:- Float & Resign
        func floatTheLabel() -> Void {
            DispatchQueue.main.async {
                if self.text == "" && self.isFirstResponder {
                    self.floatPlaceHolder(selected: true)
                }else if self.text == "" && !self.isFirstResponder {
                    self.resignPlaceholder()
                }else if self.text != "" && !self.isFirstResponder  {
                    self.floatPlaceHolder(selected: false)
                }else if self.text != "" && self.isFirstResponder {
                    self.floatPlaceHolder(selected: true)
                }
            }
        }
        
        //MARK:- Upadate and Manage Subviews
        func upadteTextField(frame:CGRect) -> Void {
            self.frame = frame;
            self.initialize()
        }
        
        //MARK:- Float UITextfield Placeholder Label
        func floatPlaceHolder(selected:Bool) -> Void {
            
            //************* New Logic *****************
            var image = rightSideIcon
                       
                       if rightSideIconSelected != nil
                       {
                           image = textFiledIconSelectedState ? rightSideIconSelected : rightSideIcon
                       }
                       
                       if let tintedImage = image?.withRenderingMode(.alwaysTemplate)
                       {
                        imgRightSideIconView?.image =  tintedImage
                       }
            //************* New Logic End *****************
            
            labelPlaceholder?.isHidden = false
            if selected {
                
                bottomLineView?.backgroundColor = showingError ? self.errorLineColor : self.selectedLineColor;
                
                 imgRightSideIconView?.tintColor = showingError ? self.errorLineColor : self.selectedLineColor;
                
                labelPlaceholder?.textColor = self.selectedPlaceHolderColor;
                bottomLineViewHeight?.constant = 2;
                
                
                if #available(iOS 13.0, *) {
                    self.placeholderColor(color: placeHolderColor)
                }else
                {
                     self.setValue(placeHolderColor, forKeyPath: "_placeholderLabel.textColor")
                }
                //self.setValue(self.selectedPlaceHolderColor, forKeyPath: "_placeholderLabel.textColor")
                
            } else {
                bottomLineView?.backgroundColor = showingError ? self.errorLineColor : self.lineColor;
                imgRightSideIconView?.tintColor = showingError ? self.errorLineColor : self.lineColor;
                
                bottomLineViewHeight?.constant = 1;
                self.labelPlaceholder?.textColor = self.placeHolderColor
                
                
                if #available(iOS 13.0, *) {
                    self.placeholderColor(color: placeHolderColor)
                }else
                {
                     self.setValue(placeHolderColor, forKeyPath: "_placeholderLabel.textColor")
                }
                //self.setValue(placeHolderColor, forKeyPath: "_placeholderLabel.textColor")
            }
            
            if disableFloatingLabel == true {
                labelPlaceholder?.isHidden = true
                return
            }
            
            if placeholderLabelHeight?.constant == 15 {
                return
            }
            
            placeholderLabelHeight?.constant = 15;
            labelPlaceholder?.font = UIFont(name: (self.font?.fontName)!, size: 12)
            
            UIView.animate(withDuration: 0.2, animations: {
                self.layoutIfNeeded()
            })
            
        }
        
        //MARK:- Resign the Placeholder
        func resignPlaceholder() -> Void {
            
            //self.setValue(self.placeHolderColor, forKeyPath: "_placeholderLabel.textColor")
            if #available(iOS 13.0, *) {
                self.placeholderColor(color: placeHolderColor)
            }else
            {
                 self.setValue(placeHolderColor, forKeyPath: "_placeholderLabel.textColor")
            }
            
            
            bottomLineView?.backgroundColor = showingError ? self.errorLineColor : self.lineColor;
            imgRightSideIconView?.tintColor = showingError ? self.errorLineColor : self.lineColor;
            
            bottomLineViewHeight?.constant = 1;
            
            if disableFloatingLabel {
                
                labelPlaceholder?.isHidden = true
                self.labelPlaceholder?.textColor = self.placeHolderColor;
                UIView.animate(withDuration: 0.2, animations: {
                    self.layoutIfNeeded()
                })
                return
            }
            
            placeholderLabelHeight?.constant = self.frame.height
            
            UIView.animate(withDuration: 0.3, animations: {
                self.labelPlaceholder?.font = self.font
                self.labelPlaceholder?.textColor = self.placeHolderColor
                self.layoutIfNeeded()
            }) { (finished) in
                self.labelPlaceholder?.isHidden = true
                self.placeholder = self.labelPlaceholder?.text
            }
        }
        
        //MARK:- UITextField Begin Editing.
        func textFieldDidBeginEditing() -> Void {
            if showingError {
                self.hideErrorPlaceHolder()
            }
            if !self.disableFloatingLabel {
                self.placeholder = ""
            }
            self.floatTheLabel()
            self.layoutSubviews()
        }
        
        //MARK:- UITextField Begin Editing.
        func textFieldDidEndEditing() -> Void {
            self.floatTheLabel()
        }
    }
    
    //MARK:- Shake
    extension UIView {
        func shake() {
            let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
            animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
            animation.duration = 0.6
            animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
            layer.add(animation, forKey: "shake")
        }
}


extension UITextField {
    func placeholderColor(color: UIColor) {
        let attributeString = [
            NSAttributedString.Key.foregroundColor: color.withAlphaComponent(0.6),
            NSAttributedString.Key.font: self.font!
            ] as [NSAttributedString.Key : Any]
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "", attributes: attributeString)
    }
}


extension UIView {
    
    var leadingConstaint: NSLayoutConstraint? {
        get {
            return constraints.first(where: {
                $0.firstAttribute == .leading && $0.relation == .equal
            })
        }
        set { setNeedsLayout() }
    }
    
}

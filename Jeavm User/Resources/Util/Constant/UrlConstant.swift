
//  UrlConstant.swift
//  Refund.me
//  Created by refund.me india on 03/12/17.
//  Copyright © 2017 refund.me india. All rights reserved.

import Foundation

let Placholder:String = "user"
let Placholder_Airline:String = "" //"noimageAirline"
let logoCover:String = "coverDefualt"
let logoPlacholder:String = "largeDefult"
let logoPlacholderSmall:String = "smallDefulat"
let PlaceHolderImage:String = "place_holder_image"
//let PlacholderFlight:String = "Screen Shot 2019-01-04 at 12.48.39 PM"
let appKey:String = "623224dd3fa6babc02ff85c71393262ce9a6c75dee39cddbd62b403aea7b40f4"
let appId:String = "4151cff18ed09e5cca069c571ece144a"
let adsId:String = "ca-app-pub-2469789184185467/6187788262"
let adsInterstitial:String = "ca-app-pub-2469789184185467/6983581336"
var app_token = ""
// live- com.MrBoie.refundme
// staging -  com.RefundmeIndia.refundme
class UrlConstant {
    
    static let documentUploadMessageAlert = "Its taking more then usual time, Would you like to continue uploading the file ?"
    static let logoutMsg = "logout"
    
    //Staging URL
    static let BASE_URL = "https://staging.quantumsoftech.in/fitness/userapi/"
   
    static let BASE_test_url = "staging.quantumsoftech.in"
    
    //Live URL
   
    //static let BASE_URL = ""
    
    static let DISTANCE_API_URL = "https://maps.googleapis.com/maps/api/directions/json?origin="
    static let UPDATE_USER_IMAGE_URL = BASE_URL + "user/updateImage";
    static let FLIGHT_REVIEW_URL = BASE_URL + "";
    static let OTP_REQUEST_OLD_URL = BASE_URL + "/user/sendOtpLogin";
    static let PASSENGER_Doc_URL = BASE_URL + "upload_doc"
    static let PASSENGER_INDoc_URL = BASE_URL + "upload_doc_in"
    static let WEB_LAUNCH = BASE_URL + "user/get_use_cred";
    static let SEND_OTP = BASE_URL + "user/sendOtp"
    static let VERIFY_OTP = BASE_URL + "user/verifyOtp"
 
}

//
//  Constants.swift
//  Dhiru Boilerplate
//
//  Created by mac on 2/06/2019.
//  Copyright © 2017 Dhiru  All rights reserved.
//

import UIKit


let appID = 1011071761

typealias cvprotocols = UICollectionViewDataSource&UICollectionViewDelegate&UICollectionViewDelegateFlowLayout&UIScrollViewDelegate





let AppName = Bundle.appName()

let appDelegate = UIApplication.shared.delegate as! AppDelegate

// User for Calling API's
typealias Result = (_ success: Bool, _ msg:String) -> Void

var isProduction = false


var BASE_URL:String
{
    get{
        if isProduction
        {
            return "https://api.jeavm.com/api/"
        }else
        {
            return "https://staging.quantumsoftech.in/fitness/api/"
        }
    }
}

struct SERVER_URL {
    static let BASE = BASE_URL   //+ "user/login"
  
    static let LOGIN = BASE_URL + "user/login"
    static let CONTACT_US = BASE_URL + "contact-us"
    static let FORGOT_PASSWORD = BASE_URL + "password/forgot"
    static let RESET_PASSWORD = BASE_URL + "password/reset"
    static let USER_PROFILE = BASE_URL + "user/profile"
    static let GET_USER_ACTIVE_MODULE =  BASE_URL + "get-active-modules"
    static let GET_DIRECTORIES = BASE_URL + "get-directories"
    static let GET_DIRECTORY_DETAILS = BASE_URL + "get-directory-details"
    
    
    static let GET_ACTIVE_MODULES = BASE_URL + "get-active-modules"
    static let GET_NOTICE_LIST = BASE_URL + "get-notice"
    
    
    static let GET_COMPLAINT_LIST = BASE_URL + "get-complaints"
    static let GET_COMPLAINT_CATEGORY = BASE_URL + "get-complaint-category"
    static let CREATE_COMPLAINTS = BASE_URL + "create-complaint"
    static let UPDATE_COMPLAINTS_STATUS = BASE_URL + "update-complaint-status"
    
    static let STAFF_UPDATE_PROFILE_PIC = BASE_URL + "staffProfilePic"
    
    static let GET_ALL_POLL = BASE_URL + "get-polls"
    static let GET_SUBMIT_POLL = BASE_URL + "user-polls-answer"
    
    
    static let terms_condition =  BASE_URL + "terms"
    static let privacy_policy =  BASE_URL + "privacy_policy"
    static let help = BASE_URL + "about"
    static let about_us = BASE_URL + "about"
    
}


extension UIColor {
    
    class Colors {
        var gl:CAGradientLayer!
        
        init() {
            let colorTop = UIColor(hexString: "#FF973E")
            let colorBottom = UIColor(hexString: "#FF5C3B")
            self.gl = CAGradientLayer()
            self.gl.colors = [colorTop, colorBottom]
            self.gl.locations = [0.0, 1.0]
        }
    }
     static let TextClrGray =  UIColor(hexString: "#707070")
    static let primaryColor =  UIColor(hexString: "#FF973E")
    static let primaryDarkColor =  UIColor(hexString: "#FF5C3B") //
    
    static let themeAppColor = UIColor(red: 32.0/255.0, green: 26.0/255.0, blue:86.0/255.0, alpha: 1)
    
    static let shadowColor = UIColor(hexString: "#E3E3E3")
    static let borderColor = UIColor(hexString: "#E3E3E3")
    
    static let themeGradientStart = primaryColor  //UIColor(hexString: "#FF410B")
    static let themeGradientEnd = primaryDarkColor //UIColor(hexString: "#FF8F1A")
    
    
    static let themeDarkBlue  = UIColor(hexString:"#111e6c")
    static let themeRed  = UIColor(hexString:"#cc0000")
    static let themeGray  = UIColor(hexString:"#cccccc")
    static let themeWhite  = UIColor(hexString:"#ffffff")
    static let themeYellow  = UIColor(hexString:"#ffff00")
    static let themeBackground = UIColor(hexString: "#dceaf1")

    
    //OLD COLOR
//    static let completedColor = UIColor(hexString:"#6481D8")
//    static let inProgrssColor =  UIColor(hexString: "#B4EF56")
//    static let cancelledColor =  UIColor(hexString: "#FF5B93")
//    static let upCommingColor =  UIColor(hexString:"#FF973E")
//    static let noShowColor =  UIColor(hexString: "#AFB8CA")  //UIColor(hexString:"#4D6DDA")
    
    //SELF CORRECTED
    static let completedColor = UIColor(hexString:"#5F80CF")
    static let inProgrssColor =  UIColor(hexString: "#F3973E")
    static let cancelledColor =  UIColor(hexString: "#ED5891")
    static let upCommingColor =  UIColor(hexString:"#2C9421") //New: #2C9421 // Old:#43DE55
    static let noShowColor =  UIColor(hexString: "#AFB8CA")
    
    //Gender Color
    static let genderMaleColor =  UIColor(hexString: "#765DDB")
    static let genderFemaleColor =  UIColor(hexString:"#FF48A5")
    //static let genderUnisexColor =  UIColor(hexString: "#AFB8CA")
    
    
    }

extension Notification.Name {
    static let homeTabClicked =  Notification.Name("homeTabClicked")
    static let bookingSessionChanged = Notification.Name("bookingSessionHasBeenChanged")
    static let fontSizeChanged =  Notification.Name("fontSizeChanged")
    static let bookingsTabClicked =  Notification.Name("bookingsTabClicked")
    static let staffServiceStatusChanged = Notification.Name("staffServiceStatusChanged")
    
    static let serviceTabListGenderChanged =  Notification.Name("serviceTabListGenderChanged")
    
    static let startBookingSessionTimer = Notification.Name("startBookingSessionTimer")
}




struct MSG
{
    static let loading = "Loading..."
    
  
    struct Error {
        static let networkError = "Please check your internet conenction"
        static let unknownError = "Oopss..Somthing went wrong"
        static let parsingError = "Error while parsing response"
        static let apiNotResonding = "Error while parsing response"
    }
}


extension UserDefaults
{
    struct Keys {
        static let FONT_SIZE_TYPE = "font_size_type"
        static let deviceToken = "naya_india_device_token"
        static let deviceId = "naya_india_device_id"
        
        static let settting_notification = "notification_setting"
        static let settting_notification_night = "notification_night"
        static let settting_notification_sound = "sound_setting"
        static let settting_notification_vibration = "vibration_setting"
    }
}


enum FontSizeType:Int {
    case small
    case medium
    case large
    case extraLarge
}


extension UIFont{
    //***************** App Font Size *******************
    
    static func fontRegular(_ ofSize:CGFloat) -> UIFont {
        return UIFont(name: "OpenSans-Regular", size: ofSize)!
    }
    static func fontMedium(_ ofSize:CGFloat) -> UIFont {
        return UIFont(name: "OpenSans-SemiBold", size: ofSize)!
    }
    
    static func fontBold(_ ofSize:CGFloat) -> UIFont {
        return UIFont(name: "OpenSans-Bold", size: ofSize)!
    }
    
    /*
    static func hindiLight(_ ofSize:CGFloat) -> UIFont {
        return UIFont(name: "Mukta-Light", size: ofSize)!
    }
    
    static func hindiRegular(_ ofSize:CGFloat) -> UIFont {
        return UIFont(name: "Mukta-Regular", size: ofSize)!
    }
    static func hindiMedium(_ ofSize:CGFloat) -> UIFont {
        //return UIFont(name: "Mukta-Medium", size: ofSize)!
        return UIFont(name: "Mukta-SemiBold", size: ofSize)!
    }
 
    
    static func hindiBold(_ ofSize:CGFloat) -> UIFont {
        return UIFont(name: "Mukta-Bold", size: ofSize)!
    }
 */
    
}


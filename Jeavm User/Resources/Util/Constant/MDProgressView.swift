//
//  MDProgressView.swift
//  Refund.me
//
//  Created by refund.me india on 16/11/17.
//  Copyright © 2017 refund.me india. All rights reserved.
//

import Foundation
import UIKit

public class MDProgressView {
    
    var containerView = UIView()
    var progressView = UIView()
    var activityIndicator = UIActivityIndicatorView()
     var imageVw = GIFHUDImageView()
    
    public class var shared: MDProgressView {
        
        struct Static {
            static let instance: MDProgressView = MDProgressView()
        }
        return Static.instance
    }
    
    @discardableResult public func setGif(named: String) -> Bool {
        imageVw.animationImages = nil
        imageVw.stopAnimating()
        
        if let image = GIFHUDImage(image: named, delegate: imageVw) {
            imageVw.image = image
            return true
        }
        
        return false
    }
    
    public func showProgressView(view: UIView) {
        appDelegate.showIndicator()
        // guard let topWindow = UIApplication.shared.windows.last else {return}
         
//        containerView.frame = topWindow.frame
//        containerView.center = topWindow.center
//        containerView.backgroundColor = UIColor(hex: 0xffffff, alpha: 0.3)
//        progressView.subviews.forEach({ $0.removeFromSuperview() })
//        progressView.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
//
//        progressView.center = topWindow.center
//        progressView.backgroundColor = UIColor(hex: 0x444444, alpha: 0.7)
//
//        progressView.clipsToBounds = true
//        progressView.layer.cornerRadius = 10
//
//
//        imageVw.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
//        setGif(named: "loader-01.gif")
//        imageVw.startAnimatingGif()
//
//       // activityIndicator.style = .whiteLarge
//        imageVw.center = CGPoint(x: progressView.bounds.width / 2, y: progressView.bounds.height / 2)
//        progressView.addSubview(imageVw)
//        containerView.addSubview(progressView)
//        let currentWindow = UIApplication.shared.keyWindow
//        currentWindow?.addSubview(containerView)
//        activityIndicator.startAnimating()
    }
    public func showProgressViewlable(view: UIView) {
        
        /*
        containerView.subviews.forEach({ $0.removeFromSuperview() })
        containerView.frame = view.frame
        containerView.center = view.center
        containerView.backgroundColor = UIColor(hex: 0xffffff, alpha: 0.3)
        progressView.subviews.forEach({ $0.removeFromSuperview() })
        progressView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        
        progressView.center = view.center
        progressView.backgroundColor = UIColor(hex: 0x444444, alpha: 0.7)
        
        progressView.clipsToBounds = true
        progressView.layer.cornerRadius = 10
        
        
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        activityIndicator.style = .whiteLarge
        activityIndicator.center = CGPoint(x: progressView.bounds.width / 2, y: progressView.bounds.height / 2)
        progressView.addSubview(activityIndicator)
        containerView.addSubview(progressView)
        let currentWindow = UIApplication.shared.keyWindow
        currentWindow?.addSubview(containerView)
        activityIndicator.startAnimating()
        
        let label = UILabel(frame: CGRect(x: 0, y: activityIndicator.frame.size.height+activityIndicator.frame.origin.y, width: progressView.frame.size.width, height: 16))
        label.font = UIFont(name: "Titillium-Regular", size: 13)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.text = "Downloading"
        progressView.addSubview(label)
     */
    }
    public func hideProgressView() {
        appDelegate.hideIndicator()
//        activityIndicator.stopAnimating()
//        containerView.removeFromSuperview()
    }
    
    public func isShowing() -> Bool{
        return activityIndicator.isAnimating
    }
}



extension UIColor {
    convenience init(hex: UInt32, alpha: CGFloat) {
        let red = CGFloat((hex & 0xFF0000) >> 16)/256.0
        let green = CGFloat((hex & 0xFF00) >> 8)/256.0
        let blue = CGFloat(hex & 0xFF)/256.0
        self.init(red: red, green: green, blue: blue, alpha: alpha)
        
    }
}

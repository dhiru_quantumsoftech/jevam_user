//
//  Util.swift
//  Dhiru Boilerplate
//
//  Created by mac on 2/06/2019.
//  Copyright © 2017 Dhiru  All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class Util
{
    
    
    class func displayDate(date:Date) -> String
      {
          let calendar = Calendar.current
          let dateComponents = calendar.component(.day, from: date)
          let numberFormatter = NumberFormatter()
              numberFormatter.numberStyle = .ordinal

          let day = numberFormatter.string(from: dateComponents as NSNumber)
          let dateFormatter = DateFormatter()

          dateFormatter.dateFormat = "MMM"

          let dateString = "\(day!) \(dateFormatter.string(from: date))"

          return dateString
      }
      
      class func displayTime(date:Date) -> String
       {
          let dateFormatter = DateFormatter()
                 dateFormatter.dateFormat = "hh:mm a"
          let dateString = dateFormatter.string(from: date)
          return dateString
         }

      
      static let voiletColor:UIColor = UIColor(red: 97.0/255.0, green: 132.0/255.0, blue: 216.0/255, alpha: 1.0)
      static let orangeColor:UIColor = UIColor(red: 255.0/255.0, green: 157.0/255.0, blue: 63.0/255, alpha: 1.0)
    
    
    class func customizeStatusBar()
    {
        
        
        if #available(iOS 13.0, *) {
        let app = UIApplication.shared
        let statusBarHeight: CGFloat = app.statusBarFrame.size.height
        let statusbarView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: statusBarHeight))
        statusbarView.backgroundColor = UIColor.primaryDarkColor
        //view.addSubview(statusbarView)
        } else {
        let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
        statusBar?.backgroundColor = UIColor.primaryDarkColor
        }
        
        
//        if let statusBar =  UIApplication.shared.statusBarUIView //UIApplication.shared.value(forKey: "statusBar") as? UIView {
//        {
//            statusBar.backgroundColor =  UIColor.primaryDarkColor
//            //statusBar.setValue(UIColor.white, forKey: "foregroundColor")
//        }el
        
        //UILabel.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont.systemFont(ofSize: 14.0)
    }
    
    
    class func customizeTabBarAppreance(){
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont.fontRegular(12)], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont.fontMedium(14)], for: .selected)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.lightGray], for: .normal)
       
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.primaryDarkColor], for: .selected)
        
        UITabBar.appearance().tintColor = UIColor.primaryDarkColor
        UITabBar.appearance().isOpaque = false
   
    }
    
    class func customizeNavigationAppreanceTranparent(){
        UINavigationBar.appearance().barTintColor = UIColor.primaryDarkColor
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font : UIFont.fontRegular(14),NSAttributedString.Key.foregroundColor : UIColor.white]
        
     // UINavigationBar.appearance().barTintColor = .clear
            
      //UIColor(patternImage: layerImage ?? UIImage())
    }
    
    
    class func customizeNavigationAppreance(){
        //UINavigationBar.appearance().barTintColor = UIColor.themeAppColor
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17.0, weight: .bold),NSAttributedString.Key.foregroundColor : UIColor.white]
        
        // let gradient = CAGradientLayer()
        /*
        let sizeLength = UIScreen.main.bounds.size.height * 2

        let colors:[UIColor] = [UIColor.primaryColor, UIColor.primaryDarkColor]
        let status_height = UIApplication.shared.statusBarFrame.size.height
        let gradientLayer = CAGradientLayer(frame: CGRect(x: 0, y: 0, width: sizeLength, height: status_height + 44), colors: colors)
        let layerImage = gradientLayer.createGradientImage()
       */
        //UINavigationBar.appearance().barTintColor = UIColor(patternImage: #imageLiteral(resourceName: "ic_nav_bg") ) //layerImage ?? UIImage())
        
       // UINavigationBar.appearance().backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "ic_nav_bg"))
        
        UINavigationBar.appearance().setBackgroundImage(#imageLiteral(resourceName: "ic_nav_bg").resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch), for: .default)
    }
    
    class func isValidURL(urlString:String?)-> Bool{
        
        if let urlString = urlString {
            // create NSURL instance
            if let url = NSURL(string: urlString) {
                // check if your application can open the NSURL instance
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    
    class func isValidPhone(phone:String) -> Bool {
        
        if phone.count  == 10
        {
            return true
        }
        
        return false
        
//        let phoneRegex = "^[0-9+]{0,1}+[0-9]{5,16}$"
//        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
//        return phoneTest.evaluate(with: phone)
        //return true
    }
    
    class func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    class func geocode(latitude: Double, longitude: Double, completion: @escaping (_ placemark: [CLPlacemark]?, _ error: Error?) -> Void)  {
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { placemark, error in
            guard let placemark = placemark, error == nil else {
                completion(nil, error)
                return
            }
            completion(placemark, nil)
        }
    }
    
    class func printAllAvaibleFonts()
    {
        
        for family in UIFont.familyNames.sorted()
        {
            let name = UIFont.fontNames(forFamilyName: family)
            print("Family: \(family) Font name :\(name)")
        }
        
        
    }
    
    
    class func image(fromLayer layer: CALayer) -> UIImage {
        UIGraphicsBeginImageContext(layer.frame.size)
        
        layer.render(in: UIGraphicsGetCurrentContext()!)
        
        let outputImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return outputImage!
    }
    
    
    
}




extension Util
{
    
    class func getGenericSizeForVerticalItem(indexPath:IndexPath,clcView:UICollectionView) -> CGSize
    {
        if UIDevice().userInterfaceIdiom == .phone {
            if UIDevice.current.orientation.isLandscape {
                return  CGSize(width: clcView.frame.width/2 - 2, height: 203.0)
            }else
            {
                return  CGSize(width: clcView.frame.width - 2, height: 203.0)
            }
        }else
        {
            if UIDevice.current.orientation.isLandscape {
                return  CGSize(width: clcView.frame.width/3 - 3, height: 203.0)
            }else
            {
                return  CGSize(width: clcView.frame.width/2 - 2, height: 203.0)
            }
        }
    }
    
    
    class func getGenericSizeForVerticalItem1(indexPath:IndexPath,clcView:UICollectionView) -> CGSize
    {
        if UIDevice().userInterfaceIdiom == .phone {
            if UIDevice.current.orientation.isLandscape {
                return  CGSize(width: clcView.frame.width/2 - 2, height: 215.0)
            }else
            {
                return  CGSize(width: clcView.frame.width - 2, height: 215.0)
            }
        }else
        {
            if UIDevice.current.orientation.isLandscape {
                return  CGSize(width: clcView.frame.width/3 - 3, height: 215.0)
            }else
            {
                return  CGSize(width: clcView.frame.width/2 - 2, height: 215.0)
            }
        }
    }
    
    class func getFitnessTrainerSizeForHorizontalItem(indexPath:IndexPath,clcView:UICollectionView) -> CGSize
    {
        return  CGSize(width: 141.0, height: 155.0)
    }
    
    class func getGenericSizeForHorizontalItem1(indexPath:IndexPath,clcView:UICollectionView) -> CGSize
    {
        
        if UIDevice.current.orientation.isLandscape  ||  UIDevice().userInterfaceIdiom != .phone{
            return  CGSize(width: 320, height: 215.0)
        }else
        {
            return  CGSize(width: clcView.frame.width - 30, height: 215.0)
        }
        
        /*
         if UIDevice().userInterfaceIdiom == .phone {
         if UIDevice.current.orientation.isLandscape {
         return  CGSize(width: clcView.frame.width/2 - 2, height: 215.0)
         }else
         {
         return  CGSize(width: clcView.frame.width - 2, height: 215.0)
         }
         }else
         {
         if UIDevice.current.orientation.isLandscape {
         return  CGSize(width: clcView.frame.width/3 - 3, height: 215.0)
         }else
         {
         return  CGSize(width: clcView.frame.width/2 - 2, height: 215.0)
         }
         }
         */
    }
    
    
}


/*
class Util
{
    
    class func customizeStatusBar()
    {
        if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
            statusBar.backgroundColor =  UIColor.themeAppColor
            statusBar.setValue(UIColor.white, forKey: "foregroundColor")
        }
        
        
        UILabel.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont.systemFont(ofSize: 14.0)
    }
    
    
    class func customizeTabBarAppreance(){
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont.fontRegular(12)], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont.fontMedium(12)], for: .selected)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.themeAppColor], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: .selected)
    }
    class func customizeNavigationAppreance(){
        UINavigationBar.appearance().barTintColor = UIColor.themeAppColor
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font : UIFont.fontMedium(14),NSAttributedString.Key.foregroundColor : UIColor.darkGray]
    }
    
    class func isValidURL(urlString:String?)-> Bool{
        
        if let urlString = urlString {
            // create NSURL instance
            if let url = NSURL(string: urlString) {
                // check if your application can open the NSURL instance
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
        
    }
    
    class func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
}

*/


extension Util
{
    
    class func getDisplayTime(strDate:String?) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        if let startDate = dateFormatter.date(from: strDate ?? "")
        {
            dateFormatter.dateFormat = "hh:mm a"
            return dateFormatter.string(from: startDate)
        }
        return ""
    }
    
    class func getDisplayDate(strDate:String?) -> String
    {
       
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if let startDate = dateFormatter.date(from: strDate ?? "")
        {
            dateFormatter.dateFormat = "d MMM yy"
            return dateFormatter.string(from: startDate)
            ///return Util.getDurationDifferance(date1: startDate, date2: Date())
        }
        return "0 Mins"
        
    }
    class func getDisplayDateOnly(strDate:String?) -> String
    {
       
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        if let startDate = dateFormatter.date(from: strDate ?? "")
        {
            dateFormatter.dateFormat = "d MMM yy"
            return dateFormatter.string(from: startDate)
            ///return Util.getDurationDifferance(date1: startDate, date2: Date())
        }
        return "0 Mins"
        
    }
    class func getDisplayTimeOnly(strDate:String?) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let startDate = dateFormatter.date(from: strDate ?? "")
        {
            dateFormatter.dateFormat = "hh:mm a"
            return dateFormatter.string(from: startDate)
        }
        return ""
    }
    
    class func getDisplayDateTime(strDate:String?) -> String
    {
//
//        guard let str = strDate else{
//            return ""
//        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if let startDate = dateFormatter.date(from: strDate ?? "")
        {
            dateFormatter.dateFormat = "hh:mm a | d MMM yy"
            return dateFormatter.string(from: startDate)
            ///return Util.getDurationDifferance(date1: startDate, date2: Date())
        }
        return "0 Mins"
        
    }
    
    class func getDurationDifferance(date1:Date,date2:Date) -> String
    {
    
        let distanceBetweenDates: TimeInterval? = date2.timeIntervalSince(date1)
        let secondsInAnHour: Double = 3600
        let minsInAnHour: Double = 60
        let secondsInDays: Double = 86400
        let secondsInWeek: Double = 604800
        let secondsInMonths : Double = 2592000
        let secondsInYears : Double = 31104000
        
        let minBetweenDates = Int((distanceBetweenDates! / minsInAnHour))
        let hoursBetweenDates = Int((distanceBetweenDates! / secondsInAnHour))
        let daysBetweenDates = Int((distanceBetweenDates! / secondsInDays))
        let weekBetweenDates = Int((distanceBetweenDates! / secondsInWeek))
        let monthsbetweenDates = Int((distanceBetweenDates! / secondsInMonths))
        let yearbetweenDates = Int((distanceBetweenDates! / secondsInYears))
        let secbetweenDates = Int(distanceBetweenDates!)
        
        
        
        
        if yearbetweenDates > 0
        {
            return "\(yearbetweenDates) years"//0 years
        }
        else if monthsbetweenDates > 0
        {
            return "\(monthsbetweenDates)months"//0 months
        }
        else if weekBetweenDates > 0
        {
            return "\(weekBetweenDates) weeks"//0 weeks
        }
        else if daysBetweenDates > 0
        {
            return "\(daysBetweenDates) days"//5 days
        }
        else if hoursBetweenDates > 0
        {
            return "\(hoursBetweenDates) hours"//120 hours
        }
        else if minBetweenDates > 0
        {
            return "\(minBetweenDates) minutes"//7200 minutes
        }
        else if secbetweenDates > 0
        {
            return "\(secbetweenDates) seconds"//seconds
        }
        
        return "0 minutes"
    }
    
    
    
    class func decodeData<T>(modelData:Data?,type:T.Type) -> T? where T:Codable
    {
        do {
            if let  data = modelData
            {
                let decodedNews = try JSONDecoder().decode(T.self, from: data)
                return decodedNews
            }else
            {
                return nil
            }
        } catch { print(error)
            return nil
        }
    }
    
   class func encodeData<T:Codable>(type:T?) -> Data?
    {
        do{
        if let newVal = type
        {
             let jsonData = try JSONEncoder().encode(newVal)
             return jsonData
        }else
        {
            return nil
        }
        }catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    
}


extension Util
{
    class  func getProgressColor(progress:Float) -> UIColor
    {
        if (progress > 0 && progress < 0.2)
        {
            return .red
        }else if (progress > 0.2 && progress < 0.4)
        {
            return .primaryColor
        }else if (progress > 0.4 && progress < 0.6)
        {
            return .primaryColor
        }else if (progress > 0.6 && progress < 0.8)
        {
            return .green
        }else if (progress > 0.8 && progress < 1.0)
        {
            return .green
        }
        
        return .red
    }
}


extension UIApplication
{
    var statusBarUIView: UIView? {
        if #available(iOS 13.0, *) {
            let tag = 38482458
            if let statusBar = self.keyWindow?.viewWithTag(tag) {
                return statusBar
            } else {
                let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
                statusBarView.tag = tag

                self.keyWindow?.addSubview(statusBarView)
                return statusBarView
            }
        } else {
            if responds(to: Selector(("statusBar"))) {
                return value(forKey: "statusBar") as? UIView
            }
        }
        return nil
    }
}
public enum Model : String {
    
    //Simulator
    case simulator     = "simulator/sandbox",
    
    //iPod
    iPod1              = "iPod 1",
    iPod2              = "iPod 2",
    iPod3              = "iPod 3",
    iPod4              = "iPod 4",
    iPod5              = "iPod 5",
    
    //iPad
    iPad2              = "iPad 2",
    iPad3              = "iPad 3",
    iPad4              = "iPad 4",
    iPadAir            = "iPad Air ",
    iPadAir2           = "iPad Air 2",
    iPadAir3           = "iPad Air 3",
    iPad5              = "iPad 5", //iPad 2017
    iPad6              = "iPad 6", //iPad 2018
    
    //iPad Mini
    iPadMini           = "iPad Mini",
    iPadMini2          = "iPad Mini 2",
    iPadMini3          = "iPad Mini 3",
    iPadMini4          = "iPad Mini 4",
    iPadMini5          = "iPad Mini 5",
    
    //iPad Pro
    iPadPro9_7         = "iPad Pro 9.7\"",
    iPadPro10_5        = "iPad Pro 10.5\"",
    iPadPro11          = "iPad Pro 11\"",
    iPadPro12_9        = "iPad Pro 12.9\"",
    iPadPro2_12_9      = "iPad Pro 2 12.9\"",
    iPadPro3_12_9      = "iPad Pro 3 12.9\"",
    
    //iPhone
    iPhone4            = "iPhone 4",
    iPhone4S           = "iPhone 4S",
    iPhone5            = "iPhone 5",
    iPhone5S           = "iPhone 5S",
    iPhone5C           = "iPhone 5C",
    iPhone6            = "iPhone 6",
    iPhone6Plus        = "iPhone 6 Plus",
    iPhone6S           = "iPhone 6S",
    iPhone6SPlus       = "iPhone 6S Plus",
    iPhoneSE           = "iPhone SE",
    iPhone7            = "iPhone 7",
    iPhone7Plus        = "iPhone 7 Plus",
    iPhone8            = "iPhone 8",
    iPhone8Plus        = "iPhone 8 Plus",
    iPhoneX            = "iPhone X",
    iPhoneXS           = "iPhone XS",
    iPhoneXSMax        = "iPhone XS Max",
    iPhoneXR           = "iPhone XR",
    iPhone11           = "iPhone 11",
    iPhone11Pro        = "iPhone 11 Pro",
    iPhone11ProMax     = "iPhone 11 Pro Max",
    
    //Apple TV
    AppleTV            = "Apple TV",
    AppleTV_4K         = "Apple TV 4K",
    unrecognized       = "?unrecognized?"
}

// #-#-#-#-#-#-#-#-#-#-#-#-#
// MARK: UIDevice extensions
// #-#-#-#-#-#-#-#-#-#-#-#-#

public extension UIDevice {
    
    var type: Model {
        var systemInfo = utsname()
        uname(&systemInfo)
        let modelCode = withUnsafePointer(to: &systemInfo.machine) {
            $0.withMemoryRebound(to: CChar.self, capacity: 1) {
                ptr in String.init(validatingUTF8: ptr)
            }}
        var modelMap : [String: Model] = [
            
            //Simulator
            "i386"      : .simulator,
            "x86_64"    : .simulator,
            
            //iPod
            "iPod1,1"   : .iPod1,
            "iPod2,1"   : .iPod2,
            "iPod3,1"   : .iPod3,
            "iPod4,1"   : .iPod4,
            "iPod5,1"   : .iPod5,
            
            //iPad
            "iPad2,1"   : .iPad2,
            "iPad2,2"   : .iPad2,
            "iPad2,3"   : .iPad2,
            "iPad2,4"   : .iPad2,
            "iPad3,1"   : .iPad3,
            "iPad3,2"   : .iPad3,
            "iPad3,3"   : .iPad3,
            "iPad3,4"   : .iPad4,
            "iPad3,5"   : .iPad4,
            "iPad3,6"   : .iPad4,
            "iPad4,1"   : .iPadAir,
            "iPad4,2"   : .iPadAir,
            "iPad4,3"   : .iPadAir,
            "iPad5,3"   : .iPadAir2,
            "iPad5,4"   : .iPadAir2,
            "iPad6,11"  : .iPad5, //iPad 2017
            "iPad6,12"  : .iPad5,
            "iPad7,5"   : .iPad6, //iPad 2018
            "iPad7,6"   : .iPad6,
            
            //iPad Mini
            "iPad2,5"   : .iPadMini,
            "iPad2,6"   : .iPadMini,
            "iPad2,7"   : .iPadMini,
            "iPad4,4"   : .iPadMini2,
            "iPad4,5"   : .iPadMini2,
            "iPad4,6"   : .iPadMini2,
            "iPad4,7"   : .iPadMini3,
            "iPad4,8"   : .iPadMini3,
            "iPad4,9"   : .iPadMini3,
            "iPad5,1"   : .iPadMini4,
            "iPad5,2"   : .iPadMini4,
            "iPad11,1"  : .iPadMini5,
            "iPad11,2"  : .iPadMini5,
            
            //iPad Pro
            "iPad6,3"   : .iPadPro9_7,
            "iPad6,4"   : .iPadPro9_7,
            "iPad7,3"   : .iPadPro10_5,
            "iPad7,4"   : .iPadPro10_5,
            "iPad6,7"   : .iPadPro12_9,
            "iPad6,8"   : .iPadPro12_9,
            "iPad7,1"   : .iPadPro2_12_9,
            "iPad7,2"   : .iPadPro2_12_9,
            "iPad8,1"   : .iPadPro11,
            "iPad8,2"   : .iPadPro11,
            "iPad8,3"   : .iPadPro11,
            "iPad8,4"   : .iPadPro11,
            "iPad8,5"   : .iPadPro3_12_9,
            "iPad8,6"   : .iPadPro3_12_9,
            "iPad8,7"   : .iPadPro3_12_9,
            "iPad8,8"   : .iPadPro3_12_9,
            
            //iPad Air
            "iPad11,3"  : .iPadAir3,
            "iPad11,4"  : .iPadAir3,
            
            //iPhone
            "iPhone3,1" : .iPhone4,
            "iPhone3,2" : .iPhone4,
            "iPhone3,3" : .iPhone4,
            "iPhone4,1" : .iPhone4S,
            "iPhone5,1" : .iPhone5,
            "iPhone5,2" : .iPhone5,
            "iPhone5,3" : .iPhone5C,
            "iPhone5,4" : .iPhone5C,
            "iPhone6,1" : .iPhone5S,
            "iPhone6,2" : .iPhone5S,
            "iPhone7,1" : .iPhone6Plus,
            "iPhone7,2" : .iPhone6,
            "iPhone8,1" : .iPhone6S,
            "iPhone8,2" : .iPhone6SPlus,
            "iPhone8,4" : .iPhoneSE,
            "iPhone9,1" : .iPhone7,
            "iPhone9,3" : .iPhone7,
            "iPhone9,2" : .iPhone7Plus,
            "iPhone9,4" : .iPhone7Plus,
            "iPhone10,1" : .iPhone8,
            "iPhone10,4" : .iPhone8,
            "iPhone10,2" : .iPhone8Plus,
            "iPhone10,5" : .iPhone8Plus,
            "iPhone10,3" : .iPhoneX,
            "iPhone10,6" : .iPhoneX,
            "iPhone11,2" : .iPhoneXS,
            "iPhone11,4" : .iPhoneXSMax,
            "iPhone11,6" : .iPhoneXSMax,
            "iPhone11,8" : .iPhoneXR,
            "iPhone12,1" : .iPhone11,
            "iPhone12,3" : .iPhone11Pro,
            "iPhone12,5" : .iPhone11ProMax,
            
            //Apple TV
            "AppleTV5,3" : .AppleTV,
            "AppleTV6,2" : .AppleTV_4K
        ]
        
        if let model = modelMap[String.init(validatingUTF8: modelCode!)!] {
            if model == .simulator {
                if let simModelCode = ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] {
                    if let simModel = modelMap[String.init(validatingUTF8: simModelCode)!] {
                        return simModel
                    }}}
            return model
        }
        return Model.unrecognized
    }
}

//
//  UserSessionManager.swift
//  Dhiru Boilerplate
//
//  Created by mac on 2/06/2019.
//  Copyright © 2017 Dhiru  All rights reserved.
//

import Foundation
import CoreLocation
class UserSession {
    
    // Login Type User
  //  static var userType:UserType = .vendor
    
    
    // singlton
    static let shared = UserSession()
    
    
    struct Keys
    {
        static let USER_INFO = "user_info"
        static let USER_LOCATION_LAT = "user_latitude"
        static let USER_LOCATION_LONG = "user_longitude"
        static let USER_SELECTED_ADDRESS_TITLE = "user_selected_adress_title"
        static let USER_SELECTED_ADDRESS = "user_selected_adress"
        
        
        static let BOOKING_SESSION = "last_booking_session"
        static let BOOKING_SESSION_TIME = "last_booking_sesion_time"
//        static let LAST_BOOKING_ID = "last_booking_id"
//        static let CART_COUNT = "cart_count"
//        static let CART_CLIENT_ID = "cart_client_id"
        
    }
    
    
    func setCurrentLocation(title:String,address:String,location:CLLocationCoordinate2D)
    {
          let userDefaults = UserDefaults.standard
          userDefaults.set(location.latitude, forKey: Keys.USER_LOCATION_LAT)
          userDefaults.set(location.longitude, forKey: Keys.USER_LOCATION_LONG)
          userDefaults.set(address, forKey: Keys.USER_SELECTED_ADDRESS)
          userDefaults.set(title, forKey: Keys.USER_SELECTED_ADDRESS_TITLE)
          userDefaults.synchronize()
    }
    
    func getCurrentLcoation() -> (title:String,address:String,location:CLLocationCoordinate2D)?
    {
        
       if  let latValue = UserDefaults.standard.value(forKey: Keys.USER_LOCATION_LAT) as? Double,
          let longValue = UserDefaults.standard.value(forKey: Keys.USER_LOCATION_LAT) as? Double,
          let address = UserDefaults.standard.value(forKey: Keys.USER_SELECTED_ADDRESS) as? String,
          let title  = UserDefaults.standard.value(forKey: Keys.USER_SELECTED_ADDRESS_TITLE) as? String
       {
        return (title,address,CLLocationCoordinate2D(latitude: latValue, longitude: longValue))
       }
        return nil
    }
    
    func clearLocationSelected()
    {
       let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: Keys.USER_LOCATION_LAT)
        userDefaults.removeObject(forKey: Keys.USER_LOCATION_LONG)
        userDefaults.removeObject(forKey: Keys.USER_SELECTED_ADDRESS)
        userDefaults.synchronize()
    }
    
    
   
    
    
//    func createUserSession(userInfo:UserInfo)  {
//        appDelegate.showIndicator()
//        let userDefaults = UserDefaults.standard
//        let jsonEncoder = JSONEncoder()
//        let jsonData = try? jsonEncoder.encode(userInfo)
//        userDefaults.set(jsonData, forKey: Keys.USER_INFO)
//        userDefaults.synchronize()
//        DispatchQueue.main.async {
//          appDelegate.setupAfterLogin()
//        }
//    }
//    
//    func getUserLoggedIn() -> UserInfo? {
//        if let data = UserDefaults.standard.object(forKey: Keys.USER_INFO) as? Data
//        {
//        let jsonDecoder = JSONDecoder()
//         let userInfo = try? jsonDecoder.decode(UserInfo.self, from: data)
//          return userInfo
//        }
//        
//        return nil
//    }
    
    func logOutUser()  {
        appDelegate.showIndicator()
        let userDefult = UserDefaults.standard
        userDefult.removeObject(forKey: Keys.USER_INFO)
        userDefult.removeObject(forKey: Keys.USER_LOCATION_LAT)
        userDefult.removeObject(forKey: Keys.USER_LOCATION_LAT)
        userDefult.synchronize()
        
        clearBookingSession()
        clearLocationSelected()
        //Remove All
//        let domain = Bundle.main.bundleIdentifier!
//        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
          DispatchQueue.main.async {
           appDelegate.setupAfterLogout()
        }
    }

    
}



extension UserSession
{
    //************* Booking Session Time ****************
    func createBookingSessionTime(date:Date)
     {
             let userDefaults = UserDefaults.standard
              userDefaults.set(date, forKey: Keys.BOOKING_SESSION_TIME)
              userDefaults.synchronize()
     }
     
     func getBookingSessionTime() -> Date?
     {
         let date = UserDefaults.standard.object(forKey: Keys.BOOKING_SESSION_TIME) as? Date
        return date
     }
     
     func clearBookingSessionTime()
      {
             let userDefaults = UserDefaults.standard
               userDefaults.removeObject(forKey: Keys.BOOKING_SESSION_TIME)
               userDefaults.synchronize()
      }
    
    //************* Booking Session ****************
    
//    func createBookingSession(bookingSession:AddBookingResponse)
//    {
//        let userDefaults = UserDefaults.standard
//        let jsonEncoder = JSONEncoder()
//        let jsonData = try? jsonEncoder.encode(bookingSession)
//        userDefaults.set(jsonData, forKey: Keys.BOOKING_SESSION)
//        userDefaults.synchronize()
//
//        NotificationCenter.default.post(name: Notification.Name.bookingSessionChanged, object: nil)
//    }
//
//
//
//    func getBookingSession() -> AddBookingResponse?
//    {
//        if let data = UserDefaults.standard.object(forKey: Keys.BOOKING_SESSION) as? Data
//        {
//            let jsonDecoder = JSONDecoder()
//            let userInfo = try? jsonDecoder.decode(AddBookingResponse.self, from: data)
//            return userInfo
//        }
//
//        return nil
//
////        if  let bookingId = UserDefaults.standard.value(forKey: Keys.LAST_BOOKING_ID) as? String,
////            let cartCount = UserDefaults.standard.value(forKey: Keys.CART_COUNT) as? String,
////            let clientId = UserDefaults.standard.value(forKey: Keys.CART_COUNT) as? String
////        {
////            let booking = AddBookingResponse(bookingId: bookingId, clientId: clientId, vendorId: "", totalCart: Int(cartCount) ?? 0)
////            return booking
//        //        }
//    }
//
    
    func clearBookingSession()
    {
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: Keys.BOOKING_SESSION)
        userDefaults.removeObject(forKey: Keys.BOOKING_SESSION_TIME)
        userDefaults.synchronize()
         NotificationCenter.default.post(name: Notification.Name.bookingSessionChanged, object: nil)
    }
    
}

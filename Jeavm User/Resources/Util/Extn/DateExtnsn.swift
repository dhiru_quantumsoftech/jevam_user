//
//  DateExtnsn.swift
//  EASYLIFE
//
//  Created by Dhiru on 31/07/19.
//  Copyright © 2019 Dhiru. All rights reserved.
//

import Foundation

extension Date {
    
    /*
    var hoursFromToday: Int{
        
        var fromDate: Date = self
        var toDate: Date = Date()
        var sign: Int = -1
        (fromDate,toDate, sign) = self.sanitizedDates()
        
        return (sign * NSCalendar.currentCalendar.components(.CalendarUnitHour, fromDate: fromDate, toDate: toDate, options: nil).hour)
    }
    var weeksFromToday: Int{
        
        
        var fromDate: Date = self
        var toDate: Date = Date()
        var sign: Int = -1
        (fromDate,toDate,sign) = self.sanitizedDates()
        
        return (sign * NSCalendar.currentCalendar.components(.CalendarUnitWeekOfYear, fromDate: fromDate, toDate: toDate, options: nil).weekOfYear)
    }
    
    
    var daysFromToday: Int{
        
        var fromDate: NSDate = self
        var toDate: NSDate = NSDate()
        var sign: Int = -1
        (fromDate,toDate, sign) = self.sanitizedDates()
        
        
        return (sign * NSCalendar.currentCalendar().components(.CalendarUnitDay, fromDate: fromDate, toDate: toDate, options: nil).day)
    }
    var monthsFromToday: Int{
        
        
        var fromDate: NSDate = self
        var toDate: NSDate = NSDate()
        var sign: Int = -1
        (fromDate,toDate, sign) = self.sanitizedDates()
        
        return (sign * NSCalendar.currentCalendar().components(.CalendarUnitMonth, fromDate: fromDate, toDate: toDate, options: nil).month)
        
    }
    var minsFromToday: Int{
        
        
        var fromDate: NSDate = self
        var toDate: NSDate = NSDate()
        var sign: Int = -1
        var offset: Int = 0
        (fromDate,toDate,sign) = self.sanitizedDates()
        
        
        return ( sign * NSCalendar.currentCalendar().components(.CalendarUnitMinute, fromDate: fromDate, toDate: toDate, options: nil).minute)
    }
    
    func relativeDateString(capitalizeFirst:Bool = false) -> String {
        let days: Int = daysFromToday
        let mins: Int = minsFromToday % 60
        
        let tense: String = (minsFromToday > 0) ? " in the future" : " in the past"
        let hrs: Int =  hoursFromToday % 24
        var retString  = (capitalizeFirst) ? "Now" : "now"
        if(minsFromToday != 0) {
            if(days == 0) {
                retString = (capitalizeFirst) ? "Today" : "today"
                retString = (mins != 0 || hrs != 0) ? retString+"," : retString
            }
            else {
                let absDays = abs(days)
                retString = "\(absDays)"
                retString += (absDays > 1) ? " days" : " day"
            }
            if(hrs != 0) {
                let absHrs = abs(hrs)
                retString += " \(absHrs)"
                retString += (absHrs > 1) ? " hours" : " hour"
            }
            
            if(mins != 0) {
                let absMins = abs(mins)
                retString += " \(absMins)"
                retString += (absMins > 1) ? " minutes" : " minute"
            }
            
            retString += tense
        }
        
        return retString
    }
*/
}


extension Date {
    static var yesterday: Date { return Date().dayBefore }
    static var tomorrow:  Date { return Date().dayAfter }
    
    var monthBefore: Date {
        return Calendar.current.date(byAdding: .month, value: -1, to: noon)!
    }
    var monthAfter: Date {
        return Calendar.current.date(byAdding: .month, value: +1, to: noon)!
    }
    
    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var dayAfter: Date {
        return Calendar.current.date(byAdding: .day, value: +1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    
    var day: Int {
        return Calendar.current.component(.day,  from: self)
    }
    
    var isLastDayOfMonth: Bool {
        return dayAfter.month != month
    }
}

//
//  UIImageView+LoadAsyncImg.swift
//  Dhiru
//
//  Created by mac on 7/31/17.
//  Copyright © 2017 Dhiru. All rights reserved.
//

import UIKit
import SDWebImage

extension UIImageView {
    func loadImageFrom(url: URL ,placeholderImage pcImage :UIImage?) {
        //contentMode = mode
      
        if pcImage != nil {
            self.image=pcImage
        }else
        {
            //self.image = #imageLiteral(resourceName: "ic_placeholder")
        }
        //************** SD Image Cache *****************
        self.sd_imageTransition = .fade
        self.sd_setImage(with: url, placeholderImage: pcImage, options:[], completed: nil)
        //***********************************************
        
        /*
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.image = image
            }
            }.resume()
 */
 
    }
    func loadImageFromURL(url: String,  placHolderImage :UIImage?) {
        
        if placHolderImage != nil {
            self.image=placHolderImage
        }else
        {
            //self.image = #imageLiteral(resourceName: "ic_placeholder")
        }
        
        //self.image = #imageLiteral(resourceName: "ic_placeholder")
        guard let url = URL(string: url) else { return }
        loadImageFrom(url: url ,placeholderImage: placHolderImage)
    }
}


extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ jpegQuality: JPEGQuality) -> Data? {
        return jpegData(compressionQuality: jpegQuality.rawValue)
    }
}

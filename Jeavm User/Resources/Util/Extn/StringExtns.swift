//
//  StringExtns.swift
//  Dhiru Boilerplate
//
//  Created by mac on 2/06/2019.
//  Copyright © 2017 Dhiru  All rights reserved.
//

import Foundation
import UIKit
extension String
{
    
    func  getAttriubuttedHindi(fontSize:CGFloat) -> NSAttributedString  {
        
        let attributedString = NSAttributedString(string:self,
                                                  attributes:[NSAttributedString.Key.foregroundColor: UIColor.black,
                                                              NSAttributedString.Key.font: UIFont.fontRegular(fontSize) as Any])
        return attributedString
    }
    
    func stripOutHtml() -> NSAttributedString? {
        do {
            guard let data = self.data(using: .unicode) else {
                return nil
            }
            
            let descriptionAttribute = [ NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14.0)]
            let attributed = try NSMutableAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html,.characterEncoding: String.Encoding.utf8.rawValue], documentAttributes:nil)
            attributed.addAttributes(descriptionAttribute, range: NSRange(location: 0, length: attributed.length))
            return attributed
        } catch {
            return nil
        }
    }
    
    var youtubeID: String? {
        let pattern = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"
        
        let regex = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
        let range = NSRange(location: 0, length: count)
        
        guard let result = regex?.firstMatch(in: self, range: range) else {
            return nil
        }
        
        return (self as NSString).substring(with: result.range)
    }
    
    func firstCharacterUpperCase() -> String {
        let lowerCasedString = self.lowercased()
        return lowerCasedString.replacingCharacters(in: lowerCasedString.startIndex...lowerCasedString.startIndex, with: String(lowerCasedString[lowerCasedString.startIndex]).uppercased())
    }
    
}

extension String {
    func strikeThrough() -> NSAttributedString {
        let attributeString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSUnderlineStyle.single.rawValue, range: NSMakeRange(0,attributeString.length))
        return attributeString
    }
}

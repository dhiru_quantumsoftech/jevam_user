//
//  CAGradientLayerExtns.swift
//  ALVM
//
//  Created by Refundme on 6/25/19.
//  Copyright © 2019 Refundme. All rights reserved.
//

import Foundation
import UIKit

extension CAGradientLayer {
    convenience init(frame: CGRect, colors: [UIColor]) {
        self.init()
        self.frame = frame
        self.colors = []
        for color in colors {
            self.colors?.append(color.cgColor)
        }
//        startPoint = CGPoint(x: 0, y: 0)
//        endPoint = CGPoint(x: 0, y: 1)
//        
        startPoint = CGPoint(x : 0.0, y : 0.3)
        endPoint = CGPoint(x :0.6, y: 1.0)
    }
    
    func createGradientImage() -> UIImage? {
        
        var image: UIImage? = nil
        
        UIGraphicsBeginImageContext(bounds.size)
        
        if let context = UIGraphicsGetCurrentContext() {
            render(in: context)
            image = UIGraphicsGetImageFromCurrentImageContext()
        }
        
        UIGraphicsEndImageContext()
        
        return image
    }
}

//
//  ViewExtsn.swift
//  ALVM
//
//  Created by Refundme on 6/19/19.
//  Copyright © 2019 Refundme. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
    func setCardView(){
        
        self.layer.cornerRadius = 5.0
        self.layer.borderColor  =  UIColor.clear.cgColor
        self.layer.borderWidth = 5.0
        self.layer.shadowOpacity = 0.5
        self.layer.shadowColor =  UIColor.lightGray.cgColor
        self.layer.shadowRadius = 5.0
        self.layer.shadowOffset = CGSize(width:5, height: 5)
        self.layer.masksToBounds = true
        
    }
    
    
    func addBlackGradientLayerInForeground(colors:[UIColor]) -> CAGradientLayer{
       
        self.layer.sublayers?
            .filter { layer1 in return layer.name == "dhiru_image_gradient" }
            .forEach { layer1 in
                layer1.removeFromSuperlayer()
                layer1.removeAllAnimations()
        }
        
        var grdFrame = self.bounds
        grdFrame.origin.x = grdFrame.origin.x - 20
        
        let gradient = CAGradientLayer()
        gradient.name = "dhiru_image_gradient"
        gradient.frame = grdFrame
        gradient.colors = colors.map{$0.cgColor}
        self.layer.addSublayer(gradient)
        
        return gradient
    }
    // For insert layer in background
    func addBlackGradientLayerInBackground(colors:[UIColor]) -> CAGradientLayer {
        
        layer.sublayers?
            .filter { layer in return layer.name == "dhiru_image_gradient_background" }
            .forEach { layer in
                layer.removeFromSuperlayer()
                layer.removeAllAnimations()
        }
        
        let gradient = CAGradientLayer()
         gradient.name = "dhiru_image_gradient_background"
        gradient.frame = self.bounds
        gradient.colors = colors.map{$0.cgColor}
        self.layer.insertSublayer(gradient, at: 0)
        
        return gradient
    }
    
       func roundCorners(corners: UIRectCorner, radius: CGFloat) {
            let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            layer.mask = mask
        }

}

extension UIView
{
   
       func roundCornersWithShadow(corners: UIRectCorner, radius: CGFloat) {
        let maskPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii:CGSize(width: radius,height: radius)).cgPath
   
    self.layer.shadowPath = maskPath
    self.layer.shadowColor = UIColor.black.cgColor
    self.layer.shadowOffset = CGSize(width: 1, height: 2)
    self.layer.shadowOpacity = 0.2
    self.layer.shadowRadius = 2
    
    let maskLayer = CAShapeLayer()
    maskLayer.path = maskPath
    self.layer.mask = maskLayer
    }
}

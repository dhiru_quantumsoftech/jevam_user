//
//  UIRefreshControlExtns.swift
//  Jeavm Business
//
//  Created by Refundme on 8/22/19.
//  Copyright © 2019 Quantumsoftech. All rights reserved.
//

import Foundation
import UIKit

extension UIRefreshControl {
    
    func beginRefreshingManually() {
        if let scrollView = superview as? UIScrollView {
            scrollView.setContentOffset(CGPoint(x: 0, y: scrollView.contentOffset.y - frame.height), animated: false)
        }
        beginRefreshing()
        sendActions(for: .valueChanged)
    }
    
}

//
//  DataExtnsn.swift
//  Dhiru Boilerplate
//
//  Created by mac on 2/06/2019.
//  Copyright © 2017 Dhiru  All rights reserved.
//

import Foundation


extension Data
{
    
    func convertToJSONString() -> String {
         let jsonString = NSString(data: self, encoding: String.Encoding.utf8.rawValue) as String?
         return jsonString ?? "Unable to convet in json";
    }
    
    func convertToDictionary() -> Dictionary<String,Any>? {
        
        if let dictMain = try? JSONSerialization.jsonObject(with: self, options:.mutableContainers) as? Dictionary<String, AnyObject>
        {
             return dictMain
        }
        
        return nil
    }
    

}



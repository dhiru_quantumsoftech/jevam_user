//
//  ViewControllerExtns.swift
//  Jeavm Business
//
//  Created by Refundme on 8/7/19.
//  Copyright © 2019 Quantumsoftech. All rights reserved.
//

import Foundation
import UIKit



extension UIViewController
{
  
    
    @IBAction func menuButtonTapped(sender:Any)
    {
      self.menuButtonClicked()
    }
    
    @IBAction func notificationButtonTapped(sender:Any)
    {
        self.notificationButtonClicked()
    }
    
    @IBAction func navBackButtonTapped(sender:Any)
    {
        self.backBtnClicked()
    }
    

    
    @IBAction func navNotificationButtonTapped(sender:Any)
    {
        self.notificationButtonClicked()
        //self.backBtnClicked()
    }
    

    func animateIndicator(show:Bool,refresh:UIRefreshControl)
    {
        DispatchQueue.main.async {
            if show
            {
               refresh.becomeFirstResponder()
               refresh.beginRefreshing()
           }else
            {
               refresh.endRefreshing()
            }
         }
    }
    
}


extension UIViewController {
    
    
    var topbarHeight: CGFloat {
        return UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
    }
    
    func setupStatusBarColor()
          {
              if #available(iOS 13.0, *) {
                  let app = UIApplication.shared
                  let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                  
                  let statusbarView = UIView()
                  statusbarView.backgroundColor = UIColor.primaryDarkColor
                  view.addSubview(statusbarView)
                
                  statusbarView.translatesAutoresizingMaskIntoConstraints = false
                  statusbarView.heightAnchor
                      .constraint(equalToConstant: statusBarHeight).isActive = true
                  statusbarView.widthAnchor
                      .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                  statusbarView.topAnchor
                      .constraint(equalTo: view.topAnchor).isActive = true
                  statusbarView.centerXAnchor
                      .constraint(equalTo: view.centerXAnchor).isActive = true
                
              } else {
                  let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                  statusBar?.backgroundColor = UIColor.primaryDarkColor
              }
              }
    
    func setupHomePageNavView()  {
  
        let btnMenu = UIBarButtonItem(image:#imageLiteral(resourceName: "ic_home"), style: .plain, target: self, action: #selector(self.menuButtonClicked))
        //let logoView = UIImageView(image: UIImage(named:"logo_header"))
//        logoView.translatesAutoresizingMaskIntoConstraints = false
//        logoView.widthAnchor.constraint(equalToConstant: 180).isActive = true
        
     
        let btnSmile = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_nav_smile"), style: .plain, target: self, action: #selector(self.smileButtonClicked))
        
         let btnNotification = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_nav_notification"), style: .plain, target: self, action: #selector(self.notificationButtonClicked))
        
        
        //self.navigationItem.titleView = logoView
        self.navigationItem.leftBarButtonItems = [btnMenu]
         self.navigationItem.rightBarButtonItems = [btnSmile,btnNotification]
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor : UIColor.white]
        
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        
        
    }
    
    
    func addMenuButton(){
  
        let btn_menu = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 44))
        btn_menu.addTarget(self, action: #selector(self.refreshBtnClicked), for: .touchUpInside)
        btn_menu.setImage(#imageLiteral(resourceName: "ic_menu"), for: .normal)
        btn_menu.setImage(#imageLiteral(resourceName: "ic_menu"), for: .selected)
        self.navigationController?.navigationItem.leftBarButtonItems = [UIBarButtonItem(customView: btn_menu)]
    }
    
    func addRefreshBtnToNavigationBar(){
        
        let btn_refresh = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 44))
        btn_refresh.addTarget(self, action: #selector(self.refreshBtnClicked), for: .touchUpInside)
        btn_refresh.setImage(#imageLiteral(resourceName: "ic_menu"), for: .normal)
        btn_refresh.contentHorizontalAlignment = .right
        self.tabBarController?.navigationItem.setRightBarButton(UIBarButtonItem(customView: btn_refresh), animated: true)
    }
    @objc func refreshBtnClicked(){
        
    }
    
    @objc func optionMenuButtonClicked(){
        
        /*
        let vc = Storyboard.loadMenuOptionsVC()
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        
        vc.delegate = {(resultType,moreOptionVc) in
            
            switch resultType {
            
            case .fontSizeChanged:
                 print("Font Size Changed")
            case .savedStory:
                 print("Saved Stroies")
                 let vc = Storyboard.loadBookmarksVC()
                 self.navigationController?.pushViewController(vc, animated: true)
                
            case .none:
                print("None Operation")
            }
            moreOptionVc.dismiss(animated: true, completion:nil)
        }
        DispatchQueue.main.async {
            self.present(vc, animated: false, completion: nil)
        }
        */
    }
    
    @objc func menuButtonClicked(){
        //appDelegate.sideMenuController?.toggleLeftViewAnimated()
    }
    
    @objc func smileButtonClicked(){
        //appDelegate.sideMenuController?.toggleLeftViewAnimated()
    }
    
    @objc func notificationButtonClicked(){
       
//        if let usertype = UserSession.shared.getUserLoggedIn()?.userType,usertype == .vendor
//        {
//          let vc =  Storyboard.loadVendorNotificationVC()
//          let nav = sideMenuController?.rootViewController as! UINavigationController
//          nav.pushViewController(vc, animated: true)
//        }else
//        {
//            let vc =  Storyboard.loadNotificationVC()
//            let nav = sideMenuController?.rootViewController as! UINavigationController
//            nav.pushViewController(vc, animated: true)
//        }
        //appDelegate.sideMenuController?.toggleLeftViewAnimated()
    }
    
    func addBackBtnToNavigationBar(){
        
        let back_button = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_nav_back"), style: .plain, target: self, action: #selector(self.backBtnClicked))
//        let logoView = UIImageView(image: UIImage(named:"logo_header"))
//
//        logoView.translatesAutoresizingMaskIntoConstraints = false
//        logoView.widthAnchor.constraint(equalToConstant: 180).isActive = true
//        
        //search
        
//        let search_button = UIBarButtonItem(image: UIImage(named: "search"), style: .plain, target: self, action: #selector(self.backSearchClicked))
       // self.navigationItem.titleView = logoView
        self.navigationItem.leftBarButtonItems = [back_button]
        //self.navigationItem.rightBarButtonItems = [search_button]
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
    }
    
    
    @objc func btnCrossClicked(){
         self.dismiss(animated: Config.animatedTransition, completion: nil)
    }
    
    func addCrossButtonWithTitle(){
        
        let back_button = UIBarButtonItem(image: UIImage(named: "ic_close"), style: .plain, target: self, action: #selector(self.backBtnClicked))
//        let logoView = UIImageView(image: UIImage(named:"logo_header"))
//        //search
//
//        let search_button = UIBarButtonItem(image: UIImage(named: "search"), style: .plain, target: self, action: #selector(self.backSearchClicked))
        
       // logoView.translatesAutoresizingMaskIntoConstraints = false
        //logoView.widthAnchor.constraint(equalToConstant: 180).isActive = true
        //self.navigationItem.titleView = logoView
        self.navigationItem.leftBarButtonItems = [back_button]
         //self.navigationItem.rightBarButtonItems = [search_button]
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
    }
    
  
    
    
    @objc func backBtnClicked(){
        
        if self.navigationController != nil {
           if ( self.navigationController?.viewControllers.count ?? 0 ) > 1
           {
             _ = self.navigationController?.popViewController(animated: Config.animatedTransition)
            }else
           {
                exit(0)
            }
        }else{
            self.dismiss(animated: true, completion: nil)
            // appDelegate.setUpSideMenu()
        }
    }
    
    @objc func backSearchClicked(){
        
//        let searchVC = Storyboard.loadSearchVC()
//        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    @objc func keyboardDonePressed(btn:Any?){
        self.view.endEditing(true)
    }
    func addToolBarDoneBtn() -> UIToolbar {
        let toolbar_Done =  UIToolbar()
        toolbar_Done.sizeToFit()
        let btn_done = UIBarButtonItem(title: "DONE", style: .plain, target: self, action: #selector(self.keyboardDonePressed(btn:)))
        toolbar_Done.items = [btn_done]
        return toolbar_Done
    }
    
    func loadEmaileUrl(email:String)
    {
        let phone = "mailto:\(email)"
        openUrl(strUrl: phone)
    }
    
    func loadPhoneUrl(phoneNumber:String)
    {
        let phone = "tel://\(phoneNumber)"
        openUrl(strUrl: phone)
//        if let url = URL(string: "tel://\(phoneNumber)"),
//            UIApplication.shared.canOpenURL(url as URL) {
//            UIApplication.shared.open(url, options: [:], completionHandler: nil)
//            //UIApplication.shared.open(url, options: [, completionHandler: nil)
//        }
    }
    
    func openUrl(strUrl:String)
    {
        if let url = URL(string: strUrl),
            UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func openShareOptions(str:String)
    {
        let text = str
        
        // set up activity view controller
        let textToShare = [text]
        let activityViewController = UIActivityViewController(activityItems: textToShare as [Any], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = []
        //        activityViewController.
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    
 
    
}





extension UILabel
{
 
    
    open override func layoutSubviews() {
        super.layoutSubviews()
    
    }
    

}


extension UIViewController
{
    
    func setupUpHomePageNavigation(title:String, subtitle:String) {
        
        let back_button = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu"), style: .plain, target: self, action: #selector(self.btnSearchClicked))
        back_button.tintColor = .white
        self.navigationItem.leftBarButtonItems = [back_button]
        
        let space = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        space.width = 0.0
        
        let one = UILabel()
        one.text = title
        one.textAlignment = .center
        one.textColor = UIColor.white
        // one.backgroundColor = .green
        one.font = UIFont.systemFont(ofSize: 17.0, weight: .bold)
        one.sizeToFit()
        
        let two = UILabel()
        two.text = subtitle
        two.textAlignment = .center
        // two.backgroundColor = .yellow
        two.textColor = .white
        //        two.font = UIFont.systemFont(ofSize: 12)
        two.font = UIFont.systemFont(ofSize: 13.0, weight: .bold)
        two.textAlignment = .center
        two.sizeToFit()
        
        
        let stackView = UIStackView(arrangedSubviews: [one, two])
        stackView.distribution = .equalCentering
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.backgroundColor = .red
        
        one.sizeToFit()
        two.sizeToFit()
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.widthAnchor.constraint(equalToConstant:150).isActive = true
        
        
        self.navigationItem.titleView = stackView
        
        let search_button = UIBarButtonItem(image:#imageLiteral(resourceName: "ic_nav_notification"), style: .plain, target: self, action: #selector(self.btnSearchClicked))
        search_button.tintColor = .white
        
        
        self.navigationItem.rightBarButtonItems = [search_button]
        
        
        
        let height: CGFloat = 200
        let bounds = self.navigationController!.navigationBar.bounds
        self.navigationController?.navigationBar.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height + height)
               self.navigationController?.navigationBar.barTintColor = UIColor.primaryColor
                let radius:CGFloat = 30.0
                self.navigationController?.navigationBar.clipsToBounds = true
                self.navigationController?.navigationBar.layer.cornerRadius = radius
                self.navigationController?.navigationBar.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        
        
    }
    
    
    @objc func btnSearchClicked(sender:UIButton){
        
    }
    
   
    
}



extension UIViewController
{
    
    func askToOpenImage(delegate:UINavigationControllerDelegate&UIImagePickerControllerDelegate)
    {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.openImage(sourceType: .camera, delegate: delegate)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.openImage(sourceType: .savedPhotosAlbum, delegate: delegate)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func openImage(sourceType:UIImagePickerController.SourceType,delegate:UINavigationControllerDelegate&UIImagePickerControllerDelegate) {
        
        let imagePicker = UIImagePickerController()
        
        if UIImagePickerController.isSourceTypeAvailable(sourceType){
            imagePicker.delegate = delegate
            imagePicker.sourceType = sourceType
            imagePicker.allowsEditing = false
            present(imagePicker, animated: true, completion: nil)
        }else
        {
            AlertManager.showMessage(msg:"Image source not availble")
        }
    }
    
}


extension UIViewController {
    public var isVisible: Bool {
        if isViewLoaded {
            return view.window != nil
        }
        return false
    }
    
    public var isTopViewController: Bool {
        if self.navigationController != nil {
            return self.navigationController?.visibleViewController === self
        } else if self.tabBarController != nil {
            return self.tabBarController?.selectedViewController == self && self.presentedViewController == nil
        } else {
            return self.presentedViewController == nil && self.isVisible
        }
    }
}


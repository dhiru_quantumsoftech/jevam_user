//
//  DictionaryExtnsn.swift
//  Dhiru Boilerplate
//
//  Created by mac on 2/06/2019.
//  Copyright © 2017 Dhiru  All rights reserved.
//

import Foundation

extension Dictionary {
    static func className(_ aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
    
    
    func toJSONString() -> String {
        
       if let jsonData: NSData = try? JSONSerialization.data(
                        withJSONObject: self,
                        options: []) as NSData
       {
        
        return  String(data: jsonData as Data, encoding: String.Encoding.utf8) ?? ""
        
        }
        
        return "";
        
    }
    


}

extension Array
{
    static func className(_ aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
    
    
    func toJSONString() -> String {
        
        if let jsonData: NSData = try? JSONSerialization.data(
            withJSONObject: self,
            options: []) as NSData
        {
            
            return  String(data: jsonData as Data, encoding: String.Encoding.utf8) ?? ""
            
        }
        
        return "";
        
    }
    
}

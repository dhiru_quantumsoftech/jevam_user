//
//  PreferenceUtils.swift
//  Refund.me
//
//  Created by refund.me india on 03/12/17.
//  Copyright © 2017 refund.me india. All rights reserved.
//
let kRegexEmailValidate = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
import Foundation
import UIKit
class PreferenceUtils {
    
    fileprivate static let preference:UserDefaults = UserDefaults.standard
    
    
    public static func getString(_ key:String)->String?{
        return getString(key,nil)
    }
    
    public static func getString(_ key:String,_ defaultValue:String?)->String?{
        if let str = preference.string(forKey: key){
            return str
        }
        return defaultValue
        
    }
    
    public static func getBool(_ key:String)->Bool?{
        return getBool(key,nil)
    }
    
    public static func getBool(_ key:String,_ defaultValue:Bool?)->Bool{
        return  preference.bool(forKey: key)
    }
    
    public static func getInt(_ key:String)->Int?{
        return getInt(key,nil)
    }
    
    public static func getInt(_ key:String,_ defaultValue:Int?)->Int?{
        return preference.integer(forKey: key)
    }
    
    public static func getDouble(_ key:String,_ defaultValue:Double?)->Double?{
        return preference.double(forKey: key)
    }
    
    
    public static func putInt(_ key:String,value:Int){
        preference.set(value, forKey: key)
    }
    
    public static func putBool(_ key:String,value:Bool){
        preference.set(value, forKey: key)
    }
    
    public static func putDouble(_ key:String,value:Double){
        preference.set(value, forKey: key)
    }
    
    public static func putString(_ key:String,value:String){
        preference.setValue(value, forKey: key)
    }
    
    public static func getStringArray(_ key:String,defaultValue:[String]?)->[String]?{
        if let arr =  preference.stringArray(forKey: key){
            return arr
        }
        return defaultValue
    }
    
    public static func putStringArray(_ key:String,value:[String]){
        preference.setValue(value, forKey: key)
    }
    public static func setNilValueForKey(_ key: String){
        preference.setValue(nil, forKey: key)
    }
    
    class func validateEmail(_ strEmail: String) -> Bool {
        let emailTest = NSPredicate(format: "SELF MATCHES %@", kRegexEmailValidate)
        if emailTest.evaluate(with: strEmail) == false {
            return false
        }
        else {
            return true
        }
    }
    
    class func showAlertMethod(stringMessage:String, controller:UIViewController)  {
        let alertController = UIAlertController(title: nil, message:stringMessage, preferredStyle: UIAlertController.Style.alert)
        let cancel = UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel, handler: nil)
        alertController.addAction(cancel)
        controller.present(alertController, animated: true, completion: nil)
    }
}


//
//  UserSessionConfig.swift
//  Dhiru Boilerplate
//
//  Created by mac on 2/06/2019.
//  Copyright © 2017 Dhiru  All rights reserved.
//

import Foundation

extension URLSession
{
    
    func URLSession(session: URLSession, didReceiveChallenge challenge: URLAuthenticationChallenge, completionHandler: (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        completionHandler(
            .useCredential,
            URLCredential(trust: challenge.protectionSpace.serverTrust!)
        )
    }
    
}

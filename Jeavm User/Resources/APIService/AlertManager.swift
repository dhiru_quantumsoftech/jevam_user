//
//  AlertManager.swift
//  Dhiru Boilerplate
//
//  Created by mac on 2/06/2019.
//  Copyright © 2017 Dhiru  All rights reserved.
//
import Foundation
import UIKit

@objc class AlertManager: NSObject {
    
    class func showMessage(msg: String) {
        
        if !msg.isEmpty
        {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: AppName, message: msg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
        }
        }
    }
    
    class func showMessage(msg: String,action:UIAlertAction) {
        
        if !msg.isEmpty
        {
            DispatchQueue.main.async {
                let alert = UIAlertController(title: AppName, message: msg, preferredStyle: .alert)
                alert.addAction(action)
                UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
            }
        }
    }
  
    class func showMessage(title: String, msg: String) {
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
        }
    }
    
    class func showMessage(title: String, msg: String,action:UIAlertAction) {
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
            alert.addAction(action)
            UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
        }
    }
    
    
    class func showMessage(error: Error?) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Error ", message: error?.localizedDescription ?? "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
        }
    }
    
    class func showInternetError() {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Oopss..", message:"Please check your internet connection", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
        }
    }
    
    class func showUnknownError() {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Oopss..", message:"somthing went wrong", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
        }
    }
    
    class func showAlertWithTextField(message: String? = nil, placeholder: String? = nil, completion: @escaping ((String) -> Void) = { _ in }) {
         DispatchQueue.main.async {
        let alert = UIAlertController(title: AppName, message: message, preferredStyle: .alert)
        alert.addTextField() { newTextField in
            newTextField.placeholder = placeholder
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { _ in completion("") })
        alert.addAction(UIAlertAction(title: "Ok", style: .default) { action in
            if
                let textFields = alert.textFields,
                let tf = textFields.first,
                let result = tf.text
            { completion(result) }
            else
            { completion("") }
        })
        UIApplication.topViewController()?.present(alert, animated: true)
        }
    }
    
}

extension UIApplication {
    
    static func topViewController(base: UIViewController? = UIApplication.shared.delegate?.window??.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return topViewController(base: selected)
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        
        return base
    }
}


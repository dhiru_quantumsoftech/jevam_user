//
//  APIService.swift
//  Jeavm Business
//
//  Created by Dhiru on 8/7/19.
//  Copyright © 2019 Dhiru. All rights reserved.
//

import UIKit
import Alamofire

typealias successBlock = (_ resonseDict:[String:Any]) ->Void
//typealias successGenricBlock =  where T:Decodable
typealias failureBlock = (_ errMsg:String) ->Void


class APIService: NSObject {
    
    
    private static var Manager: Alamofire.SessionManager = {
        
        //l
        
        // Create the server trust policies
//        let serverTrustPolicies: [String: ServerTrustPolicy] = [
//            "staging.quantumsoftech.in": .disableEvaluation
//        ]
        
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
        isProduction ? "api.jeavm.com":"staging.quantumsoftech.in": .disableEvaluation
        ]
        
        // Create custom manager
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        let manager = Alamofire.SessionManager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )
        
        return manager
    }()
    
    
    static let isDebugging = true
    
   /* class func parseData(urlStr:String,parameters:NSDictionary?,methodType:HTTPMethod,showHud:Bool,successHandler:@escaping successBlock,failureHandler:@escaping failureBlock){
        if showHud {
            appDelegate.showIndicator()
        }
        var  headers: HTTPHeaders
            
        if let userInfo = UserSession.shared.getUserLoggedIn() {
            headers   = ["accept":"application/json","Content-Type":"application/json","Authorization":"Bearer \(userInfo.token ?? "")"]
        }else
        {
            headers   = [ "accept":"application/json","Content-Type":"application/json"]
        }
        
        if isDebugging
        {
            
        }
        
        var request :DataRequest!
        
        if methodType == .get
        {
            request =  APIService.Manager.request(urlStr, method: methodType,encoding: JSONEncoding.default, headers: headers)
        }else{
            request =  APIService.Manager.request(urlStr, method: methodType, parameters: parameters as? [String:Any], encoding: JSONEncoding.default, headers: headers)
        }
        
        
        
       request.responseJSON { response in
        
        appDelegate.hideIndicator()
            
            switch(response.result) {
            case .success(_):
                
                let statusCode = (response.response?.statusCode)!
                print("...HTTP code: \(statusCode)")
                
                if statusCode == 403 || statusCode == 401{
                    
                    //APIManager.logoutUser();
                   
                    if let _ = UserSession.shared.getUserLoggedIn() {
                        failureHandler("Session expired, please login again")
                         //appDelegate.setupAfterLogout()
                          return;
                    }
                }
                
                guard let json = response.result.value as? [String:Any] else{
                    failureHandler("Unkown error. Please try again.")
                    //appDelegate.hideIndicator()
                   return
                }
                
                
                guard let isSuccess = json["result"] as? String, isSuccess == "success"  else{
                    if let msg = json["message"] as? String{
                        print("Error",json)
                        failureHandler(msg)
                    }else{
                        failureHandler("Unkown error. Please try again.")
                    }
                   return
                }
                
                successHandler(json)
                //appDelegate.hideIndicator()
                break
                
            case .failure(_):
                failureHandler(response.result.error?.localizedDescription ?? "" )
                ///appDelegate.hideIndicator()
                break
                
            }
            
            appDelegate.hideIndicator()
            
            
        }
    }
    
    
    class func parseData<T>(urlStr:String,parameters:[String: Any]?,methodType:HTTPMethod,showHud:Bool,successHandler:@escaping (_ response:Response<T>) ->Void,failureHandler:@escaping failureBlock) where T:Decodable{
        if showHud {
            appDelegate.showIndicator()
        }
        var  headers: HTTPHeaders
        
        if let userInfo = UserSession.shared.getUserLoggedIn() {
            headers   = ["accept":"application/json","Content-Type":"application/json","Authorization":"Bearer \(userInfo.token ?? "")"]
        }else
        {
            headers   = [ "accept":"application/json","Content-Type":"application/json"]
        }
        
        print("👉URL:\(urlStr) \n\n Request:\(parameters?.toJSONString() ?? "-NA-")")
        
        
        var request :DataRequest!
        
        if methodType == .get
        {
            request =  APIService.Manager.request(urlStr, method: methodType,encoding: JSONEncoding.default, headers: headers)
        }else{
             request =  APIService.Manager.request(urlStr, method: methodType, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
        }
        
        request.responseJSON { response in
        // NO : methodType
        
            appDelegate.hideIndicator()
        
            switch(response.result) {
            case .success(_):
                
                let statusCode = (response.response?.statusCode)!
                print("...HTTP code: \(statusCode)")
                
                if statusCode == 403 || statusCode == 401 {
                   
                    if let _ = UserSession.shared.getUserLoggedIn() {
                        failureHandler("Session expired, please login again")
                        appDelegate.setupAfterLogout()
                        return;
                    }
                    
                }
    
                guard let data = response.data else { return }
                do {
                    print("👈 Response : \(data.convertToJSONString())")
                    let decoder = JSONDecoder()
                    let responseModel = try decoder.decode(Response<T>.self, from: data)
                    
                    if responseModel.result == "success"
                    {
                        successHandler(responseModel)
                        return
                    }else{
                        
                        if responseModel.error_type == "error_another_device_login"
                        {
                             successHandler(responseModel)
                        }else
                        {
                            failureHandler(responseModel.message ?? "Unkown Error , Try again")
                        }
                        return;
                     }
                   
                } catch let error {
                    print(error)
                   failureHandler("Unkown error. Parsing error.")
                }
                break
                
            case .failure(_):
                failureHandler(response.result.error?.localizedDescription ?? "" )
                appDelegate.hideIndicator()
                break
                
            }
        }  ///.debugLog()
    }
    
    
    class func parseDataWithImage<T>(urlStr:String,parameters:[String: Any]?,media:Media?,methodType:HTTPMethod,showHud:Bool,successHandler:@escaping (_ response:Response<T>) ->Void,failureHandler:@escaping failureBlock) where T:Decodable{
        if showHud {
            appDelegate.showIndicator()
        }
        
        
        print("Request POST : \(urlStr) \n\n \(parameters?.toJSONString() ?? "")")
        
        //        defer{
        //            appDelegate.hideIndicator()
        //        }
        //
        
        
        //        var  headers: HTTPHeaders
        //
        //        if let userInfo = UserSession.shared.getUserLoggedIn() {
        //            headers   = ["accept":"application/json","Content-Type":"multipart/form-data","Authorization":"Bearer \(userInfo.token ?? "")"]
        //        }else
        //        {
        //            headers   = [ "accept":"application/json","Content-Type":"application/json"]
        //        }
        //
        
        
        APIService.Manager.upload(
            multipartFormData: { MultipartFormData in
                //    multipartFormData.append(imageData, withName: "user", fileName: "user.jpg", mimeType: "image/jpeg")
                
                for (key, value) in parameters ?? [:] {
                    MultipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
                }
                
                if let media = media
                {
                    MultipartFormData.append(media.data, withName:media.key, fileName:media.filename, mimeType: media.mimeType)
                }
                //                MultipartFormData.append(UIImageJPEGRepresentation(UIImage(named: "1.png")!, 1)!, withName: "photos[2]", fileName: "swift_file.jpeg", mimeType: "image/jpeg")
                //
                
        }, to:urlStr) { (result) in
            
            appDelegate.hideIndicator()
            
            switch result {
                
                
                
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    print(response.result.value)
                    
                    let statusCode = (response.response?.statusCode)!
                    print("...HTTP code: \(statusCode)")
                    
                    if statusCode == 403 || statusCode == 401 {
                        
                        if let _ = UserSession.shared.getUserLoggedIn() {
                            failureHandler("Session expired, please login again")
                            appDelegate.setupAfterLogout()
                            return;
                        }
                        
                    }
                    
                    guard let data = response.data else { return }
                    do {
                        print("👈 Response : \(data.convertToJSONString())")
                        let decoder = JSONDecoder()
                        let responseModel = try decoder.decode(Response<T>.self, from: data)
                        
                        if (responseModel.result ?? "error") == "success"
                        {
                            successHandler(responseModel)
                            return
                        }else{
                            failureHandler(responseModel.message ?? "Unkown Error , Try again")
                            return;
                        }
                        
                    } catch let error {
                        print(error)
                        failureHandler("Unkown error. Parsing error.")
                    }
                    
                }
                
            case .failure(let encodingError):
            print(encodingError)
            failureHandler(encodingError.localizedDescription ?? "" )
            appDelegate.hideIndicator()
            }
            
            
        }
    }
    */
}


extension Request {
    public func debugLog() {
        #if DEBUG
        debugPrint(self)
        #endif
       // return self
    }
}

//
//  Response.swift
//  Dhiru Boilerplate
//
//  Created by mac on 2/06/2019.
//  Copyright © 2017 Dhiru  All rights reserved.
//

import Foundation

struct Response<T: Decodable>: Decodable {
    let data: T?
    let message:String?
    var result:String?
    var canServiceAdd:String?
    var error_type:String?
}



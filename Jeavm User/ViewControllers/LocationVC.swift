
//  LocationVC.swift
//  Jeavm User
//  Created by Sueb on 17/10/19.
//  Copyright © 2019 Sueb. All rights reserved.

import UIKit
import Alamofire
import AlamofireObjectMapper

var headerToken = ""
var selectedCity = "New Delhi"
var selectedCityID = ""
class LocationVC: BaseVC {

    @IBOutlet weak var scrlVw: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        headerToken = PreferenceUtils.getString("token") ?? ""
        selectedCity = PreferenceUtils.getString("selectedCity") ?? ""
        selectedCityID = PreferenceUtils.getString("selectedCityID") ?? ""
        
        if selectedCity.count == 0{
            selectedCity = "New Delhi"
        }
     
        if headerToken.count > 0{
            let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "rootController")
            self.navigationController?.pushViewController(initialViewController, animated: false)
         }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    @IBAction func currentLocationAction(_ sender: Any) {
       // webServiceCallingForGettingVersion()
       let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "rootController")
       self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    
    @IBAction func manuallyLocationAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Second", bundle: nil)
        let pvc = storyboard.instantiateViewController(withIdentifier: "SelectCityVC") as! SelectCityVC
        self.navigationController?.pushViewController(pvc, animated: true)
    }
    
    /*
     @IBAction func currentLocationActioon(_ sender: Any) {
     }
     // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


    func webServiceCallingForGettingVersion(){
         
         MDProgressView.shared.showProgressView(view: self.view)
         
         let parameters: [String: Any] = [
           "api_type":"userLogin",
            "userId":"9718050991",
            "loginType":"normal"
         ]
         
         print(parameters)
         
         Almgr.request(UrlConstant.BASE_URL, method: .post, parameters: parameters, encoding: JSONEncoding.default)
             .responseObject { (response:DataResponse<LoginResponse>) in
                 MDProgressView.shared.hideProgressView()
                 if response.result.isSuccess {
                    
                    let apiResult = response.result.value
                   
                    if apiResult!.result == "success"{
                        print("Success:: ",response)
                        let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "rootController")
                        self.navigationController?.pushViewController(initialViewController, animated: true)
                    }
                    
                    /* if(apiResult?.result == 1){
                         let appVersion = apiResult?.version ?? ""
                         let appVersionApp = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
                         if appVersionApp != appVersion{
                            // self.versionAlert()
                         }
                     }else{
                         //Toast(text :apiResult?.message! ,duration:Delay.short).show()
                     }
                     */
                    
                 }else{
                    // Toast(text :"Server Error",duration:Delay.short).show()
                 }
         }
     }
    
    
    let Almgr : Alamofire.SessionManager = {
        // Create the server trust policies
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            UrlConstant.BASE_test_url : .disableEvaluation
        ]
        // Create custom manager
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        let man = Alamofire.SessionManager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )
        return man
    }()
    
    public func AlertShow(title : NSString){
        
        //1. Create the alert controller.
        let alert = UIAlertController(title: "", message: title as String, preferredStyle: .alert)
        
        //2. Add the text field. You can configure it however you need.
        //3. Grab the value from the text field, and print it when the user clicks OK.
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { [weak alert] (_) in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
}

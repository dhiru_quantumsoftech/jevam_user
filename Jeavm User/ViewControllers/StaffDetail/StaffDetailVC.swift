//
//  StaffDetailVC.swift
//  Jeavm User
//
//  Created by Quantum softech on 11/11/19.
//  Copyright © 2019 Sueb. All rights reserved.
//

import UIKit

class StaffDetailVC: BaseVC {
      var sizingCell: TagCell?
    //// header
    @IBOutlet weak var backgroung_img: UIImageView!
    @IBOutlet weak var Staff_profile_img: UIImageView!
    @IBOutlet weak var deatail_view: UIView!
    //.......
    @IBOutlet weak var collectionview: CollectionView!
    @IBOutlet weak var flowLayout: FlowLayout!
    @IBOutlet weak var tableView: TableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cellNib = UINib(nibName: "TagCell", bundle: nil)
        self.sizingCell = (cellNib.instantiate(withOwner: nil, options: nil) as NSArray).firstObject as! TagCell?
        self.flowLayout.sectionInset = UIEdgeInsets.init(top: 2, left: 0, bottom: 2, right: 0)
  
        
        registerNib()
     
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.collectionview.delegate = self
        self.collectionview.dataSource = self
        
        //..Staff outline profile
        self.Staff_profile_img.layer.cornerRadius = Staff_profile_img.frame.height/2
        self.Staff_profile_img.clipsToBounds = true
        self.Staff_profile_img.layer.borderWidth = 1
        self.Staff_profile_img.layer.borderColor = UIColor.white.cgColor
        
        ///...detail .. view
        
        self.deatail_view.layer.cornerRadius = 24
        self.deatail_view.clipsToBounds = true
        
        
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        let size = collectionview.contentSize
//
//        if let headerView = self.tableView.tableHeaderView
//        {
//            var frame = headerView.frame
//            frame.size.height =  size.height
//            headerView.frame = frame
//            self.tableView.tableHeaderView = headerView
//        }
    }
    
    func registerNib()
    {
          let stafNib = UINib(nibName: "TagCell", bundle: nil)
          collectionview.register(stafNib, forCellWithReuseIdentifier: "TagCell")
        

         let ratingNib = UINib(nibName: "StaffReviewCell", bundle: nil)
        tableView.register(ratingNib, forCellReuseIdentifier: "StaffReviewCell")
    
      }
    
}
extension StaffDetailVC : UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "StaffReviewCell")as!StaffReviewCell
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          return UITableView.automaticDimension
    }
    
    
}
extension StaffDetailVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TagCell", for: indexPath) as! TagCell
        
         self.configureCell(cell, forIndexPath: indexPath)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         self.sizingCell?.setMaximumCellWidth(collectionView.frame.width)
         self.configureCell(self.sizingCell!, forIndexPath: indexPath)
         return self.sizingCell!.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
         
     }
    func configureCell(_ cell: TagCell, forIndexPath indexPath: IndexPath) {
          // cell.tagName.text = tag.name
    }
    
    
}

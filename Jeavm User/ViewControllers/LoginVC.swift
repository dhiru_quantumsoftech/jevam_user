
//  LoginVC.swift
//  Jeavm User
//  Created by Sueb on 16/10/19.
//  Copyright © 2019 Sueb. All rights reserved.

import UIKit
import SVPinView
import Toast_Swift
import Alamofire
import AlamofireObjectMapper

class LoginVC: BaseVC {
    
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var backImageVw: UIImageView!
    @IBOutlet weak var letCreateConstant: NSLayoutConstraint!
    @IBOutlet weak var emailConstant: NSLayoutConstraint!
    @IBOutlet weak var otpConstant: NSLayoutConstraint!
    @IBOutlet var emailVw: UIView!
    @IBOutlet var otpVw: UIView!
    @IBOutlet var parentView: UIView!
    @IBOutlet var userView: UIView!
   
    @IBOutlet var scrllVw: UIScrollView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var txfdFirstName: TextField!
    @IBOutlet weak var txfdLastName: TextField!
    @IBOutlet weak var txfdEmailMobile: TextField!
    var isSafeArea = false
    @IBOutlet weak var pinView: SVPinView!
    //passwordChange
   
    @IBOutlet weak var passwordConstant: NSLayoutConstraint!
    @IBOutlet var passwordVw: UIView!
    @IBOutlet weak var btnArrowContinue_Constant: NSLayoutConstraint!
    
    @IBOutlet weak var btnArrowConstants1: NSLayoutConstraint!
    @IBOutlet weak var btnArrowConstant: NSLayoutConstraint!

    var isNumber = false
    var emailAddress = ""
    var mobileNumber = ""
   
    override func viewDidLoad() {
          
      super.viewDidLoad()
        
      //Do any additional setup after loading the view.
       
      let screenSize = UIScreen.main.bounds
      print("screenSize",screenSize)
      let screenHeight = (screenSize.height)
      let screenWidth = (screenSize.width)
      print("Screen Height is::",screenHeight)
      print("Screen Width is::",screenWidth)
     
      if screenHeight == 667 || screenHeight == 736{
          btnArrowContinue_Constant.constant = -26
          btnArrowConstant.constant = -28
          btnArrowConstants1.constant = -22
      }else{
          btnArrowContinue_Constant.constant = -4
          btnArrowConstant.constant = -9
          btnArrowConstants1.constant = -8
      }
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @IBAction func submitActin(_ sender: Any) {
    
        if self.emailTxtField.text?.count == 0{
           //self.showToast(message:"Please enter email or mobile.")
             self.view.makeToast("Please enter email or mobile.", duration: 2.0, position: .top, title: "", image: nil)
           //self.AlertShow(title: "Please enter email or mobile." as NSString)
        }else if self.emailTxtField.text!.isContainsLetters{
            isNumber = false
            if(!PreferenceUtils.validateEmail(self.emailTxtField.text!)){
              //  self.showToast(message: "Please enter valid email address")
                self.view.makeToast("Please enter valid email address", duration: 2.0, position: .top, title: "", image: nil)
               //self.AlertShow(title: "Please enter valid email address." as NSString)
                return
            }
            self.emailAddress = self.emailTxtField.text ?? ""
            self.webServiceCallingForLogin()
            
        }else{
             isNumber = true
             self.mobileNumber = self.emailTxtField.text ?? ""
             self.webServiceCallingForLogin()
        }
    
    }
    
    func callLoginAnimation(){
        self.parentView.addSubview(self.otpVw)
               if self.isSafeArea{
                   self.otpVw.frame = CGRect(x: -self.view.frame.size.width, y: 85,width: self.view.frame.size.width - 65 ,height: self.view.frame.size.height - 219)
               }else{
                   self.otpVw.frame = CGRect(x: -self.view.frame.size.width, y: 85,width: self.view.frame.size.width - 65 ,height: self.view.frame.size.height - 175)
               }
               
               animateVw(view: otpVw, hideView: userView)
               backBtn.isHidden = true
               backBtn.tag = 1
              
    }
    
    func callOTPAnimation(){
           self.parentView.addSubview(self.otpVw)
                  if self.isSafeArea{
                      self.otpVw.frame = CGRect(x: -self.view.frame.size.width, y: 85,width: self.view.frame.size.width - 65 ,height: self.view.frame.size.height - 219)
                  }else{
                      self.otpVw.frame = CGRect(x: -self.view.frame.size.width, y: 85,width: self.view.frame.size.width - 65 ,height: self.view.frame.size.height - 175)
                  }
                  
                  animateVw(view: otpVw, hideView: passwordVw)
                  backBtn.isHidden = true
                  backBtn.tag = 1
                 
    }
    
    func callRegistrationAnimation(){
        self.parentView.addSubview(self.passwordVw)
                   if self.isSafeArea{
                       self.passwordVw.frame = CGRect(x: -self.view.frame.size.width, y: 50,width: self.view.frame.size.width - 65 ,height: self.view.frame.size.height - 205)
                   }else{
                       self.passwordVw.frame = CGRect(x: -self.view.frame.size.width, y: 50,width: self.view.frame.size.width - 65 ,height: self.view.frame.size.height - 180)
                   }
                   animateVw(view: passwordVw, hideView: userView)
                  // backBtn.isHidden = false
                   backBtn.tag = 3
    }
    
    func animateVw(view : UIView , hideView : UIView)   {
        //view.backgroundColor = UIColor.red
        UIView.animate(withDuration: 1.5, delay: 1.00, options: UIView.AnimationOptions(), animations: { () -> Void in
            
            if self.isSafeArea{
                view.frame = CGRect(x: 0, y: 85,width: UIScreen.main.bounds.size.width - 65 ,height: UIScreen.main.bounds.size.height - 219)
            }else{
                view.frame = CGRect(x: 0, y: 85,width: UIScreen.main.bounds.size.width - 65 ,height: UIScreen.main.bounds.size.height - 175)
            }
        }, completion: { (finished: Bool) -> Void in
            view.backgroundColor = UIColor.clear
            hideView.isHidden = true
            
        })
    }
    
    func animateHideVw(view : UIView)  {
        
        UIView.animate(withDuration: 1.5, delay: 1.00, options: UIView.AnimationOptions(), animations: { () -> Void in
            
            if self.isSafeArea{
                view.frame = CGRect(x: -self.view.frame.size.width, y: 50,width: self.view.frame.size.width - 65 ,height: self.view.frame.size.height - 205)
            }
            else{
                view.frame = CGRect(x: -self.view.frame.size.width, y: 50,width: self.view.frame.size.width - 65 ,height: self.view.frame.size.height - 180)
            }
        }, completion: { (finished: Bool) -> Void in
            view.backgroundColor = UIColor.clear
            view.removeFromSuperview()
            
        })
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
       
        /* if backBtn.tag == 1 {
            userView.isHidden = false
            backBtn.isHidden = true
            backBtn.tag = 0
            animateHideVw(view: otpVw)
        }
        if backBtn.tag == 2 {
            emailVw.isHidden = false
            backBtn.tag = 1
            animateHideVw(view: otpVw)
        }
        if backBtn.tag == 3 {
            otpVw.isHidden = false
            backBtn.tag = 2
            animateHideVw(view: passwordVw)
        }*/
    }
    
    @IBAction func btnForgotAction(_ sender: Any) {
        self.parentView.addSubview(self.otpVw)
        if self.isSafeArea{
            self.otpVw.frame = CGRect(x: -self.view.frame.size.width, y: 50,width: self.view.frame.size.width - 65 ,height: self.view.frame.size.height - 205)
        }else{
            self.otpVw.frame = CGRect(x: -self.view.frame.size.width, y: 50,width: self.view.frame.size.width - 65 ,height: self.view.frame.size.height - 180)
        }
        animateVw(view: otpVw, hideView: emailVw)
       // backBtn.isHidden = false
        backBtn.tag = 2
        
    }
    
    @IBAction func loginAction(_ sender: Any) {
        
    }
    
    @IBAction func facebookAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pvc = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(pvc, animated: true)
    }
    
    @IBAction func instagramAction(_ sender: Any) {
        
    }
    
    @IBAction func googleAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pvc = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(pvc, animated: true)
    }
    
    ///OtpView
    @IBAction func btnOtpSubmitAction(_ sender: Any) {
        if self.validation(){
          self.webServiceCallingForOTP_Verification()
         }
    }
    
    func validation() -> Bool
    {
        if self.pinView.getPin().isEmpty
        {
            AlertManager.showMessage(msg: "Please enter otp ")
            return false
        }else if self.pinView.getPin().count != 4
        {
            AlertManager.showMessage(msg: "Please enter valid otp")
            return false
        }
        
        return true
    }
    
    override func viewSafeAreaInsetsDidChange() {
        super.viewSafeAreaInsetsDidChange()
        
        if view.safeAreaInsets.bottom > 0 {
            //home indicator
            isSafeArea = true
            print("home indicator")
            letCreateConstant.constant = 50
            otpConstant.constant = 50
            emailConstant.constant = 50
            passwordConstant.constant = 50
        } else {
            isSafeArea = false
            print("no home indicator")
            //no home indicator
        }
    }
    
    @IBAction func PasswordVerifyAction(_ sender: Any) {
       
        if self.txfdFirstName.text?.count == 0{
             self.view.makeToast("Please enter first name", duration: 2.0, position: .top, title: "", image: nil)
        }else if self.txfdLastName.text?.count == 0{
             self.view.makeToast("Please enter last name", duration: 2.0, position: .top, title: "", image: nil)
        }else if isNumber && self.txfdEmailMobile.text?.count == 0{
             self.view.makeToast("Please enter email address", duration: 2.0, position: .top, title: "", image: nil)
        }else if isNumber == false && self.txfdEmailMobile.text?.count == 0{
            self.view.makeToast("Please enter mobile number", duration: 2.0, position: .top, title: "", image: nil)
        }else {
            
            if isNumber{
                emailAddress = self.txfdEmailMobile.text ?? ""
            }
            
            if isNumber == false{
                mobileNumber = self.txfdEmailMobile.text ?? ""
            }
            
            self.webServiceCallingForRegistration()
            
        }
       
    }
    
    public func AlertShow(title : NSString){
    
        let alert = UIAlertController(title: "", message: title as String, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { [weak alert] (_) in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func webServiceCallingForLogin(){
          
          MDProgressView.shared.showProgressView(view: self.view)
          
          let parameters: [String: Any] = [
            "api_type":"userLogin",
            "userId":self.emailTxtField.text ?? "",
             "loginType":"normal"
          ]
          
          print(parameters)
          
          Almgr.request(UrlConstant.BASE_URL, method: .post, parameters: parameters, encoding: JSONEncoding.default)
              .responseObject { (response:DataResponse<LoginResponse>) in
                  MDProgressView.shared.hideProgressView()
                  if response.result.isSuccess {
                     
                     let apiResult = response.result.value
                    if apiResult!.result == "success"{
                         print("Success:: ",response)
                          self.callLoginAnimation()
                    }else{
                        self.callRegistrationAnimation()
                    }
                     
                  }else{
                      self.view.makeToast("Server Error", duration: 2.0, position: .top, title: "", image: nil)
                  }
          }
      }
    
    func webServiceCallingForOTP_Verification(){
          
          MDProgressView.shared.showProgressView(view: self.view)
          
          let parameters: [String: Any] = [
            "api_type":"verify_otp",
            "userId":self.emailTxtField.text ?? "",
             "otp":self.pinView.getPin()
          ]
          
          print(parameters)
          
          Almgr.request(UrlConstant.BASE_URL, method: .post, parameters: parameters, encoding: JSONEncoding.default)
              .responseObject { (response:DataResponse<OTPResponse>) in
                  MDProgressView.shared.hideProgressView()
                  if response.result.isSuccess {
                    let apiResult = response.result.value
                    if apiResult!.result == "success"{
                         PreferenceUtils.putString("token", value: apiResult!.token ?? "")
                         print("Success:: ",response)
                         let storyboard = UIStoryboard(name: "Main", bundle: nil)
                         let pvc = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                         self.navigationController?.pushViewController(pvc, animated: true)
                    }
                     
                  }else{
                    self.view.makeToast("Server Error", duration: 2.0, position: .top, title: "", image: nil)
                  }
          }
      }
    
    func webServiceCallingForRegistration(){
          
          MDProgressView.shared.showProgressView(view: self.view)
          
          let parameters: [String: Any] = [
            "api_type":"userRegistration",
            "firstName":self.txfdFirstName.text ?? "",
            "lastName":self.txfdLastName.text ?? "",
            "email":self.emailAddress,
            "phone": self.mobileNumber
          ]

          print(parameters)
          
          Almgr.request(UrlConstant.BASE_URL, method: .post, parameters: parameters, encoding: JSONEncoding.default)
              .responseObject { (response:DataResponse<LoginResponse>) in
                  MDProgressView.shared.hideProgressView()
                  if response.result.isSuccess {
                    let apiResult = response.result.value
                    if apiResult!.result == "success"{
                         print("Success:: ",response)
                        self.callOTPAnimation()
                    }
                     
                  }else{
                      self.view.makeToast("Server Error", duration: 2.0, position: .top, title: "", image: nil)
                  }
          }
      }
    
     
     let Almgr : Alamofire.SessionManager = {
         // Create the server trust policies
         let serverTrustPolicies: [String: ServerTrustPolicy] = [
             UrlConstant.BASE_test_url : .disableEvaluation
         ]
         // Create custom manager
         let configuration = URLSessionConfiguration.default
         configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
         let man = Alamofire.SessionManager(
             configuration: URLSessionConfiguration.default,
             serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
         )
         return man
     }()
     
}



extension String{
    var isContainsLetters : Bool{
        let letters = CharacterSet.letters
        return self.rangeOfCharacter(from: letters) != nil
    }
}

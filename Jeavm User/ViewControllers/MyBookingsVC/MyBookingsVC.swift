//
//  MyBookingsVC.swift
//  Jeavm User
//
//  Created by Purnendu on 08/11/19.
//  Copyright © 2019 Sueb. All rights reserved.
//

import UIKit
import Toast_Swift
import Alamofire
import AlamofireObjectMapper

class MyBookingListCell:UITableViewCell{
    
    @IBOutlet weak var lblBookingNo: UILabel!
    @IBOutlet weak var detailCardView: CardView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var logoCardView: CardView!
    
}
class PastBookingListCell:UITableViewCell{
    
    @IBOutlet weak var lblBookingNo: UILabel!
      @IBOutlet weak var detailCardView: CardView!
      @IBOutlet weak var lblDate: UILabel!
      @IBOutlet weak var lblTime: UILabel!
      @IBOutlet weak var lblTitle: UILabel!
      @IBOutlet weak var logo: UIImageView!
      @IBOutlet weak var logoCardView: CardView!
}

class MyBookingsVC: BaseVC,UITableViewDelegate,UITableViewDataSource {
   
    var upcomming : Array<UpcommingRespoce> = [UpcommingRespoce]()
    var pastBooking : Array<PastResponce> = [PastResponce]()
    @IBOutlet weak var tableview2: UITableView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var upcomingView: UIView!
    @IBOutlet weak var pastView: UIView!
    
    override func viewDidLoad() {
          super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableview2.delegate = self
        self.tableview2.dataSource = self
        
      }
  
    
    override func viewDidAppear(_ animated: Bool) {
        self.webServiceForBookingList()
      }
    
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1 {
            return upcomming.count
        }else{
            return pastBooking.count
        }
          
          }
    
          func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if tableView.tag == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "MyBookingListCell")as!MyBookingListCell
                cell.lblBookingNo.text = "BookingId-\(upcomming[indexPath.row].bookingId ?? "")"
                if let value = self.upcomming[indexPath.row].image{
                                     let imageUrl = value
                                     cell.logo.sd_setImage(with: URL(string: imageUrl ), placeholderImage: UIImage(named: PlaceHolderImage))
                             }
                cell.lblTitle.text = upcomming[indexPath.row].outletName ?? ""
                
                cell.lblTime.text = upcomming[indexPath.row].displayTime
                
                cell.lblDate.text =  upcomming[indexPath.row].displayDate
                
                  return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "PastBookingListCell")as!PastBookingListCell
                     cell.lblBookingNo.text = "BookingId-\(pastBooking[indexPath.row].bookingId ?? "")"
                           if let value = self.pastBooking[indexPath.row].image{
                                                let imageUrl = value
                                                cell.logo.sd_setImage(with: URL(string: imageUrl ), placeholderImage: UIImage(named: PlaceHolderImage))
                                        }
                           cell.lblTitle.text = pastBooking[indexPath.row].outletName ?? ""
                           
                          cell.lblTime.text = pastBooking[indexPath.row].displayTime
                                   
                        cell.lblDate.text =  pastBooking[indexPath.row].displayDate
                return cell
            }
     
          }
        
      func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
      return 80
     }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Second", bundle: nil)
        let pvc = storyboard.instantiateViewController(withIdentifier: "BookingDetailVC") as! BookingDetailVC
        self.navigationController?.pushViewController(pvc, animated: true)
    }

    
    ////...# Api for Booking
     
     func webServiceForBookingList(){
           
           MDProgressView.shared.showProgressView(view: self.view)
           
           let parameters: [String: Any] = [
              "api_type":"bookingList",
             "userId":"86",
             "start":"0",
             "limit":"10"
     ]
         
         print(UrlConstant.BASE_URL)
           print(parameters)
           
           Almgr.request(UrlConstant.BASE_URL, method: .post, parameters: parameters, encoding: JSONEncoding.default)
               .responseObject { (response:DataResponse<BookingListRespnce>) in
                   MDProgressView.shared.hideProgressView()
                 
                  //let json = response.result.value as? [String:Any]
                  print("Response :\(response.data?.convertToJSONString() ?? "-NA-")")
                 
                   if response.result.isSuccess {
                      let apiResult = response.result.value
                     if apiResult!.result == "success"{
                          print("Success:: ",response)
                        self.upcomming = (apiResult?.data?.upcoming)!
                        self.pastBooking = (apiResult?.data?.past)!
                         self.tableView.reloadData()
                        self.tableview2.reloadData()
                     }else{
                        
                     }
                      
                   }else{
                       self.view.makeToast("Server Error", duration: 2.0, position: .top, title: "", image: nil)
                   }
           }
       }
     
      let Almgr : Alamofire.SessionManager = {
          // Create the server trust policies
          let serverTrustPolicies: [String: ServerTrustPolicy] = [
              UrlConstant.BASE_test_url : .disableEvaluation
          ]
          // Create custom manager
          let configuration = URLSessionConfiguration.default
          configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
          let man = Alamofire.SessionManager(
              configuration: URLSessionConfiguration.default,
              serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
          )
          return man
      }()
     public func AlertShow(title : NSString){
     
         let alert = UIAlertController(title: "", message: title as String, preferredStyle: .alert)
         
         alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { [weak alert] (_) in
             
         }))
         
         self.present(alert, animated: true, completion: nil)
         
     }
}

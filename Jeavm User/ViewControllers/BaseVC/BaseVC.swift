//
//  BaseVC.swift
//  Jeavm User
//
//  Created by Purnendu on 19/11/19.
//  Copyright © 2019 Sueb. All rights reserved.
//

import UIKit

class BaseVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let color1 = hexStringToUIColor(hex: "#FF973E")
        setStatusBarBackgroundColor(color:color1)
    }

    func showToast(message : String) {

        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: 100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.green
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 15;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
        
    }
    
    func setStatusBarBackgroundColor(color: UIColor) {
                
               
                if #available(iOS 13.0, *) {
                    let app = UIApplication.shared
                              let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                              
                              let statusbarView = UIView()
                              statusbarView.backgroundColor = color
                              view.addSubview(statusbarView)
                            
                              statusbarView.translatesAutoresizingMaskIntoConstraints = false
                              statusbarView.heightAnchor
                                  .constraint(equalToConstant: statusBarHeight).isActive = true
                              statusbarView.widthAnchor
                                  .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                              statusbarView.topAnchor
                                  .constraint(equalTo: view.topAnchor).isActive = true
                              statusbarView.centerXAnchor
                                  .constraint(equalTo: view.centerXAnchor).isActive = true
                            
                  
                } else  {
                       guard  let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey: "statusBar") as? UIView else {
                           return
                       }
                       statusBar.backgroundColor = color
                       }
                
            }
    
}

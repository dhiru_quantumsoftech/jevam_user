//
//  StaffClcCell.swift
//  Jeavm Business
//
//  Created by Refundme on 8/8/19.
//  Copyright © 2019 Quantumsoftech. All rights reserved.
//

import UIKit

class StaffClcCell: UICollectionViewCell {
    

    @IBOutlet weak var lblStaffName: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    
    @IBOutlet weak var imgSelectedImgOverlay: UIImageView!

    
    @IBOutlet weak var imgRatingStar: UIImageView!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
   
}

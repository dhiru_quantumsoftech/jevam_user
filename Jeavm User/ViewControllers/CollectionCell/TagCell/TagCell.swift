//
//  TagCell.swift
//  TagFlowLayout
//
//  Created by Diep Nguyen Hoang on 7/30/15.
//  Copyright (c) 2015 CodenTrick. All rights reserved.
//

import UIKit

class TagCell: UICollectionViewCell {
    
    
    @IBOutlet weak var btnBgGradient: Button!
    
    @IBOutlet weak var tagName: UILabel!
    @IBOutlet weak var tagNameMaxWidthConstraint: NSLayoutConstraint!

    private var horizontalPadding: CGFloat = 32

    func setMaximumCellWidth(_ width: CGFloat) {
        self.tagNameMaxWidthConstraint.constant = width - horizontalPadding
    }
    
    override func awakeFromNib() {
        self.backgroundColor = UIColor.clear
        self.tagName.textColor = UIColor.white
        self.tagName.numberOfLines = 0
        //self.layer.cornerRadius = 4
    }
}

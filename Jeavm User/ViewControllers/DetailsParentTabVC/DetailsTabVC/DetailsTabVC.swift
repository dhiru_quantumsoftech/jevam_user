//
//  DetailsTabVC.swift
//  Jeavm User
//
//  Created by Purnendu on 07/11/19.
//  Copyright © 2019 Sueb. All rights reserved.
//

import UIKit

class AddressDetailsCell:UITableViewCell{
    
}

class ContactDetailsCell:UITableViewCell{
    
}

class SocialHandleDetailsCell:UITableViewCell{
    
}

class AgeOfSalonDetailsCell:UITableViewCell{
    
}

class BusinessHourDetailsHeaderCell:UITableViewCell{
    
}

class BusinessHourDetailsCell:UITableViewCell{
    
}

class AcceptedModeOfPaymentsCell:UITableViewCell{
    
}

class ReportDetailsCell:UITableViewCell{
    
}



class DetailsTabVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return 8
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddressDetailsCell")as!AddressDetailsCell

                      return cell
        }else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactDetailsCell")as!ContactDetailsCell

                      return cell
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SocialHandleDetailsCell")as!SocialHandleDetailsCell

                      return cell
        }else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AgeOfSalonDetailsCell")as!AgeOfSalonDetailsCell

                      return cell
        }else if indexPath.row == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessHourDetailsHeaderCell")as!BusinessHourDetailsHeaderCell

                      return cell
        }
        else if indexPath.row == 5{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessHourDetailsCell")as!BusinessHourDetailsCell

                      return cell
        }else if indexPath.row == 6{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AcceptedModeOfPaymentsCell")as!AcceptedModeOfPaymentsCell

                      return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReportDetailsCell")as!ReportDetailsCell

                      return cell
        
          
        }
       }
       
       func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           
       }
       
       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
             if indexPath.row == 0{
                   return 80
                }else if indexPath.row == 1 {
                     return 80
                }else if indexPath.row == 2{
                   return 90
                }else if indexPath.row == 3{
                    return 80
                }else if indexPath.row == 4{
                    return 35
                }else if indexPath.row == 5{
                    return 35
                }else if indexPath.row == 6{
                    return 80
                }else{
                    return 100
                }
             
       }
    
    /*func scrollViewDidScroll(_ scrollView: UIScrollView) {
           
           let y =  scrollView.contentOffset.y
           let value = 350 - y
           let height = min(max(value, 60), 350)
          
           print("scrollView.contentOffset.y:: ",y)
           print("height:: ",height)
           bannerHeight = Int(height)
           NotificationCenter.default.post(name: Notification.Name("bannerHideNotification"), object: nil)
       
       }
 */

}

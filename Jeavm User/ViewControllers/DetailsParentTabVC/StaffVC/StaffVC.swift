
//  StaffVC.swift
//  Jeavm User
//  Created by Purnendu on 18/10/19.
//  Copyright © 2019 Sueb. All rights reserved.

import UIKit
import Toast_Swift
import Alamofire
import AlamofireObjectMapper
class StaffCell:UICollectionViewCell{
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization codzz
        img_Profile.layer.cornerRadius = img_Profile.frame.size.width/2
        img_Profile.clipsToBounds = true
    }
    
    @IBOutlet weak var img_Profile: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTitle1: UILabel!
    @IBOutlet weak var review_View: UIView!
    @IBOutlet weak var lblRating: UILabel!
    
}

class StaffVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 40
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : StaffCell? = (collectionView.dequeueReusableCell(withReuseIdentifier: "StaffCell", for: indexPath)as!  StaffCell)
        cell?.review_View.clipsToBounds = true
        cell?.review_View.layer.cornerRadius = 9
        let borderColor = hexStringToUIColor(hex: "#FF973E")
        cell?.review_View.layer.borderColor = borderColor.cgColor
        cell?.review_View.layer.borderWidth = 1
       
       // cell?.img_Profile.layer.cornerRadius = cell!.img_Profile.frame.size.width/2
        
        return cell!
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let screenSize = UIScreen.main.bounds
        
        print("screenSize",screenSize)
        
        let screenWidth = (screenSize.width*0.23)
        //let screenHeight = (screenSize.height*0.10)
        
        return CGSize(width:screenWidth , height:120)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Second", bundle: nil)
              let pvc = storyboard.instantiateViewController(withIdentifier: "StaffDetailVC") as! StaffDetailVC
              self.navigationController?.pushViewController(pvc, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
           
           let y =  scrollView.contentOffset.y
           let value = 350 - y
           let height = min(max(value, 60), 350)
          
           print("scrollView.contentOffset.y:: ",y)
           print("height:: ",height)
           bannerHeight = Int(height)
           NotificationCenter.default.post(name: Notification.Name("bannerHideNotification"), object: nil)
       }
}




//  ChildTabVC.swift
//  Jeavm User
//  Created by Purnendu on 17/10/19.
//  Copyright © 2019 Sueb. All rights reserved.

import UIKit

class ItemCell:UITableViewCell{
    
    override func awakeFromNib() {
           super.awakeFromNib()
           // Initialization codzz
           userImg.layer.cornerRadius = userImg.frame.size.width/2
           userImg.clipsToBounds = true
       }
    
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
}

class ChildTabVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 30
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell")as!ItemCell

        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension

    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
           
           let y =  scrollView.contentOffset.y
           let value = 350 - y
           let height = min(max(value, 60), 350)
          
           print("scrollView.contentOffset.y:: ",y)
           print("height:: ",height)
           bannerHeight = Int(height)
           NotificationCenter.default.post(name: Notification.Name("bannerHideNotification"), object: nil)
       
       }
    
}


//  ServicesVC.swift
//  Jeavm User
//  Created by Sueb on 25/10/19.
//  Copyright © 2019 Sueb. All rights reserved.


import UIKit

class ServicesCell: UITableViewCell {
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var strockPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
  
        // Initialization code
    }
}

class ServicesHeaderCell: UITableViewCell {
    
    @IBOutlet weak var headerMainCatLbl: UILabel!
    @IBOutlet weak var headerSubCatLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
}

class ServicesVC: UIViewController,UITableViewDataSource,UITableViewDelegate  {
    
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var btnFilterView: CardView!
    @IBOutlet weak var catView: CardView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnFilter.layer.cornerRadius = 28
        self.btnFilterView.layer.cornerRadius = 28
        self.btnFilterView.clipsToBounds = true
        self.btnFilter.clipsToBounds = true
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "ServicesHeaderCell") as! ServicesHeaderCell
        
         headerView.addSubview(headerCell)
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServicesCell")as!ServicesCell
            cell.strockPrice.attributedText = "₹1500".strikeThrough()
        return cell
        
    }
    
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         let storyboard = UIStoryboard(name: "Dialer", bundle: nil)
         let pvc = storyboard.instantiateViewController(withIdentifier: "DialerVC") as! DialerVC
         self.navigationController?.pushViewController(pvc, animated: true)
      }
    
    @IBAction func btnFilter(_ sender: Any) {
         self.catView.isHidden = false
    }
    
    @IBAction func btnFemale(_ sender: Any) {
         self.catView.isHidden = true
    }
    
    @IBAction func btnMale(_ sender: Any) {
        self.catView.isHidden = true
    }
    
    @IBAction func btnMaleFemale(_ sender: Any) {
        self.catView.isHidden = true
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let y =  scrollView.contentOffset.y
        let value = 350 - y
        let height = min(max(value, 60), 350)
       
        print("scrollView.contentOffset.y:: ",y)
        print("height:: ",height)
        bannerHeight = Int(height)
        NotificationCenter.default.post(name: Notification.Name("bannerHideNotification"), object: nil)
    
    }


}

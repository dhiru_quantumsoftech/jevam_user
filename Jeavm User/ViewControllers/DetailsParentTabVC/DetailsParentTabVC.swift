
//  DetailsParentTabVC.swift
//  Jeavm User
//  Created by Purnendu on 17/10/19.
//  Copyright © 2019 Sueb. All rights reserved.

import UIKit
import CarbonKit

var bannerHeight = 0

class DetailsParentTabVC: BaseVC,CarbonTabSwipeNavigationDelegate {
    @IBOutlet weak var headerViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var ratingView: CardView!
    @IBOutlet weak var topConstraints: NSLayoutConstraint!
    @IBOutlet weak var tabBarView: UIView!
    @IBOutlet weak var targetView: UIView!
    @IBOutlet weak var headerView: CardView!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var toolBar: UIToolbar!
    let itemsArray = ["SERVICES","DETAILS","REVIEW","PORTFOLIO","STAFF"]
    @IBOutlet weak var topView: UIView!
   
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var headerViewConstraintsTop: NSLayoutConstraint!
   
    override func viewDidLoad() {
        
        super.viewDidLoad()
      
        
        topConstraints.constant = 350
        headerViewConstraintsTop.constant = -50
        headerView.isHidden = true
        let tabSwipe = CarbonTabSwipeNavigation(items: itemsArray,toolBar:self.toolBar, delegate: self)
        let selectedColor = hexStringToUIColor(hex: "#FF973E")
        let normalTextColor = hexStringToUIColor(hex: "#d3d3d3")
        tabSwipe.setTabExtraWidth(0)
        ratingView.layer.borderWidth = 1
        ratingView.layer.borderColor = selectedColor.cgColor
        tabSwipe.setSelectedColor(selectedColor, font: UIFont(name:"OpenSans-Light",size:14)!)
        tabSwipe.setNormalColor(UIColor.black, font: UIFont(name:"OpenSans-Light",size:14)!)
        
        tabSwipe.setIndicatorColor(selectedColor)
        tabSwipe.setNormalColor(normalTextColor)
        tabSwipe.setSelectedColor(UIColor.white)
        //tabSwipe.pagesScrollView?.isScrollEnabled = false
        
        let bounds = UIScreen.main.bounds
        print("Screen Width:: ",bounds.size.width)
       
        tabSwipe.insert(intoRootViewController: self, andTargetView: self.targetView);
        
        tabSwipe.setIndicatorHeight(30)
        let bgColor = hexStringToUIColor(hex: "#FFFFFF")
        tabSwipe.carbonSegmentedControl?.backgroundColor = bgColor
        self.toolBar.barTintColor = UIColor.white
        self.toolBar.layer.cornerRadius = 12
        self.toolBar.clipsToBounds = true
        //self.toolBar.backgroundColor = UIColor.clear
       //self.toolBar.backgroundColor = UIColor.clear
        self.tabBarView.layer.cornerRadius = 18
        self.tabBarView.clipsToBounds = true
        
        self.tabBarView.layer.shadowOffset = CGSize.zero
        self.tabBarView.layer.shadowOpacity = 1
        self.tabBarView.layer.shadowRadius = 5
        self.tabBarView.layer.masksToBounds = false
        self.tabBarView.layer.shadowColor = hexStringToUIColor(hex: "#d3d3d3").cgColor
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotificationBannerHide(notification:)), name: Notification.Name("bannerHideNotification"), object: nil)
        
//        tabSwipe.carbonSegmentedControl!.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
//        tabSwipe.carbonSegmentedControl!.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
//        tabSwipe.carbonSegmentedControl!.layer.shadowOpacity = 2.0
//        tabSwipe.carbonSegmentedControl!.layer.shadowRadius = 10.0
//        tabSwipe.carbonSegmentedControl!.layer.masksToBounds = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
         self.topView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 300)
    }
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBackHeader(_ sender: Any) {
    }
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        guard let storyboard = storyboard else { return UIViewController() }
        if index == 4{
            return storyboard.instantiateViewController(withIdentifier: "StaffVC")
        }else if index == 0{
            return storyboard.instantiateViewController(withIdentifier: "ServicesVC")
        }else if index == 1{
             return storyboard.instantiateViewController(withIdentifier: "DetailsTabVC")
        }else if index == 3{
            return storyboard.instantiateViewController(withIdentifier: "PortFolioVC")
        }else{
            return storyboard.instantiateViewController(withIdentifier: "ChildTabVC")
        }
        
    }
    
    @objc func methodOfReceivedNotificationBannerHide(notification: Notification) {
            
               print("Banner Height Changed:: ",bannerHeight)
               if bannerHeight <= 60{
                   headerViewConstraintsTop.constant = 0
                   let targetViewTopConstraints = bannerHeight + 50
                   topConstraints.constant = CGFloat(targetViewTopConstraints)
                   topViewHeight.constant =  CGFloat(bannerHeight)
                   btnBack.isHidden = true
                   btnShare.isHidden = true
                   headerView.isHidden = false
                 
               }else{
                    headerViewConstraintsTop.constant = -50
                    topViewHeight.constant =  CGFloat(bannerHeight)
                    topConstraints.constant = CGFloat(bannerHeight)
                    btnBack.isHidden = false
                    btnShare.isHidden = false
                    
                    if bannerHeight <= 150{
                        headerView.isHidden = true
                    }
               }
      }
    
   
}




//  PortFolioVC.swift
//  Jeavm User
//  Created by Sueb on 17/10/19.
//  Copyright © 2019 Sueb. All rights reserved.

import SquareFlowLayout
import UIKit

class PortFolioCell: UICollectionViewCell {
     @IBOutlet weak var imgVw: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization codzz
        imgVw.layer.cornerRadius = 11
        
    }
}

class PortFolioVC: UIViewController,cvprotocols,SquareFlowLayoutDelegate {
    enum CellType {
        case normal
        case expanded
    }
    private var photos: [Int : UIImage] = [:]
    @IBOutlet private var collectionView: UICollectionView!
    private let layoutValues: [CellType] = [
        .expanded, .normal, .normal,
        .normal, .normal, .normal,
        .normal, .normal, .normal,
        .normal, .expanded, .normal,
        .expanded, .normal, .normal,
        .normal, .expanded, .normal,
        .normal, .normal, .normal,
        .normal, .normal, .expanded,
        .normal, .normal, .normal,
        .normal, .expanded, .normal,
        .normal, .normal, .normal,
        .expanded, .normal, .normal,
        .normal, .normal, .normal,
        .normal, .expanded, .normal,
        .normal, .normal, .normal,
        .normal, .normal, .normal,
        .expanded, .normal, .normal,
        .normal, .normal, .normal,
        .normal, .normal, .expanded,
        .normal, .expanded, .normal,
        .normal, .normal, .normal,
        .normal, .normal, .normal,
        .expanded, .normal, .normal,
        .normal, .expanded, .normal,
        .normal, .normal, .normal,
        .normal, .normal, .expanded,
        .expanded, .normal, .normal
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        let flowLayout = SquareFlowLayout()
        flowLayout.flowDelegate = self
        self.collectionView.collectionViewLayout = flowLayout
        for i in 0..<self.layoutValues.count {
            self.photos[i] = UIImage()
            ImageLoader.load(from: self.url(at: i + 1)) { image in
                self.photos[i] = image
                self.collectionView.reloadItems(at: [IndexPath(row: i, section: 0)])
            }
        }
        self.collectionView.reloadData()
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        guard let flowLayout = self.collectionView.collectionViewLayout as? SquareFlowLayout else {
            return
        }
        flowLayout.invalidateLayout()
    }
    func shouldExpandItem(at indexPath: IndexPath) -> Bool {
        return self.layoutValues[indexPath.row] == .expanded
    }
    private func url(at index: Int) -> URL {
        return URL(string: "https://randomfox.ca/images/\(index).jpg")!
    }
    //CollectionView Method
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          return self.photos.keys.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PortFolioCell", for: indexPath)as!PortFolioCell
          cell.imgVw.image = self.photos[indexPath.row]
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let totalwidth = collectionView.bounds.size.width - 40;
        let numberOfCellsPerRow = 3
        let oddEven = indexPath.row / numberOfCellsPerRow % 2
        let dimensions = CGFloat(Int(totalwidth) / numberOfCellsPerRow)
        if (oddEven == 0) {
            return CGSize(width: dimensions , height: dimensions)
        } else {
            return CGSize(width: dimensions , height: dimensions * 2)
        }
  
    }
    @nonobjc func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 10
    }


    func scrollViewDidScroll(_ scrollView: UIScrollView) {
              
              let y =  scrollView.contentOffset.y
              let value = 350 - y
              let height = min(max(value, 60), 350)
             
              print("scrollView.contentOffset.y:: ",y)
              print("height:: ",height)
              bannerHeight = Int(height)
              NotificationCenter.default.post(name: Notification.Name("bannerHideNotification"), object: nil)
          
          }

}

//
//  DiscountQuickView.swift
//  Jeavm Business
//
//  Created by Quantum softech on 01/10/19.
//  Copyright © 2019 Quantumsoftech. All rights reserved.
//

import UIKit

class DiscountQuickView: UIViewController {
  
    
    
    @IBOutlet weak var lbl_discountValue: UILabel!
    @IBOutlet weak var lbl_priceAfterDiscount: UILabel!
    @IBOutlet weak var lbl_DiscountPercent: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    

    @IBAction func tapGesture(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_close(_ sender: Any) {
          self.dismiss(animated: true, completion: nil)
    }
    
}

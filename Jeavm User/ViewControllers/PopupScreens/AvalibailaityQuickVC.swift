//
//  AvalibailaityQuickVC.swift
//  Jeavm User
//
//  Created by Quantum softech on 14/11/19.
//  Copyright © 2019 Sueb. All rights reserved.
//

import UIKit

class AvalibailaityQuickVC: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableview.delegate = self
        self.tableview.dataSource = self
        registerNib()
    }
    func registerNib()
      {
           let ratingNib = UINib(nibName: "WorkingDayTblCell", bundle: nil)
          tableview.register(ratingNib, forCellReuseIdentifier: "WorkingDayTblCell")
      
        }
      
    @IBAction func tapGesture(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
extension AvalibailaityQuickVC : UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WorkingDayTblCell")as!WorkingDayTblCell
               return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableView.automaticDimension
       }
    
}

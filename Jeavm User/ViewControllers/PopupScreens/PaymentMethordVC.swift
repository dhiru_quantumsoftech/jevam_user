//
//  PaymentMethordVC.swift
//  Jeavm User
//
//  Created by Quantum softech on 07/11/19.
//  Copyright © 2019 Sueb. All rights reserved.
//

import UIKit

class PaymentMethordVC: UIViewController {
    
    var selectedPaymentType:PaymentMode?
    
    /// payment Methord
    @IBOutlet weak var btn_paytm: UIButton!
    @IBOutlet weak var btn_debitcard: UIButton!
    @IBOutlet weak var btn_netbanking: UIButton!
    @IBOutlet weak var btn_cash: UIButton!
    
    ///Amount to be Paid
    
    @IBOutlet weak var lbl_amounttobepaid: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

    }
/// btn confirm paymnet
    @IBAction func btn_confirm(_ sender: Any) {
        
    }
    
    //btn Paytm
    
    @IBAction func btn_paytm(_ sender: Any) {
        selectedPaymentType =  selectedPaymentType != .paytm ?.paytm:nil
              reloadButtons()
    }
    //btn debit/credit/atm
    
    @IBAction func btn_debit(_ sender: Any) {
        selectedPaymentType =  selectedPaymentType != .debit_credit_card ?.debit_credit_card:nil
         reloadButtons()
    }
    
    ///btn net banking
    @IBAction func btn_netBanking(_ sender: Any) {
        selectedPaymentType =  selectedPaymentType != .netbanking ?.netbanking:nil
            reloadButtons()
    }

    ///btn cash payment
    @IBAction func btn_cashpaymnet(_ sender: Any) {
        selectedPaymentType =  selectedPaymentType != .cash ?.cash:nil
             reloadButtons()
    }
    
    
    
    func reloadButtons()
     {
         if let type = selectedPaymentType
         {
            switch type {
             
            case .paytm:
             btn_paytm.isSelected = true
             btn_debitcard.isSelected = false
             btn_netbanking.isSelected = false
             btn_cash.isSelected = false
             
            case .debit_credit_card:
             btn_paytm.isSelected = false
             btn_debitcard.isSelected = true
             btn_netbanking.isSelected = false
             btn_cash.isSelected = false
             
            case .cash:
             btn_paytm.isSelected = false
             btn_debitcard.isSelected = false
             btn_netbanking.isSelected = true
             btn_cash.isSelected = false
             
            case .netbanking:
             btn_paytm.isSelected = false
             btn_debitcard.isSelected = false
             btn_netbanking.isSelected = false
             btn_cash.isSelected = true
            }
         }else
         {
             btn_paytm.isSelected = false
             btn_debitcard.isSelected = false
             btn_netbanking.isSelected = false
             btn_cash.isSelected = false
             
         }
     }
    
    

}
enum PaymentMode:String
{
    case paytm = "Paytm"
    case debit_credit_card = "Card"
    case netbanking = "net"
    case cash = "Cash"
}


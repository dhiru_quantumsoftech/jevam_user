//
//  LeftMenuViewController.swift
//  AKSideMenuSimple
//
//  Created by Diogo Autilio on 6/7/16.
//  Copyright © 2016 AnyKey Entertainment. All rights reserved.

import UIKit
import AKSideMenu

class DrawerItemCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblUnderLine: UILabel!
    
}

var isOpenHome = false

public class LeftMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    var titles = ["Home","Notifications","Profile","My Bookings","Terms & Condition",  "Privacy Policy", "Support","Logout"]
    
    var images = ["IconHome", "IconCalendar", "IconProfile", "IconSettings"]
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    override public func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         self.sideMenuViewController?.hideMenuViewController()
        if indexPath.row == 2{
            let storyboard = UIStoryboard(name: "Cart", bundle: nil)
            let pvc = storyboard.instantiateViewController(withIdentifier: "StaffEditProfileVC") as! StaffEditProfileVC
            self.navigationController?.pushViewController(pvc, animated: true)
        }else if indexPath.row == 3{
            let storyboard = UIStoryboard(name: "Second", bundle: nil)
            let pvc = storyboard.instantiateViewController(withIdentifier: "MyBookingsVC") as! MyBookingsVC
            self.navigationController?.pushViewController(pvc, animated: true)
        }
    }
    
    // MARK: - <UITableViewDataSource>
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection sectionIndex: Int) -> Int {
        return titles.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DrawerItemCell")as!DrawerItemCell
        cell.lblTitle?.text = titles[indexPath.row]
        cell.img?.image = UIImage(named: titles[indexPath.row])
        
        return cell
        
    }
    
    @IBAction func btnSettings(_ sender: Any) {
    
    }
    
    @IBAction func btnInbox(_ sender: Any) {
        // self.comingSoonPopup()
       
    }
    
    @IBAction func btnLogout(_ sender: Any) {
     
        
    }
}


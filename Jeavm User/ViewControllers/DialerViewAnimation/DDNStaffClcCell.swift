//
//  DDNStaffClcCell.swift
//  Dhiru CircularCollection
//
//  Created by Dhiru on 11/14/19.
//  Copyright © 2019 QuantumSoftech All rights reserved.
//

import UIKit

class DDNStaffClcCell: UICollectionViewCell {


    @IBOutlet weak var lblStaffName: UILabel!
    @IBOutlet weak var imgStaffAvatar: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    

    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
             super.apply(layoutAttributes)
             let circularlayoutAttributes = layoutAttributes as! CircularCollectionViewLayoutAttributes
             self.layer.anchorPoint = circularlayoutAttributes.anchorPoint
             self.center.y += (circularlayoutAttributes.anchorPoint.y - 0.5)*self.bounds.height
    }
}

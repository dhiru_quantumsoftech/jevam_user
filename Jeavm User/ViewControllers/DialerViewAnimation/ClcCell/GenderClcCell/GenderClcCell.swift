//
//  GenderClcCell.swift
//  Dhiru CircularCollection
//
//  Created by Refundme on 11/20/19.
//  Copyright © 2019 QuantumSoftech All rights reserved.
//

import UIKit

class GenderClcCell: UICollectionViewCell {

    @IBOutlet weak var imgIcon:UIImageView!
    
    
override func awakeFromNib() {
            super.awakeFromNib()
            // Initialization code
        }
        

        override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
                 super.apply(layoutAttributes)
                 let circularlayoutAttributes = layoutAttributes as! CircularCollectionViewLayoutAttributes
                 self.layer.anchorPoint = circularlayoutAttributes.anchorPoint
                 self.center.y += (circularlayoutAttributes.anchorPoint.y - 0.5)*self.bounds.height
        }
    }

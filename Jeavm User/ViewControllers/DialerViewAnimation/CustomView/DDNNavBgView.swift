//
//  DDNNavBgView.swift
//  Dhiru CircularCollection
//
//  Created by Refundme on 11/20/19.
//  Copyright © 2019 QuantumSoftech. All rights reserved.
//

import Foundation
import UIKit

class DDNNavBgView: UIView {

override func layoutSubviews() {
    super.layoutSubviews()
    
    let path = UIBezierPath()
    path.move(to: CGPoint(x: 0, y: self.frame.height - self.frame.height/4 ))
    path.addQuadCurve(to: CGPoint(x: self.frame.width, y: self.frame.height - self.frame.height/4 ), controlPoint: CGPoint(x: self.frame.width / 2 , y: self.frame.height ))
    
    path.addLine(to: CGPoint(x: self.frame.width , y: 0))
    path.addLine(to: CGPoint(x: 0 , y: 0))
    //path.addLine(to: CGPoint(x: 0, y: self.frame.height))
    path.close()
    
    
    let maskLayer = CAShapeLayer()
    maskLayer.path = path.cgPath
    maskLayer.fillColor = UIColor.red.cgColor //self.backgroundColor!.cgColor
    
    self.layer.mask = maskLayer    //insertSublayer(maskLayer, at: 0)
    self.layer.masksToBounds = true
}
}

//
//  DDNRoundedView.swift
//  Dhiru CircularCollection
//
//  Created by Refundme on 11/20/19.
//  Copyright © 2019 QuantumSoftech All rights reserved.
//

import Foundation
import UIKit

class DDNRoundView: UIView {

override func layoutSubviews() {
    super.layoutSubviews()
    self.layer.cornerRadius = self.frame.height/2
    self.layer.masksToBounds = true
}

}

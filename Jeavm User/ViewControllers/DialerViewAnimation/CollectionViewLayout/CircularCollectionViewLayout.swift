//
//  CircularCollectionViewLayout.swift
//  Dhiru CircularCollection
//
//  Created by Dhiru on 11/14/19.
//  Copyright © 2019 QuantumSoftech All rights reserved.
//

import Foundation
import UIKit

class CircularCollectionViewLayoutAttributes: UICollectionViewLayoutAttributes {
  
  var anchorPoint = CGPoint(x: 0.5, y: 0.5)
  
  var angle: CGFloat = 0 {
    didSet {
        zIndex = Int(angle*1000000)
        transform = CGAffineTransform(rotationAngle: angle)
    }
  }
  
    override func copy(with zone: NSZone? = nil) -> Any {
        let copiedAttributes: CircularCollectionViewLayoutAttributes = super.copy(with: zone) as! CircularCollectionViewLayoutAttributes
           copiedAttributes.anchorPoint = self.anchorPoint
           copiedAttributes.angle = self.angle
           return copiedAttributes
    }
  
}

class CircularCollectionViewLayout: UICollectionViewLayout {
  
  let itemSize = CGSize(width: 100, height: 120)
  
  var angleAtExtreme: CGFloat {
    return collectionView!.numberOfItems(inSection: 0) > 0 ? -CGFloat(collectionView!.numberOfItems(inSection: 0)-1)*anglePerItem : 0
  }
  
  var angle: CGFloat {
    return angleAtExtreme*collectionView!.contentOffset.x/(collectionViewContentSize.width - collectionView!.bounds.width)
  }
  
  var radius: CGFloat = 190 {
    didSet {
      invalidateLayout()
    }
  }
  
  var anglePerItem: CGFloat {
    return atan((itemSize.width/radius) + 0.18)
  }
  
  var attributesList = [CircularCollectionViewLayoutAttributes]()
  
    override var collectionViewContentSize: CGSize {
        return CGSize(width: CGFloat(collectionView!.numberOfItems(inSection: 0))*itemSize.width,
                        height: collectionView!.bounds.height)
    }

    
    override class var layoutAttributesClass: AnyClass
    {
        return CircularCollectionViewLayoutAttributes.self
    }

    override func prepare() {
        super.prepare()
        let centerX =  collectionView!.contentOffset.x  + (collectionView!.bounds.width/2.0)
        let anchorPointY = ((itemSize.height/2.0) + radius)/itemSize.height
    //1
        let theta = atan2(collectionView!.bounds.width/2.0, radius + (itemSize.height/2.0) - (collectionView!.bounds.height/2.0)) //1
    //2
    var startIndex = 0
        var endIndex = collectionView!.numberOfItems(inSection: 0) - 1
    //3
    if (angle < -theta) {
      startIndex = Int(floor((-theta - angle)/anglePerItem))
    }
    //4
    endIndex = min(endIndex, Int(ceil((theta - angle)/anglePerItem)))
    //5
    if (endIndex < startIndex) {
      endIndex = 0
      startIndex = 0
    }
    attributesList = (startIndex...endIndex).map { (i) -> CircularCollectionViewLayoutAttributes in
        let attributes = CircularCollectionViewLayoutAttributes(forCellWith: NSIndexPath(item: i, section: 0) as IndexPath)
    attributes.size = self.itemSize
    attributes.center = CGPoint(x: centerX, y: self.collectionView!.bounds.midY)
    attributes.angle = self.angle + (self.anglePerItem*CGFloat(i))
    attributes.anchorPoint = CGPoint(x: 0.5, y: anchorPointY)
        
//    attributes?.transform = CGAffineTransform(
//          translationX: 0,
//          y: 500.0
//        )
        // Set Content Offset
//        attributes.transform = CGAffineTransform(
//          scaleX: 0.1,
//          y: 0.1
//        )
        
        
    return attributes
        
    }
  }
  

  
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        return attributesList
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return attributesList[indexPath.row]
    }
    

    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
  
    
    override func initialLayoutAttributesForAppearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        
        let attributes =  attributesList[itemIndexPath.row]
            attributes.alpha = 0.0
            attributes.transform = CGAffineTransform(
                  translationX: 0,
                  y: 500.0
                )
        return attributes
//        let attributes = super.initialLayoutAttributesForAppearingItem(at: itemIndexPath)
//
//           attributes?.alpha = 0.0
//           attributes?.transform = CGAffineTransform(
//             scaleX: 0.1,
//             y: 0.1
//           )
//
//        return attributes
    }
    
}

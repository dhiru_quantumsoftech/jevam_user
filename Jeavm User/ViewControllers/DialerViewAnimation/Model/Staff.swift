//
//  Staff.swift
//  Dhiru CircularCollection
//
//  Created by Refundme on 11/20/19.
//  Copyright © 2019 QuantumSoftech. All rights reserved.
//

import Foundation
import UIKit

struct Staff {
    var name:String
    var rating:String
    var image:UIImage
}

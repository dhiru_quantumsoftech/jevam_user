//
//  Gender.swift
//  Dhiru CircularCollection
//
//  Created by Refundme on 11/20/19.
//  Copyright © 2019 QuantumSoftech All rights reserved.
//

import Foundation
import UIKit
enum Gender:String
{
    case male = "male"
    case female = "female"
    
    var icon:UIImage
    {
        switch self {
            
        case .male:
            return #imageLiteral(resourceName: "ic_men")
        case .female:
             return #imageLiteral(resourceName: "ic_girl")
        }
    }
}

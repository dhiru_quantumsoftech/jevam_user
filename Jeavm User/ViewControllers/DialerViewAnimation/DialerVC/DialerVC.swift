//
//  DialerVC.swift
//  Jeavm User
//
//  Created by Purnendu on 22/11/19.
//  Copyright © 2019 Sueb. All rights reserved.
//

import UIKit

class DialerVC: UIViewController {

   @IBOutlet weak var clcDialerView: UICollectionView!
        
        @IBOutlet weak var dialerContainerView: DDNHalfRoundView!
        
        //Slecttion Title
        @IBOutlet weak var lblSelectionTitle: UILabel!
        
        //Seleccted COntainer  Views
        @IBOutlet weak var selctedGenderContainer: DDNRoundView!
        @IBOutlet weak var selctedDateContainer: DDNRoundView!
        @IBOutlet weak var selectedStaffContainer: DDNRoundView!
        @IBOutlet weak var selectedTimeContainer: DDNRoundView!
        
        @IBOutlet weak var viewConfirmationContainer: UIView!
        
        
        // Selcted Views
        @IBOutlet weak var imgSelectedGender: UIImageView!
        @IBOutlet weak var lblSelectedDate: UILabel!
        @IBOutlet weak var imgSelectedStaff: UIImageView!
        @IBOutlet weak var lblSelectedTime: UILabel!
        
        //selected View Model
        let viewModel = BookingSelectionVM()
        
        override func viewDidLoad() {
            super.viewDidLoad()
            // Do any additional setup after loading the view.
            registerNib()
            
            
            
            //clcDialerView.isHidden = true
            reloadSelectedViews()
        }
        
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
            reloadSelectedViews()
         }
        
        
        
        func registerNib()
        {
            let nib =  UINib(nibName: "DDNStaffClcCell", bundle: nil)
            clcDialerView.register(nib, forCellWithReuseIdentifier: "DDNStaffClcCell")
            
            let nibGender =  UINib(nibName: "GenderClcCell", bundle: nil)
             clcDialerView.register(nibGender, forCellWithReuseIdentifier: "GenderClcCell")
            
            
            let nibTime = UINib(nibName: "DateTimeClcCell", bundle: nil)
            clcDialerView.register(nibTime, forCellWithReuseIdentifier: "DateTimeClcCell")
            
        }
        
        @IBOutlet weak var bottomActionView: DDNArcView!
        
        @IBOutlet weak var btnBottomActionButton: UIButton!
        
        
        @IBAction func btnBottomAction(_ sender: Any) {
            
            switch viewModel.selectionStage {
            case .genderSelection:
                print("Go to back")
                self.navigationController?.popViewController(animated: true)
            case .dateSelection:
                viewModel.changeStageMannually(stage: .genderSelection)
            case .staffSelection:
                viewModel.changeStageMannually(stage: .dateSelection)
            case .timeSlot:
                viewModel.changeStageMannually(stage: .staffSelection)
            case .confirmationView:
                viewModel.changeStageMannually(stage: .timeSlot)
            }
            
             self.reloadSelectedViews()
             
            /*
             btnBottomActionButton.isSelected = !btnBottomActionButton.isSelected
             
             if btnBottomActionButton.isSelected
             {
             let scaledTrf = CGAffineTransform(scaleX: 4.0, y: 4.0)
             let translate = scaledTrf.translatedBy(x: 0, y: -10)
             UIView.animate(withDuration: 1, animations: {
             self.bottomActionView.transform = translate  //CGAffineTransform(scaleX: 4.0, y: 4.0)
             //                self.bottomActionView.tr
             }) { (completed) in }
             
             }else
             {
             UIView.animate(withDuration: 1, animations: {
             self.bottomActionView.transform = .identity
             }) { (completed) in }
             }
             */
            
        }
        
        
        func reloadSelectedViews()
        {
            switch viewModel.selectionStage {
            case .genderSelection:
                self.lblSelectionTitle.text = "Select Gender"
                self.selctedGenderContainer.isHidden = true
                self.selctedDateContainer.isHidden = true
                self.selectedStaffContainer.isHidden = true
                self.selectedTimeContainer.isHidden = true
                self.viewConfirmationContainer.isHidden = true
            case .dateSelection:
                self.lblSelectionTitle.text = "Appointment Date?"
                self.selctedGenderContainer.isHidden = false
                self.selctedDateContainer.isHidden = true
                self.selectedStaffContainer.isHidden = true
                self.selectedTimeContainer.isHidden = true
                self.viewConfirmationContainer.isHidden = true
            case .staffSelection:
                self.lblSelectionTitle.text = "Select Staff"
                self.selctedGenderContainer.isHidden = false
                self.selctedDateContainer.isHidden = false
                self.selectedStaffContainer.isHidden = true
                self.selectedTimeContainer.isHidden = true
                 self.viewConfirmationContainer.isHidden = true
            case .timeSlot:
                self.lblSelectionTitle.text = "What Time?"
                self.selctedGenderContainer.isHidden = false
                self.selctedDateContainer.isHidden = false
                self.selectedStaffContainer.isHidden = false
                self.selectedTimeContainer.isHidden = true
                 self.viewConfirmationContainer.isHidden = true
            case .confirmationView:
                self.lblSelectionTitle.text = ""
                self.selctedGenderContainer.isHidden = false
                self.selctedDateContainer.isHidden = false
                self.selectedStaffContainer.isHidden = false
                self.selectedTimeContainer.isHidden = false
                self.viewConfirmationContainer.isHidden = false
            }
            
            //load data on selected View
           if let gender = viewModel.selectedGender
           {
                self.imgSelectedGender.image = gender.icon
            }
             
            if let date = viewModel.selectedDate
            {
                lblSelectedDate.text = Util.displayDate(date:date)
            }
            
            if let staff = viewModel.selectedStaff
            {
                 self.imgSelectedStaff.image = staff.image
            }
            
            if let date = viewModel.selectedTime
            {
                lblSelectedTime.text = Util.displayTime(date:date)
            }
            
           // self.clcDialerView.reloadData()
            
           
             self.clcDialerView.collectionViewLayout.invalidateLayout()
               
            
            DispatchQueue.main.asyncAfter(deadline: .now() +  0.3) {
                 self.clcDialerView.reloadData()
                let xOffset:CGFloat = self.viewModel.selectionStage == .genderSelection ? -120.0:self.clcDialerView.frame.width/4
                self.clcDialerView.contentOffset = CGPoint(x:xOffset , y: 0)
                self.clcDialerView.isScrollEnabled = self.viewModel.selectionStage == .genderSelection ? false:true
            }
            
        }
        
        
        
        
        
        
    }

    extension DialerVC:UICollectionViewDelegate,UICollectionViewDataSource
    {
        public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            switch viewModel.selectionStage {
            case .genderSelection:
                return 2
            case .dateSelection:
                return self.viewModel.listOfDate.count
            case .staffSelection:
                return self.viewModel.staffList.count
            case .timeSlot:
                return self.viewModel.listOfTime.count
            case .confirmationView:
                return 0
            }
        }
        
        public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
             switch viewModel.selectionStage {
            
             case .genderSelection:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GenderClcCell", for: indexPath) as! GenderClcCell
                   cell.imgIcon.image = indexPath.row == 0 ? Gender.male.icon:Gender.female.icon
                   fadeInAnimation(cell:cell)
                   return cell
             case .dateSelection:
                  let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DateTimeClcCell", for: indexPath) as! DateTimeClcCell
                  cell.lblTitle.text = Util.displayDate(date: self.viewModel.listOfDate[indexPath.row])
                  cell.viewRoundBG.backgroundColor = Util.voiletColor
                  fadeInAnimation(cell:cell)
                    return cell
             case .staffSelection:
                 let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DDNStaffClcCell", for: indexPath) as! DDNStaffClcCell
                   
                 let staff = viewModel.staffList[indexPath.row]
                 cell.lblStaffName.text = staff.name
                 cell.imgStaffAvatar.image =  staff.image
                 
                       fadeInAnimation(cell:cell)
                       return cell
             case .timeSlot:
                  let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DateTimeClcCell", for: indexPath) as! DateTimeClcCell
                     cell.lblTitle.text = Util.displayTime(date: self.viewModel.listOfTime[indexPath.row])
                     cell.viewRoundBG.backgroundColor = Util.orangeColor
                    fadeInAnimation(cell:cell)
                    return cell
             case .confirmationView:
                 let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GenderClcCell", for: indexPath) as! GenderClcCell
                     fadeInAnimation(cell:cell)
                     return cell
            }
           
        }
        
        
        
        func fadeInAnimation(cell:UICollectionViewCell)
        {
               cell.alpha =  0.0
               UIView.animate(withDuration: 0.5, animations: {
                        cell.alpha =  1.0
                }) { (comlpeted) in }
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            //flyAnimateCell(cell:collectionView.cellForItem(at: indexPath)!)
           switch viewModel.selectionStage {
              case .genderSelection:
                selectGender(indexPath: indexPath)
              case .dateSelection:
                selectDate(indexPath:indexPath)
            case .staffSelection:
                selectStaff(indexPath: indexPath)
            case .timeSlot:
                selecteTime(indexPath: indexPath)
            case .confirmationView:
                  print("Invalid Condition")
            }
        
        }
        
        
        
        
        
        public func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
            
        }
        
        
        func flyAnimateCell(cell:UICollectionViewCell,target:UIView)
        {
            
             let viewInFrame = cell.convert(cell.bounds, to: self.view)
             let snapShotView = UIImageView(frame: viewInFrame)    //cell.contentView
             //snapShotView.frame = viewInFrame
             snapShotView.alpha = 0.3
             snapShotView.image = cell.screenshot
             self.view.addSubview(snapShotView)
             
             UIView.animate(withDuration: 0.5, animations: {
             cell.alpha =  1.0
             snapShotView.alpha = 0.8
             snapShotView.transform = .identity
             snapShotView.frame = target.frame
             }) { (comlpeted) in
             if comlpeted
             {
             snapShotView.removeFromSuperview()
             target.isHidden = false
             self.reloadSelectedViews()
             //self.clcDialerView.reloadData()
             }
             }
             
        }
        
        
        
        func selectGender(indexPath:IndexPath)
        {
             let gender =  indexPath.row == 0 ? Gender.male:Gender.female
             self.viewModel.selectGender(gender: gender) {
                
                guard  let cell = clcDialerView.cellForItem(at: indexPath) else
                {
                    return
                }
                self.flyAnimateCell(cell: cell, target: self.selctedGenderContainer)
                //self.reloadSelectedViews()
            }
        }
        
        func selectDate(indexPath:IndexPath)
        {
             let date =  self.viewModel.listOfDate[indexPath.row]
             self.viewModel.selectDate(date: date) {
                 guard  let cell = clcDialerView.cellForItem(at: indexPath) else
                {
                    return
                }
                self.flyAnimateCell(cell: cell, target: self.selctedDateContainer)
                // self.reloadSelectedViews()
            }
        }
        
        func selectStaff(indexPath:IndexPath)
        {
            let staff =  self.viewModel.staffList[indexPath.row]
                    self.viewModel.selectStaff(staff: staff) {
                       
                       guard  let cell = clcDialerView.cellForItem(at: indexPath) else
                       {
                           return
                       }
                self.flyAnimateCell(cell: cell, target: self.selectedStaffContainer)
                       // self.reloadSelectedViews()
            }
        }
        
        func selecteTime(indexPath:IndexPath)
        {
                let date =  self.viewModel.listOfTime[indexPath.row]
                self.viewModel.selectTime(time: date) {
                      
                       guard  let cell = clcDialerView.cellForItem(at: indexPath) else
                       {
                           return
                       }
                       self.flyAnimateCell(cell: cell, target: self.selectedTimeContainer)
    //                   self.reloadSelectedViews()
                   }
        }
        
        
    }

    extension UIView{
        
        var screenshot: UIImage{
            UIGraphicsBeginImageContext(self.bounds.size);
            let context = UIGraphicsGetCurrentContext();
            self.layer.render(in: context!)
            let screenShot = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            return screenShot!
        }
    }

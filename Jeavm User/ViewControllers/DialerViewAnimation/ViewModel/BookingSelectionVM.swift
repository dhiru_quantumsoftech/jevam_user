//
//  BookingSelectionVM.swift
//  Dhiru CircularCollection
//
//  Created by Refundme on 11/20/19.
//  Copyright © 2019 QuantumSoftech. All rights reserved.
//

import Foundation

//typealias Result = (_ success: Bool, _ msg:String) -> Void
typealias CompletionHandler = () -> Void


protocol  BookingSelectionVMProtocal {
    
    func selectGender(gender:Gender,result:CompletionHandler)
    func selectDate(date:Date,result:CompletionHandler)
    func selectStaff(staff:Staff,result:CompletionHandler)
    func selectTime(time:Date,result:CompletionHandler)
    
    func changeStageMannually(stage:SelectionStage)
}

class BookingSelectionVM
{
    var selectionStage:SelectionStage = .genderSelection
   
    var selectedGender:Gender?
    
    var selectedDate:Date?
    
    var selectedStaff:Staff?
    
    var selectedTime:Date?
 
    
    init() {
    
        for i in 0..<8
        {
            let staff = Staff(name: "Name \(i)", rating: "\(i).0", image: #imageLiteral(resourceName: "ic_user_placeholder"))
            staffList.append(staff)
        }
        
        for i in 0...7
        {
           if let date =  Calendar.current.date(byAdding: .day, value: i, to: Date())
           {
               listOfDate.append(date)
            }
        }
        
       for i in 0...7
        {
               if  let date =  Calendar.current.date(byAdding: .hour, value: i, to: Date())
               {
                   listOfTime.append(date)
            }
        }
        
    }
    
    var staffList = [Staff]()
    var listOfDate = [Date]()
    var listOfTime = [Date]()
    
}



extension BookingSelectionVM:BookingSelectionVMProtocal
{
  
    func changeStageMannually(stage: SelectionStage) {
        switch stage {
          case .genderSelection:
            self.selectedGender = nil
            self.selectedDate = nil
            self.selectedStaff = nil
            self.selectedTime = nil
          case .dateSelection:
             self.selectedDate = nil
             self.selectedStaff = nil
             self.selectedTime = nil
          case .staffSelection:
             self.selectedStaff = nil
             self.selectedTime = nil
          case .timeSlot:
            self.selectedTime = nil
          case .confirmationView:
            print("This is the confirmation Page")
        }
        
        self.selectionStage = stage
        
    }
    
    func selectGender(gender: Gender, result: () -> Void) {
        self.selectedGender =  gender
        self.selectionStage = .dateSelection
        result()
    }
    
    func selectDate(date: Date, result: () -> Void) {
        self.selectedDate = date
        self.selectionStage = .staffSelection
        result()
    }
    
    func selectStaff(staff: Staff, result: () -> Void) {
        self.selectedStaff = staff
        self.selectionStage = .timeSlot
        result()
    }
    
    func selectTime(time: Date, result: () -> Void) {
        self.selectedTime = time
        self.selectionStage = .confirmationView
        result()
    }
    
    
}


enum SelectionStage
{
    case genderSelection
    case dateSelection
    case staffSelection
    case timeSlot
    case confirmationView
}

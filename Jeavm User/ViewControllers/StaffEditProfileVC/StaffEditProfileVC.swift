//
//  StaffEditProfileVC.swift
//  Jeavm User
//
//  Created by Quantum softech on 13/11/19.
//  Copyright © 2019 Sueb. All rights reserved.
//

import UIKit

class StaffEditProfileVC: BaseVC {
    var selectedStaff:buttontype?
    @IBOutlet weak var btn_female: UIButton!
    @IBOutlet weak var btn_male: UIButton!
    @IBOutlet weak var btn_camera: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btn_camera.layer.cornerRadius = btn_camera.frame.height/2
        self.btn_camera.clipsToBounds = true
        selectedStaff =  selectedStaff != .male ?.male:nil
                     reloadButtons()
        
    }
    
    @IBAction func btn_male(_ sender: Any) {
     selectedStaff =  selectedStaff != .male ?.male:nil
              reloadButtons()
    }
    @IBAction func btn_female(_ sender: Any) {
        selectedStaff =  selectedStaff != .female ?.female:nil
                 reloadButtons()

    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func reloadButtons()
    {
        if let type = selectedStaff
        {
        switch type {
               case .male:
                btn_male.isSelected = true
                btn_female.isSelected = false
               case .female:
                btn_male.isSelected = false
                btn_female.isSelected = true
               }
            }else
            {
                btn_male.isSelected = false
                btn_female.isSelected = false
            }
      }
}
enum buttontype : String
{
  case male = "Male"
  case female = "Female"
}

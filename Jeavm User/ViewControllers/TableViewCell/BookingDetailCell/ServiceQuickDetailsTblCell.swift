//
//  ServiceQuickDetailsTblCell.swift
//  Jeavm Business
//
//  Created by Refundme on 8/21/19.
//  Copyright © 2019 Quantumsoftech. All rights reserved.
//

import UIKit

class ServiceQuickDetailsTblCell: UITableViewCell {

    
    @IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var lblServiceBy: UILabel!
    
    
    @IBOutlet weak var lblServiceTime: UILabel!
    @IBOutlet weak var lblServiceDate: UILabel!
    @IBOutlet weak var lblServiceDuration: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
  
  
    
}

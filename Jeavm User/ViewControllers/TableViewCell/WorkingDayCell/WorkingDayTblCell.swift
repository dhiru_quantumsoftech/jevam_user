//
//  WorkingDayTblCell.swift
//  Jeavm Business
//
//  Created by Refundme on 8/29/19.
//  Copyright © 2019 Quantumsoftech. All rights reserved.
//

import UIKit

class WorkingDayTblCell: UITableViewCell {

    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblWorkingHours: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

    
}

//
//  CartTableCell.swift
//  Jeavm User
//
//  Created by Quantum softech on 06/11/19.
//  Copyright © 2019 Sueb. All rights reserved.
//

import UIKit

class CartTableCell: UITableViewCell{
  

    @IBOutlet weak var tableview: TableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
          registerNib()
        tableview.delegate = self
        tableview.dataSource =  self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
           

    }
    
    func registerNib()
       {
           let ratingNib = UINib(nibName: "AdditionslServiceCell", bundle: nil)
           tableview.register(ratingNib, forCellReuseIdentifier: "AdditionslServiceCell")
       }

      
    
}
extension CartTableCell : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
              let cell = tableView.dequeueReusableCell(withIdentifier: "AdditionslServiceCell")as!AdditionslServiceCell
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}


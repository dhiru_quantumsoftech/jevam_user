//
//  OtpPopupVC.swift
//  Jeavm Business
//
//  Created by Refundme on 10/3/19.
//  Copyright © 2019 Quantumsoftech. All rights reserved.
//

import UIKit
import SVPinView


class OtpVC: UIViewController {
   @IBOutlet weak var letCreateConstant: NSLayoutConstraint!
    
    @IBOutlet weak var pinView: SVPinView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UIDevice().type.rawValue == "iPhone X" || UIDevice().type.rawValue == "iPhone XS" || UIDevice().type.rawValue == "iPhone XS Max" || UIDevice().type.rawValue == "iPhone XR" || UIDevice().type.rawValue == "iPhone XR"{
            letCreateConstant.constant = 50
        }
        else{
        }
        // Do any additional setup after loading the view.
    }
    

    
    @IBAction func btnSubmitAction(_ sender: Any) {
        if validation()
        {
        
        }
    }
    
    
    func validation() -> Bool
       {
           if self.pinView.getPin().isEmpty
           {
               AlertManager.showMessage(msg: "Please enter otp ")
               return false
           }else if self.pinView.getPin().count != 4
           {
               AlertManager.showMessage(msg: "Please enter valid otp")
               return false
           }
           
         return true
       }


}

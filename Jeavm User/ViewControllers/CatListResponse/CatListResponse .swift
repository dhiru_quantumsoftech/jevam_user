
//  ClaimListResponse.swift
//  Refund.me
//
//  Created by refund.me india on 04/12/17.
//  Copyright © 2017 refund.me india. All rights reserved.


import Foundation
import AlamofireObjectMapper
import ObjectMapper

class LoginResponse : Mappable{
    
    var  message:String?
    var  result : String?
    var  data : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        message <- map["message"]
        result <- map["result"]
        data <- map["data"]
        
    }
}

class OTPResponse : Mappable{
    
    var  message:String?
    var  result : String?
    var  token : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        message <- map["message"]
        result <- map["result"]
        token <- map["data.token"]
        
    }
}

class CityListResponse : Mappable{
    
    var  message:String?
    var  result : String?
    var  cityList : [CityListArray]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        message <- map["message"]
        result <- map["result"]
        cityList <- map["data"]
        
    }
}

class CityListArray : Mappable{
    
    var  id:String?
    var  city_name : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        city_name <- map["city_name"]
        
    }
}


class OutletByPinCodeResponce : Mappable{
    
    var  message:String?
    var  result : String?
    var  data : [OutletArrayResponce]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        message <- map["message"]
        result <- map["result"]
        data <- map["data"]
        
    }
}
class OutletArrayResponce : Mappable{
    var id : String?
    var  beautyOutletCode:String?
    var  outletName : String?
    var  contactPersonName:String?
    var  outletEmail : String?
    var  outletContactNo:String?
    var  outletAddress : String?
    var  address:String?
    var  avg_rating : String?
    var total_rating: String?
    
    var image : String?
    var  contact_no:String?
    var  email_id : String?
    var  pinCode:String?
    var  lat : String?
    var  lng:String?
    var  openTime : String?
    var  closeTime:String?
    var  paymentMode : String?
    
    var outletGenderType: String?
    
    var weekoffDays : String?
    var  facebookLink:String?
    var  instagramLink : String?
    var  googleLink:String?
    var  startingDate : String?
    var  created_datetime:String?
    var  city_name : String?
    var  distance:Float?
  

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        beautyOutletCode <- map["beautyOutletCode"]
        outletName <- map["outletName"]
        contactPersonName <- map["contactPersonName"]
             outletEmail <- map["outletEmail"]
        outletContactNo <- map["outletContactNo"]
             outletAddress <- map["outletAddress"]
        address <- map["beautyOutletCode"]
             avg_rating <- map["outletName"]
        total_rating <- map["total_rating"]
        
        image <- map["image"]
             contact_no <- map["contact_no"]
             email_id <- map["email_id"]
                  pinCode <- map["pinCode"]
             lat <- map["lat"]
                  lng <- map["lng"]
             openTime <- map["openTime"]
                  closeTime <- map["closeTime"]
             paymentMode <- map["paymentMode"]
        
        outletGenderType <- map["outletGenderType"]
                   weekoffDays <- map["weekoffDays"]
                   facebookLink <- map["facebookLink"]
                        instagramLink <- map["instagramLink"]
                   googleLink <- map["googleLink"]
                        startingDate <- map["startingDate"]
                   created_datetime <- map["created_datetime"]
                        city_name <- map["city_name"]
                   distance <- map["distance"]
    }
}

class SearchOuletResponce : Mappable{
    
    var  message:String?
    var  result : String?
    var  data : [searchoutletArray]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        message <- map["message"]
        result <- map["result"]
        data <- map["data"]
        
    }
}

class searchoutletArray : Mappable{
    var id : String?
    var  beautyOutletCode:String?
    var  outletName : String?
    var  contactPersonName:String?
    var  outletEmail : String?
    var  outletContactNo:String?
    var  outletAddress : String?
    var  address:String?
    var  avg_rating : String?
    var total_rating: String?
    
    var image : String?
    var  contact_no:String?
    var  email_id : String?
    var  pinCode:String?
    var  lat : String?
    var  lng:String?
    var  openTime : String?
    var  closeTime:String?
    var  paymentMode : String?
    
    var outletGenderType: String?
    
    var weekoffDays : String?
    var  facebookLink:String?
    var  instagramLink : String?
    var  googleLink:String?
    var  startingDate : String?
    var  created_datetime:String?
    var  city_name : String?
    var  distance:Float?
  

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        beautyOutletCode <- map["beautyOutletCode"]
        outletName <- map["outletName"]
        contactPersonName <- map["contactPersonName"]
             outletEmail <- map["outletEmail"]
        outletContactNo <- map["outletContactNo"]
             outletAddress <- map["outletAddress"]
        address <- map["beautyOutletCode"]
             avg_rating <- map["outletName"]
        total_rating <- map["total_rating"]
        
        image <- map["image"]
             contact_no <- map["contact_no"]
             email_id <- map["email_id"]
                  pinCode <- map["pinCode"]
             lat <- map["lat"]
                  lng <- map["lng"]
             openTime <- map["openTime"]
                  closeTime <- map["closeTime"]
             paymentMode <- map["paymentMode"]
        
        outletGenderType <- map["outletGenderType"]
                   weekoffDays <- map["weekoffDays"]
                   facebookLink <- map["facebookLink"]
                        instagramLink <- map["instagramLink"]
                   googleLink <- map["googleLink"]
                        startingDate <- map["startingDate"]
                   created_datetime <- map["created_datetime"]
                        city_name <- map["city_name"]
                   distance <- map["distance"]
    }
}

class BookingListRespnce : Mappable{
    
    var  message:String?
    var  result : String?
    var  data : BookingTypeResponce?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        message <- map["message"]
        result <- map["result"]
        data <- map["data"]
        
    }
}
class BookingTypeResponce : Mappable{
    
    var  upcoming:[UpcommingRespoce]?
    var  past : [PastResponce]?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        upcoming <- map["upcoming"]
        past <- map["past"]
      
        
    }
}



class UpcommingRespoce : Mappable{
    var  id : String?
    var  image:String?
    var  outletName : String?
    var  bookingId:String?
    var  appointmentDateSlot : String?
    
    
    var displayTime:String
    {
        return Util.getDisplayTimeOnly(strDate: appointmentDateSlot)
    }
    
    var displayDate:String
       {
           return Util.getDisplayDate(strDate: appointmentDateSlot)
       }

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        image <- map["image"]
        outletName <- map["outletName"]
        bookingId <- map["bookingId"]
        appointmentDateSlot <- map["appointmentDateSlot"]
    }
}

class PastResponce : Mappable{
    var  id : String?
    var  image:String?
    var  outletName : String?
    var  bookingId:String?
    var  appointmentDateSlot : String?
    
    
       var displayTime:String
       {
           return Util.getDisplayTimeOnly(strDate: appointmentDateSlot)
       }
       
       var displayDate:String
          {
              return Util.getDisplayDate(strDate: appointmentDateSlot)
          }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        image <- map["image"]
        outletName <- map["outletName"]
        bookingId <- map["bookingId"]
        appointmentDateSlot <- map["appointmentDateSlot"]
    }
}
class BookingDetailRespnce : Mappable{
    
    var  message:String?
    var  result : String?
    var  data : BookingDetailList?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        message <- map["message"]
        result <- map["result"]
        data <- map["data"]
        
    }
}
class BookingDetailList : Mappable{
    var  bookingId : String?
    var  clientId:String?
    var  appointmentDate : String?
    var  appointmentDateSlot:String?
    var  bookingTotalAmount : String?
    var  paymentMode : String?
    var  created_datetime:String?
    var  booking_status : String?
    var  outletImage:String?
    var  outletName : String?
    var  clientName : String?
    var  lastName:String?
    var  clientEmail : String?
    var  clientPhone:String?
    var  clientProfilePic : String?
    var serviceDetail : [ServiceDeatilResponce]?
    var clientFeedBack : String?
    
    var displayTime:String
         {
             return Util.getDisplayTimeOnly(strDate: appointmentDateSlot)
         }
         
         var displayDate:String
            {
                return Util.getDisplayDate(strDate: appointmentDateSlot)
            }

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        bookingId <- map["bookingId"]
        clientId <- map["clientId"]
        appointmentDate <- map["appointmentDate"]
        appointmentDateSlot <- map["appointmentDateSlot"]
        bookingTotalAmount <- map["bookingTotalAmount"]
        paymentMode <- map["paymentMode"]
            created_datetime <- map["created_datetime"]
            booking_status <- map["booking_status"]
            outletImage <- map["outletImage"]
            outletName <- map["outletName"]
        clientName <- map["clientName"]
                  lastName <- map["lastName"]
                  clientEmail <- map["clientEmail"]
                  clientPhone <- map["clientPhone"]
        clientProfilePic <- map["clientProfilePic"]
                         serviceDetail <- map["serviceDetail"]
                         clientFeedBack <- map["clientFeedBack"]
        
    }
}
class ServiceDeatilResponce : Mappable{
    var  serviceName : String?
    var  serviceDuration:String?
    var  serviceAgent : String?
    var  slot:String?
    var  appointDate : String?
    var displayTime:String
        {
            return Util.getDisplayTimeOnly(strDate: slot)
        }
        
        var displayDate:String
           {
               return Util.getDisplayDateOnly(strDate: appointDate)
           }

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        serviceName <- map["serviceName"]
        serviceDuration <- map["serviceDuration"]
        serviceAgent <- map["serviceAgent"]
        slot <- map["slot"]
        appointDate <- map["appointDate"]
    }
}


class StaffListResponce : Mappable{
    
    var  openTime:String?
    var  closeTime : String?
    var  result : String?
     var  data : String?
      var  message : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        openTime <- map["openTime"]
        closeTime <- map["closeTime"]
        result <- map["result"]
        data <- map["data"]
         message <- map["message"]
        
        
    }
}

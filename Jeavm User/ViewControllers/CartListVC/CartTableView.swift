//
//  CartTableView.swift
//  Jeavm User
//  Created by Quantum softech on 06/11/19.
//  Copyright © 2019 Sueb. All rights reserved.

import UIKit

class CartTableView: BaseVC {

    @IBOutlet weak var tableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
         registerNib()
         tableview.delegate = self
         tableview.dataSource =  self
    }
    func registerNib()
          {
              let ratingNib = UINib(nibName: "CartTableCell", bundle: nil)
              tableview.register(ratingNib, forCellReuseIdentifier: "CartTableCell")
          }


}
extension CartTableView : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                 let cell = tableView.dequeueReusableCell(withIdentifier: "CartTableCell")as!CartTableCell
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return UITableView.automaticDimension
     }
    
    
}

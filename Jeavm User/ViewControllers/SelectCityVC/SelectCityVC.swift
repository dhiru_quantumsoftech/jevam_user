
//  SelectCityVC.swift
//  Jeavm User
//  Created by Purnendu on 06/11/19.
//  Copyright © 2019 Sueb. All rights reserved.


import UIKit
import Toast_Swift
import Alamofire
import AlamofireObjectMapper

class CityCell:UITableViewCell{
    @IBOutlet weak var lblCity: UILabel!
}

class SelectCityVC: BaseVC,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var btnLocation: Button!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var cityListArray :Array<CityListArray> = [CityListArray]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.webServiceCallingForCityList()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnLOcation(_ sender: Any) {
        
    }
    
    @IBAction func btnSearch(_ sender: Any) {
        
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cityListArray.count
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           
           let cell = tableView.dequeueReusableCell(withIdentifier: "CityCell")as!CityCell
           cell.lblCity.text = self.cityListArray[indexPath.row].city_name
           return cell
           
       }
       
       func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           
            selectedCity = self.cityListArray[indexPath.row].city_name ?? ""
            selectedCityID = self.cityListArray[indexPath.row].id ?? ""
        
            PreferenceUtils.putString("selectedCity", value: self.cityListArray[indexPath.row].city_name ?? "")
            PreferenceUtils.putString("selectedCityID", value: self.cityListArray[indexPath.row].id ?? "")
            self.navigationController?.popViewController(animated: true)
       }
       
       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 40

       }

       func webServiceCallingForCityList(){
            
            MDProgressView.shared.showProgressView(view: self.view)
            
            let parameters: [String: Any] = [
              "api_type":"cityList"
            ]

            print(parameters)
        
      /*  Almgr.request(UrlConstant.BASE_URL, method: .post, parameters: parameters,encoding: JSONEncoding.default, headers: nil).responseJSON {
                   response in*/
        
          let headers: HTTPHeaders = [
            "Authorization": headerToken,
            "Accept": "application/json"
          ]
        
            Almgr.request(UrlConstant.BASE_URL, method: .post, parameters: parameters, encoding: JSONEncoding.default,headers: headers)
                .responseObject { (response:DataResponse<CityListResponse>) in
                    MDProgressView.shared.hideProgressView()
                    if response.result.isSuccess {
                      let apiResult = response.result.value
                      if apiResult!.result == "success"{
                        self.cityListArray = (apiResult?.cityList)!
                        self.tableView.reloadData()
                      }
                    }else{
                        self.view.makeToast("Server Error", duration: 2.0, position: .top, title: "", image: nil)
                    }
            }
        }
      
       
       let Almgr : Alamofire.SessionManager = {
           // Create the server trust policies
           let serverTrustPolicies: [String: ServerTrustPolicy] = [
               UrlConstant.BASE_test_url : .disableEvaluation
           ]
           // Create custom manager
           let configuration = URLSessionConfiguration.default
           configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
           let man = Alamofire.SessionManager(
               configuration: URLSessionConfiguration.default,
               serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
           )
           return man
       }()
    
}

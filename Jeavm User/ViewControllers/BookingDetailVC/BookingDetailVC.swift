//
//  BookingDetailVC.swift
//  Jeavm User
//
//  Created by Quantum softech on 13/11/19.
//  Copyright © 2019 Sueb. All rights reserved.
//

import UIKit
import Toast_Swift
import Alamofire
import AlamofireObjectMapper

class BookingDetailVC: BaseVC {
    var bookingDetail : BookingDetailList? = nil
    var serviceDetail : Array<ServiceDeatilResponce> = [ServiceDeatilResponce]()
    //.....HeaderDeatil
    @IBOutlet weak var img_Avtar: UIImageView!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_clientNmae: UILabel!
    @IBOutlet weak var lbl_BooikngId: UILabel!
    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var lbl_time: UILabel!
    ///text view height constant
    @IBOutlet weak var secondtext_viewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var FirsttextView_Height: NSLayoutConstraint!
    
    ///....
    @IBOutlet weak var collectionView: CollectionView!
    @IBOutlet weak var tableView: TableView!
    @IBOutlet weak var dashView: UIView!
    //......TextView....
    @IBOutlet weak var first_textViiew: UITextView!
    @IBOutlet weak var second_textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        registerNib()
        ///.......
        self.first_textViiew.layer.borderColor = UIColor.lightGray.cgColor
        self.first_textViiew.layer.borderWidth = 1
        self.first_textViiew.clipsToBounds = true
        self.first_textViiew.layer.cornerRadius = 5
        
        self.second_textView.layer.borderColor = UIColor.lightGray.cgColor
        self.second_textView.layer.borderWidth = 1
        self.second_textView.clipsToBounds = true
        self.second_textView.layer.cornerRadius = 5
        
        
        ///.......
        FirsttextView_Height.constant = 0
        secondtext_viewHeight.constant = 0
        
        //...headerimage
        self.img_Avtar.layer.cornerRadius = 5
        self.img_Avtar.clipsToBounds = true
    
    }
    override func viewDidAppear(_ animated: Bool) {
          self.webServiceForBookingDeatail()
        
        }
    
    @IBAction func btn_firsttextReview(_ sender: Any) {
        if FirsttextView_Height.constant > 0
        {
            FirsttextView_Height.constant = 0
        }else{
            FirsttextView_Height.constant = 70
        }

    }
    
    @IBAction func btn_secondtextReview(_ sender: Any) {
            if secondtext_viewHeight.constant > 0
                {
                    secondtext_viewHeight.constant = 0
                }else{
                    secondtext_viewHeight.constant = 70
                }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func loadData() {
        if let value = self.bookingDetail?.outletImage{
                                                     let imageUrl = value
                                                     self.img_Avtar.sd_setImage(with: URL(string: imageUrl ), placeholderImage: UIImage(named: PlaceHolderImage))
                                             }
        self.lbl_BooikngId.text = "BookingId-\(bookingDetail?.bookingId ?? "")"
        self.lbl_name.text = "\(bookingDetail?.outletName ?? "")"
        self.lbl_clientNmae.text = bookingDetail?.clientName ?? ""
        self.lbl_time.text = bookingDetail?.displayTime
        self.lbl_date.text = bookingDetail?.displayDate
        
    }
    
    func registerNib()
    {
          let stafNib = UINib(nibName: "StaffClcCell", bundle: nil)
          collectionView.register(stafNib, forCellWithReuseIdentifier: "StaffClcCell")
        

         let ratingNib = UINib(nibName: "ServiceQuickDetailsTblCell", bundle: nil)
        tableView.register(ratingNib, forCellReuseIdentifier: "ServiceQuickDetailsTblCell")
    
      }
    
     ////...# Api for Booking Deatil
      
      func webServiceForBookingDeatail(){
            
            MDProgressView.shared.showProgressView(view: self.view)
            
            let parameters: [String: Any] = [
               "api_type":"bookingDetail",
               "bookingId":"h68-191533"
      ]
          
          print(UrlConstant.BASE_URL)
            print(parameters)
            
            Almgr.request(UrlConstant.BASE_URL, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                .responseObject { (response:DataResponse<BookingDetailRespnce>) in
                    MDProgressView.shared.hideProgressView()
                  
                   //let json = response.result.value as? [String:Any]
                   print("Response :\(response.data?.convertToJSONString() ?? "-NA-")")
                  
                    if response.result.isSuccess {
                       let apiResult = response.result.value
                      if apiResult!.result == "success"{
                           print("Success:: ",response)
                        self.bookingDetail = (apiResult?.data)!
                        self.serviceDetail = (apiResult?.data?.serviceDetail)!
                           self.tableView.reloadData()
                         self.collectionView.reloadData()
                         self.loadData()
                      }else{
                         
                      }
                       
                    }else{
                        self.view.makeToast("Server Error", duration: 2.0, position: .top, title: "", image: nil)
                    }
            }
        }
      
       let Almgr : Alamofire.SessionManager = {
           // Create the server trust policies
           let serverTrustPolicies: [String: ServerTrustPolicy] = [
               UrlConstant.BASE_test_url : .disableEvaluation
           ]
           // Create custom manager
           let configuration = URLSessionConfiguration.default
           configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
           let man = Alamofire.SessionManager(
               configuration: URLSessionConfiguration.default,
               serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
           )
           return man
       }()
      public func AlertShow(title : NSString){
      
          let alert = UIAlertController(title: "", message: title as String, preferredStyle: .alert)
          
          alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { [weak alert] (_) in
              
          }))
          
          self.present(alert, animated: true, completion: nil)
          
      }
}
extension BookingDetailVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return serviceDetail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceQuickDetailsTblCell")as!ServiceQuickDetailsTblCell
        cell.lblServiceBy.text = "Serviced by : \(serviceDetail[indexPath.row].serviceAgent ?? "")"
        cell.lblServiceName.text = serviceDetail[indexPath.row].serviceName ?? ""
        cell.lblServiceDate.text = serviceDetail[indexPath.row].displayDate
        cell.lblServiceTime.text =  serviceDetail[indexPath.row].slot
        cell.lblServiceDuration.text = "\(serviceDetail[indexPath.row].serviceDuration ?? "" ) mins"
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return UITableView.automaticDimension
    }
    
    
    
}

extension BookingDetailVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StaffClcCell", for: indexPath) as! StaffClcCell
        return cell 
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width/7, height: self.collectionView.frame.height-10)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
      return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}

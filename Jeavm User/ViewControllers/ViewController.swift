
//  ViewController.swift
//  Jeavm User
//  Created by Sueb on 15/10/19.
//  Copyright © 2019 Sueb. All rights reserved.

import UIKit
import SVPinView

class ViewController: UIViewController {
   
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var paswwordTxtField: UITextField!
    @IBOutlet weak var backImageVw: UIImageView!
    @IBOutlet weak var letCreateConstant: NSLayoutConstraint!
    @IBOutlet weak var otpConstant: NSLayoutConstraint!
    @IBOutlet weak var pinView: SVPinView!
    @IBOutlet weak var otpVw: UIView!
    @IBOutlet weak var backBtn: UIButton!
    var isSafeArea = false
    @IBOutlet var parentView: UIView!
    @IBOutlet var userView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @IBAction func submitActin(_ sender: Any) {
        
        self.parentView.addSubview(self.otpVw)
        if self.isSafeArea{
            self.otpVw.frame = CGRect(x: -self.view.frame.size.width, y: 50,width: self.view.frame.size.width - 65 ,height: self.view.frame.size.height - 205)
        }else{
            self.otpVw.frame = CGRect(x: -self.view.frame.size.width, y: 50,width: self.view.frame.size.width - 65 ,height: self.view.frame.size.height - 180)
        }
        
        animateVw(view: otpVw, hideView: userView)
        backBtn.isHidden = false
        backBtn.tag = 1
        
    }
    
    override func viewSafeAreaInsetsDidChange() {
        super.viewSafeAreaInsetsDidChange()
        
        if view.safeAreaInsets.bottom > 0 {
            //home indicator
             letCreateConstant.constant = 50
             otpConstant.constant = 50
            isSafeArea = true
            
        }else{
            isSafeArea = false
            print("no home indicator")
            //no home indicator
        }
    }
    
    func animateVw(view : UIView , hideView : UIView)   {
        //view.backgroundColor = UIColor.red
        UIView.animate(withDuration: 1.5, delay: 1.00, options: UIView.AnimationOptions(), animations: { () -> Void in
            
            
            if self.isSafeArea{
                view.frame = CGRect(x: 0, y: 50,width: UIScreen.main.bounds.size.width - 65 ,height: UIScreen.main.bounds.size.height - 205)
            }else{
                view.frame = CGRect(x: 0, y: 50,width: UIScreen.main.bounds.size.width - 65 ,height: UIScreen.main.bounds.size.height - 180)
            }
        }, completion: { (finished: Bool) -> Void in
            view.backgroundColor = UIColor.clear
            hideView.isHidden = true
            
        })
    }
    
    func animateHideVw(view : UIView)  {
        
        UIView.animate(withDuration: 1.5, delay: 1.00, options: UIView.AnimationOptions(), animations: { () -> Void in
            
            if self.isSafeArea{
                view.frame = CGRect(x: -self.view.frame.size.width, y: 50,width: self.view.frame.size.width - 65 ,height: self.view.frame.size.height - 205)
            }else{
                view.frame = CGRect(x: -self.view.frame.size.width, y: 50,width: self.view.frame.size.width - 65 ,height: self.view.frame.size.height - 180)
            }
        }, completion: { (finished: Bool) -> Void in
            view.backgroundColor = UIColor.clear
            view.removeFromSuperview()
            
        })
    }
    
    @IBAction func backAction(_ sender: Any) {
        if backBtn.tag == 1 {
            userView.isHidden = false
            backBtn.isHidden = true
            backBtn.tag = 0
            animateHideVw(view: otpVw)
        }
    }
    
    @IBAction func submitOtp(_ sender: Any) {
        
    }
    
}


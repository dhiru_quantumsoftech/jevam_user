
//  HomeVC.swift
//  Jeavm User
//  Created by Sueb on 17/10/19.
//  Copyright © 2019 Sueb. All rights reserved.

import UIKit
import SDWebImage


class HomeVC: UIViewController,UITextFieldDelegate {
   
    @IBOutlet weak var mainScroll: UIScrollView!
    
    @IBOutlet weak var clcBannerView: UICollectionView!
    
    @IBOutlet weak var clcVendorsList: UICollectionView!
    
    var listType:ListType = .list
    
    override func viewDidLoad() {
           super.viewDidLoad()


       }
   
    @IBAction func btnListGridToggleAction(_ sender: Any) {
        
        
    }
    
}






   extension HomeVC : UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate {
       func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           if collectionView.tag == 0{
               return 5
           }else{
               return 5
           }
       }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           if collectionView == clcBannerView
           {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeTrendingClcCell", for: indexPath)  as! HomeTrendingClcCell
            
            return cell
          }else
           {
            // collectionView == clcVendorsList
            if listType == .list
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeListClcCell", for: indexPath)  as! HomeListClcCell
                           
                           return cell
            }else
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeGridClcCell", for: indexPath)  as! HomeGridClcCell
                           
                           return cell
            }
           }
       }
       func collectionView(_ collectionView: UICollectionView,
                             layout collectionViewLayout: UICollectionViewLayout,
                             sizeForItemAt indexPath: IndexPath) -> CGSize {
             
             if collectionView.tag == 0 {
                 return CGSize(width:UIScreen.main.bounds.width - 40 , height:180)
             }
             else{
                     return CGSize(width:UIScreen.main.bounds.width - 20 , height:100)
                 }
             }
         func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
                
                return 0
            }
            
            func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
                
                return 0
            }
       func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                      let storyboard = UIStoryboard(name: "Second", bundle: nil)
                      let pvc = storyboard.instantiateViewController(withIdentifier: "DetailsParentTabVC") as! DetailsParentTabVC
                      self.navigationController?.pushViewController(pvc, animated: true)
       }
       
         
   }



enum ListType:Int
{
    case list
    case grid
}

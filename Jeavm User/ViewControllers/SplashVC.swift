//
//  SplashVC.swift
//  Jeavm User
//
//  Created by Sueb on 21/10/19.
//  Copyright © 2019 Sueb. All rights reserved.
//

import UIKit
import PulsingHalo

class SplashVC: BaseVC {
 @IBOutlet weak var imgvAvatar: UIImageView!
    var pulseArray = [CAShapeLayer]()
    override func viewDidLoad() {
        super.viewDidLoad()
          createPulse()
          self.navigationController?.setNavigationBarHidden(true, animated: false)
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.1) {
            // your code here
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let pvc = storyboard.instantiateViewController(withIdentifier: "LocationVC") as! LocationVC
            self.navigationController?.pushViewController(pvc, animated: true)

        }
        // Do any additional setup after loading the view.
    }
    
    func createPulse() {
       
        let halo = PulsingHaloLayer()
        halo.position = view.center
        halo.haloLayerNumber = 9
        halo.radius = 180.0
        halo.animationDuration = 6
        halo.backgroundColor = UIColor.white.withAlphaComponent(0.6).cgColor
       // pulsator.radius =  240.0 //CGFloat(0.7) * kMaxRadius
       // halo.backgroundColor = UIColor(hexString: "#FF5C3B").cgColor
        //view.layer.addSublayer(halo)
        imgvAvatar.superview!.layer.insertSublayer(halo, below: imgvAvatar.layer)
        halo.start()
    }
   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


//  AppDelegate.swift
//  Jeavm User
//  Created by Sueb on 15/10/19.
//  Copyright © 2019 Sueb. All rights reserved.

import MapKit
import CoreLocation
import UIKit
import SVProgressHUD
import IQKeyboardManagerSwift
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,CLLocationManagerDelegate {
  let locationManager = CLLocationManager()
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
          IQKeyboardManager.shared.enable = true
        setLocationMangerSetting()
        
             SVProgressHUD.setDefaultStyle(.custom)
             SVProgressHUD.setDefaultMaskType(.custom)
             SVProgressHUD.setForegroundColor(UIColor.primaryDarkColor)
        return true
    }
    func setLocationMangerSetting() {
        self.locationManager.delegate = self
        self.locationManager.distanceFilter = 100.0;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
    
    func fetchCountryAndCity(location: CLLocation, completion: @escaping (String, String) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            if let error = error {
                print(error)
            }else if let country = placemarks?.first?.country,
                let city = placemarks?.first?.locality {
                completion(country, city)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager.stopUpdatingLocation()
        let locValue:CLLocationCoordinate2D  = locations[0].coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        let locations1 = CLLocation(latitude: locations[0].coordinate.latitude, longitude: locations[0].coordinate.longitude)
       
        
        let latitude:String = String(format:"%.6f", locations[0].coordinate.latitude)
        let longitude:String = String(format:"%.6f", locations[0].coordinate.longitude)
      
        fetchCountryAndCity(location: locations1) { country, city in
            print("country:", country)
            print("city:", city)
           
            
        }
        manager.stopUpdatingLocation()
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

extension AppDelegate {
    
    func showIndicator()
    {
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
    }
    
    func hideIndicator ()
    {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
}


extension AppDelegate
{
    func setupAfterLogin()
    {
        //DispatchQueue.main.async {
//        self.setUpSideMenu()
//        self.hideIndicator()
        //  }
    }
    func setupAfterLogout()
    {
        // DispatchQueue.main.async {
//        let mainNav = Storyboard.loadLoginRootVC()
//        self.window?.rootViewController = mainNav
//        self.hideIndicator()
        // }
    }
    
    
    func setUpSideMenu()
    {
        
//        var mainNav:UINavigationController!
//        var sideMenu:UIViewController!
//        
//        if let userInfo = UserSession.shared.getUserLoggedIn(),userInfo.userType == .vendor
//        {
//            mainNav = Storyboard.loadDashboardNav()
//            sideMenu = Storyboard.loadSideMenuVC()
//        }else
//        {
//            mainNav = Storyboard.loadStaffNavigationVC()
//            sideMenu = Storyboard.loadSaffSideMenuVC()
//        }
//        
//        sideMenuController = LGSideMenuController(rootViewController: mainNav,
//                                                  leftViewController: sideMenu,
//                                                  rightViewController: nil)
//        
//        sideMenuController?.isLeftViewEnabled = true
//        sideMenuController?.isLeftViewSwipeGestureEnabled = true
//        sideMenuController?.leftViewWidth = 280.0;
//        sideMenuController?.leftViewPresentationStyle = .slideAbove;
//        
//        window?.rootViewController = sideMenuController!
    }
    
}

func hexStringToUIColor (hex:String) -> UIColor {
    
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}



//
//  HomeListClcCell.swift
//  Jeavm User
//
//  Created by Refundme on 12/3/19.
//  Copyright © 2019 Sueb. All rights reserved.
//

import UIKit

class HomeListClcCell: UICollectionViewCell {
     
    @IBOutlet weak var img_GenderType: UIImageView!
      @IBOutlet weak var imgHome: ImageCardView!
      @IBOutlet weak var lblTitle: UILabel!
      @IBOutlet weak var ratingView: CardView!
      @IBOutlet weak var lblRating: UILabel!
      @IBOutlet weak var lblTime: UILabel!
      @IBOutlet weak var lblGender: UILabel!
      @IBOutlet weak var lblAddress: UILabel!
      @IBOutlet weak var lbl_Distance: UILabel!
      override func awakeFromNib() {
          super.awakeFromNib()
          
          // Initialization code
          
      }
}

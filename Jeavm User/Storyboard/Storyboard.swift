//
//  Storyboard.swift
//  Dhiru Boilerplate
//
//  Created by mac on 2/06/2019.
//  Copyright © 2017 Dhiru  All rights reserved.
//

import Foundation
import UIKit


extension Storyboard
{
 
    class func loadLoginVC() -> ViewController
    {
        return loadFromMain(identifire: "ViewController") as! ViewController
    }
    
    class func loadLoginRootVC() -> UINavigationController
    {
        return loadFromMain(identifire: "LoginNavVC") as! UINavigationController
    }
    
    class func loadLocationVC() -> LocationVC
     {
            return loadFromMain(identifire: "LocationVC") as! LocationVC
     }
    

}


// MARK: LocationPicker Storyboard
//extension Storyboard
//{
//    class func loadSetLocationVC() -> SetLocationVC
//    {
//           return loadFromLocationPicker(identifire: "SetLocationVC") as! SetLocationVC
//    }
//
//    class func loadSetLocationManuallyVC() -> SetLocationManuallyVC
//      {
//             return loadFromLocationPicker(identifire: "SetLocationManuallyVC") as! SetLocationManuallyVC
//      }
//
//    class func loadLocationPickerVC() -> LocationPickerVC
//    {
//        return loadFromLocationPicker(identifire: "LocationPickerVC") as! LocationPickerVC
//    }
//
//    class func loadLocationSearchTableVC() -> LocationSearchTable
//    {
//        return loadFromLocationPicker(identifire: "LocationSearchTable") as! LocationSearchTable
//    }
//}


// Base Class 
class Storyboard {

    private class func loadFromMain(identifire:String) -> UIViewController
    {
        return loadViewController(storyboardName: StoryboardName.main, identifire: identifire)
    }
    private class func loadFromDashboard(identifire:String) -> UIViewController
    {
        return loadViewController(storyboardName: StoryboardName.dashboard, identifire: identifire)
    }
    
    
    
    private  class func loadViewController(storyboardName:String,identifire:String) -> UIViewController
    {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: identifire)
    }
    
    private struct StoryboardName
    {
        static let main = "Main"
        static let dashboard = "Dashboard"
        static let cart = "Cart"
        static let dailer = "Dialer"
        static let second = "Second"
    }
    
}


